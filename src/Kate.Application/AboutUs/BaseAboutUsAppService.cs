﻿using Kate.AboutUs.Dto;

namespace Kate.AboutUs
{
    public abstract class BaseAboutUsAppService
    {
        public abstract AboutUsReadDto GetAboutUsReadViewModel();

        public abstract AboutUsDto GetAboutUsManageViewModel();

        public abstract void Update(AboutUsDto model);

        public abstract void Dispose();
    }
}