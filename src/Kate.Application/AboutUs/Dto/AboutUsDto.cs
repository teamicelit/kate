﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.AboutUs.Dto
{
    public class AboutUsDto
    {
        public int Id { get; set; }

        public IEnumerable<AboutUsTranslationDto> Translations { get; set; }
    }
}
