﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.AboutUs.Dto
{
    public class AboutUsReadDto
    {
        public int Id { get; set; }

        public AboutUsTranslationDto Translation { get; set; }
    }
}
