﻿using Abp.ObjectMapping;
using Kate.AboutUs.Dto;
using Kate.EntityFrameworkCore.Repositories;
using Kate.Localization;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Kate.AboutUs
{
    public class AboutUsAppService : BaseAboutUsAppService
    {
        private readonly ICustomUnitOfWork _unitOfWork;
        private readonly IObjectMapper _objectMapper;

        public AboutUsAppService(ICustomUnitOfWork unitOfWork, IObjectMapper objectMapper)
        {
            _unitOfWork = unitOfWork;
            _objectMapper = objectMapper;
        }

        public override AboutUsReadDto GetAboutUsReadViewModel()
        {
            var model = new AboutUsReadDto();

            model = _objectMapper.Map<AboutUsReadDto>(
                _unitOfWork.AboutUsRepo.Set()
                .Include(a => a.Translations)
                .Select(a => new { a.Id, a.IsDeleted, Translation = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()) })
                .First(a => !a.IsDeleted));

            return model;
        }
        
        public override AboutUsDto GetAboutUsManageViewModel()
        {
            var model = new AboutUsDto();

            model = _objectMapper.Map<AboutUsDto>(
                _unitOfWork.AboutUsRepo.Set()
                .Include(a => a.Translations)
                .Select(a => new { a.Id, a.IsDeleted, Translations = a.Translations })
                .First(a => !a.IsDeleted));

            return model;
        }

        public override void Update(AboutUsDto model)
        {
            var dboAboutUs = _unitOfWork.AboutUsRepo.Set()
               .Include(a => a.Translations)
               .First(a => a.Id == model.Id);                  

            foreach (var dboTranslation in dboAboutUs.Translations)
                foreach (var modelTranslation in model.Translations)
                {
                    if (dboTranslation.Id != modelTranslation.Id)
                        continue;

                    dboTranslation.Description = modelTranslation.Description;
                    dboTranslation.Name = modelTranslation.Name;
                }

            _unitOfWork.AboutUsRepo.Update(dboAboutUs);
            _unitOfWork.Save();
        }

        public override void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
