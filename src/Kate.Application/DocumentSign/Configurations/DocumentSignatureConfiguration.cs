﻿namespace Kate.DocumentSign.Configurations
{
    public class DocumentSignatureConfiguration
    {
        public string Url { get; set; }
    }
}
