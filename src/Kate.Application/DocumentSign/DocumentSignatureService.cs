﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Kate.DocumentSign.Configurations;
using Kate.DocumentSign.Dtos;
using Kate.EntityFrameworkCore;
using Kate.EntityFrameworkCore.Entities;
using Kate.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Kate.DocumentSign
{
    public class DocumentSignatureService : IDocumentSignatureService
    {
        private readonly ICustomUnitOfWork _unitOfWork;
        private readonly IOptions<DocumentSignatureConfiguration> _documentSignatureConfiguration;

        public DocumentSignatureService(
            ICustomUnitOfWork unitOfWork, 
            IOptions<DocumentSignatureConfiguration> documentSignatureConfiguration)
        {
            _unitOfWork = unitOfWork;
            _documentSignatureConfiguration = documentSignatureConfiguration;
        }

        public Uri GenerateApiUrl(string path)
        {
            var signatureUrl = new Uri(_documentSignatureConfiguration.Value.Url);
            return new Uri(signatureUrl, path);
        }

        public Guid CreateDocumentSignature(string filePath)
        {
            var documentSignature = new DocumentSignature
            {
                FilePath = filePath,
                IsProcessed = false
            };

            _unitOfWork.DocumentSignatureRepository.Insert(documentSignature);
            _unitOfWork.Save();

            return documentSignature.Id;
        }

        public DocumentSignatureProcess ValidateSignatureProcess(Guid id)
        {
            var documentSignature = _unitOfWork.DocumentSignatureRepository.GetById(id);
            if (documentSignature == null)
            {
                throw new FileNotFoundException();
            }

            var filePath = GenerateApiUrl($"DocumentSign/Download?fileName={documentSignature.FilePath}").ToString();

            return new DocumentSignatureProcess
            {
                FilePath = filePath,
                IsProcessed = documentSignature.IsProcessed
            };
        }

        public async Task<UploadDocumentResponse> UploadPdf(Stream fileStream, string fileName)
        {
            using (fileStream)
            using (var httpClient = new HttpClient())
            using (var httpRequest = new HttpRequestMessage())
            {
                var boundary = Guid.NewGuid().ToString();
                var content = new MultipartFormDataContent(boundary);
                content.Headers.Remove("Content-Type");
                content.Headers.TryAddWithoutValidation("Content-Type", "multipart/form-data; boundary=" + boundary);
                content.Add(new StreamContent(fileStream), "File", fileName);

                httpRequest.Content = content;
                httpRequest.RequestUri = GenerateApiUrl("DocumentSign/UploadFile");
                httpRequest.Method = new HttpMethod("POST");

                var result = await httpClient.SendAsync(httpRequest, HttpCompletionOption.ResponseContentRead);
                var response = await result.Content.ReadAsStringAsync();
                var uploadDocumentResponse = JsonConvert.DeserializeObject<UploadDocumentResponse>(response);
                var uploadedDocumentFilePath = uploadDocumentResponse.Url;
                var id = CreateDocumentSignature(uploadedDocumentFilePath);
                uploadDocumentResponse.Id = id;
                return uploadDocumentResponse;
            }
        }

        public JsonDocumentResponse GetSignatureData(string fileName)
        {
            using (var webClient = new WebClient())
            {
                var requestUrl = GenerateApiUrl($"DocumentSign/GetDocumentHashString?fileName={fileName}");
                var data = webClient.DownloadData(requestUrl);
                var resultOfData = Encoding.UTF8.GetString(data);

                var downloadUrl = GenerateApiUrl($"DocumentSign/Download?fileName={fileName}").ToString();
                var putHashUrl = GenerateApiUrl($"DocumentSign/PutHash?fileName={fileName}").ToString();
                var message = new JsonDocumentResponse
                {
                    DocType = "PDF",
                    DataType = "hash",
                    DataUrl = downloadUrl,
                    Hash = resultOfData,
                    SignAlg = "SHA256withRSA",
                    KeyId = "sign",
                    Language = "ka",
                    SubmitUrl = putHashUrl
                };

                return message;
            }
        }
    }
}