﻿namespace Kate.DocumentSign.Dtos
{
    public class DocumentSignatureProcess
    {
        public bool IsProcessed { get; set; }

        public string FilePath { get; set; }
    }
}