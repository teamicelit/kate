﻿using System;

namespace Kate.DocumentSign.Dtos
{
    public class UploadDocumentResponse
    {
        public Guid Id { get; set; }

        public string Url { get; set; }
    }
}