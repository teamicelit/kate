﻿namespace Kate.DocumentSign.Dtos
{
    public class JsonDocumentResponse
    {
        public string DocType { get; set; }

        public string DataType { get; set; }

        public string DataHex { get; set; }

        public string DataUrl { get; set; }

        public string Hash { get; set; }

        public string SignAlg { get; set; }

        public string SubmitUrl { get; set; }

        public string Description { get; set; }

        public string Language { get; set; }

        public string KeyId { get; set; }

        public string SignatureProfile { get; set; }
    }
}
