﻿using System;
using System.IO;
using System.Threading.Tasks;
using Kate.DocumentSign.Dtos;

namespace Kate.DocumentSign
{
    public interface IDocumentSignatureService
    {
        Task<UploadDocumentResponse> UploadPdf(Stream fileStream, string fileName);

        JsonDocumentResponse GetSignatureData(string fileName);

        DocumentSignatureProcess ValidateSignatureProcess(Guid id);
    }
}