﻿using Kate.EntityFrameworkCore.Entities;
using System.Collections.Generic;
using Kate.DynamicMenu.ViewModels;

namespace Kate.DynamicMenu.Dto
{
    public class DynamicMenuManageDto
    {
        public int Id { get; set; }
        public int SelectedParentCategoryId { get; set; }
        public int SortIndex { get; set; }

        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }
        public string SchemeSvg { get; set; }
        public string Tags { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }
        public bool DontShowOnResponsive { get; set; }
        public bool ShowInSideNavigation { get; set; }

        public CategoryType CategoryType { get; set; }

        public IEnumerable<DynamicMenuDropdownDto> Dropdown { get; set; }
        public IEnumerable<DynamicMenuTranslationDto> Translations { get; set; }
        public IEnumerable<SchemeItemViewModel> SchemeItems { get; set; }
    }
}