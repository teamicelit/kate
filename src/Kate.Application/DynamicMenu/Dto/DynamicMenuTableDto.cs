﻿using Kate.EntityFrameworkCore.Entities;
using Kate.Services.GenericDtos;
using System.Collections.Generic;

namespace Kate.DynamicMenu.Dto
{
    public class DynamicMenuTableDto
    {
        public int Id { get; set; }
        public int SortIndex { get; set; }

        public string ParentName { get; set; }
        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }
        public bool DontShowOnResponsive { get; set; }
        public bool ShowInSideNavigation { get; set; }

        public DynamicMenuTranslationDto Translation { get; set; }

        public IEnumerable<SearchTagDto> SearchTags { get; set; }

        public CategoryType CategoryType { get; set; }
    }
}
