﻿using Kate.EntityFrameworkCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.DynamicMenu.ViewModels
{
    public class CategoryMenuViewModel
    {
        public int Id { get; set; }
        public int SortIndex { get; set; }

        public bool HasDropdownButton { get; set; }
        public bool DontShowOnResponsive { get; set; }
        public bool ShowInSideNavigation { get; set; }

        public string CategoryName { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }

        public CategoryType CategoryType { get; set; }

        public int? ParentId { get; set; }

        public List<CategoryMenuViewModel> Categories { get; set; }
    }
}
