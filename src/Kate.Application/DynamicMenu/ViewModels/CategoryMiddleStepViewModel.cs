﻿using Kate.EntityFrameworkCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Kate.BaseViewModels;

namespace Kate.DynamicMenu.ViewModels
{
    public class CategoryMiddleStepViewModel : BaseViewModelForPagesWithHeaderStrip
    {
        public int Id { get; set; }

        public string CategoryName { get; set; }

        public IEnumerable<CategoryCildMiddleStep> ClildCategories { get; set; }
    }

    public class CategoryCildMiddleStep
    {
        public int Id { get; set; }

        public string Controller { get; set; }
        public string Action { get; set; }
        public string CategoryName { get; set; }        
        public string MainPhotoPath { get; set; }

        public CategoryType CategoryType { get; set; }
    }
}
