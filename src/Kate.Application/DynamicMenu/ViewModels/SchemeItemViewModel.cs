﻿namespace Kate.DynamicMenu.ViewModels
{
    public class SchemeItemViewModel
    {
        public int X { get; set; }
        public int Y { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        public string Value { get; set; }
        public string SourceId { get; set; }
        public string ConnectToId { get; set; }
        public string Style { get; set; }
    }
}