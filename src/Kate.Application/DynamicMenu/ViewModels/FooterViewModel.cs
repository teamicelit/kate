﻿using Kate.AboutUs.Dto;
using Kate.EntityFrameworkCore.Entities;
using System.Collections.Generic;

namespace Kate.DynamicMenu.ViewModels
{
    public class FooterViewModel
    {
        public IEnumerable<FooterCategoryViewModel> Categories { get; set; }
        public AboutUsReadDto AboutUsText { get; set; }
    }
    
    public class FooterCategoryViewModel
    {
        public int Id { get; set; }
        public int SortIndex { get; set; }

        public string CategoryName { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }

        public CategoryType CategoryType { get; set; }

        public List<Category> Categories { get; set; }
    }
}
