﻿using Kate.DynamicMenu.ViewModels;
using Kate.EntityFrameworkCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kate.DynamicMenu.Helpers
{
    public static class RecursionHelper
    {
        public static List<CategoryMenuViewModel> GetChildren(List<Category> categories, int parentId, int languageId)
        {
            var model = categories.Where(c => c.ParentCategoryId == parentId)
                .Select(c => new CategoryMenuViewModel
                {
                    Id = c.Id,
                    SortIndex = c.SortIndex,
                    CategoryName = c.Translations.First(a => a.LanguageId == languageId).Name,
                    HasDropdownButton = true,
                    DontShowOnResponsive = c.DontShowOnResponsive,
                    ShowInSideNavigation = c.ShowInSideNavigation,
                    CategoryType = c.CategoryType,
                    ParentId = c.ParentCategoryId,
                    Categories = GetChildren(categories, c.Id, languageId)
                }).ToList();

            foreach (var category in model)
            {
                switch (category.CategoryType)
                {
                    case CategoryType.Basic:
                        category.Controller = "About";
                        category.Action = "Work";
                        break;
                    case CategoryType.Home:
                        category.Controller = "Home";
                        category.Action = "Index";
                        break;
                    case CategoryType.Aparat:
                        category.Controller = "About";
                        category.Action = "Aparat";
                        break;
                    case CategoryType.Decisions:
                        category.Controller = "Legislation";
                        category.Action = "Decision";
                        break;
                    case CategoryType.PhotoGallery:
                        break;
                    case CategoryType.News:
                        category.Controller = "News";
                        category.Action = "Index";
                        break;
                    case CategoryType.Disciplinary:
                        category.Controller = "DisciplineJustice";
                        category.Action = "Index";
                        break;
                }
            }

            return model;
        }

        public static void HieararchyWalk(List<CategoryMenuViewModel> hierarchy)
        {
            if (hierarchy == null)
                return;

            foreach (var item in hierarchy)
                HieararchyWalk(item.Categories);
        }
    }
}
