﻿using System;
using System.Collections.Generic;
using System.Text;
using Kate.DynamicMenu.Dto;
using Kate.EntityFrameworkCore.Repositories;
using Kate.EntityFrameworkCore.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Kate.Localization;
using Kate.Services.GenericDtos;
using Kate.DynamicMenu.ViewModels;
using Kate.DynamicMenu.Helpers;
using Kate.Helpers;

namespace Kate.DynamicMenu
{
    public class DynamicMenuAppService : BaseDynamicMenuService
    {
        private readonly ICustomUnitOfWork _unitOfWork;

        public DynamicMenuAppService(ICustomUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override void Delete(int id)
        {
            var dboCategory = _unitOfWork.CategoryRepo.GetById(id);

            dboCategory.IsDeleted = true;

            _unitOfWork.CategoryRepo.Update(dboCategory);
            _unitOfWork.Save();
        }

        public override void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public override IEnumerable<DynamicMenuTableDto> GetTableViewModels()
        {
            var model =
                _unitOfWork.CategoryRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Where(a => !a.IsDeleted)
                .Select(a => new DynamicMenuTableDto
                {
                    Id = a.Id,
                    CategoryType = a.CategoryType,
                    FileUserReadableName = a.FileUserReadableName,
                    MainPhotoPath = a.MainPhotoPath,
                    ParentName = a.ParentCategory.Translations.FirstOrDefault(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name ?? "",
                    PdfFilePath = a.PdfFilePath,
                    ShowOnMainPageBox = a.ShowOnMainPageBox,
                    DontShowOnResponsive = a.DontShowOnResponsive,
                    ShowInSideNavigation = a.ShowInSideNavigation,
                    SortIndex = a.SortIndex,
                    ShowOnSite = a.ShowOnSite,
                    WordFilePath = a.WordFilePath,
                    Translation = a.Translations.Select(b => new DynamicMenuTranslationDto
                    {
                        Id = b.Id,
                        LanguageId = b.LanguageId,
                        Description = b.Description,
                        Name = b.Name
                    }).First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()),
                    SearchTags = a.SearchTags.Select(b => new SearchTagDto { Id = b.Id, Tag = b.Tag })
                }).OrderByDescending(a => a.Id).ToList();

            return model;
        }

        public override DynamicMenuManageDto GetViewModel(int id)
        {
            var model = new DynamicMenuManageDto
            {
                Translations = new List<DynamicMenuTranslationDto>
                {
                    new DynamicMenuTranslationDto {LanguageId = 1},
                    new DynamicMenuTranslationDto {LanguageId = 2},
                    new DynamicMenuTranslationDto {LanguageId = 3},
                    new DynamicMenuTranslationDto {LanguageId = 4}
                },
                Dropdown = _unitOfWork.CategoryRepo.Set()
                .Select(a => new DynamicMenuDropdownDto
                {
                    Id = a.Id,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name
                })
            };

            if (id == 0)
                return model;

            var dboCategory = _unitOfWork.CategoryRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Include(a => a.SchemeItems)
                .First(a => a.Id == id);

            model.Id = dboCategory.Id;
            model.SortIndex = dboCategory.SortIndex;
            model.CategoryType = dboCategory.CategoryType;
            model.FileUserReadableName = dboCategory.FileUserReadableName;
            model.MainPhotoPath = dboCategory.MainPhotoPath;
            model.PdfFilePath = dboCategory.PdfFilePath;
            model.WordFilePath = dboCategory.WordFilePath;
            model.ShowOnMainPageBox = dboCategory.ShowOnMainPageBox;
            model.DontShowOnResponsive = dboCategory.DontShowOnResponsive;
            model.ShowInSideNavigation = dboCategory.ShowInSideNavigation;
            model.ShowOnSite = dboCategory.ShowOnSite;
            model.SelectedParentCategoryId = dboCategory.ParentCategoryId ?? 0;
            model.Tags = string.Join(",", dboCategory.SearchTags.Select(a => a.Tag).ToList());
            model.SchemeItems = dboCategory.SchemeItems.Select(s => new SchemeItemViewModel
            {
                Height = s.Height,
                Value = s.Value,
                Width = s.Width,
                X = s.X,
                Y = s.Y,
                SourceId = s.SourceId,
                ConnectToId = s.ConnectToId,
                Style = s.Style
            }).ToList();

            model.Translations = dboCategory.Translations
                .Select(a => new DynamicMenuTranslationDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    Description = a.Description,
                    LanguageId = a.LanguageId
                }).ToList();

            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new DynamicMenuDropdownDto
                {
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Id = a.Id,
                    IsSelected = a.Id == model.SelectedParentCategoryId
                }).ToList();

            return model;
        }

        public override void FillManageDtoWithInitialData(DynamicMenuManageDto model)
        {
            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new DynamicMenuDropdownDto
                {
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Id = a.Id,
                    IsSelected = a.Id == model.SelectedParentCategoryId
                }).ToList();
        }

        public override void Save(DynamicMenuManageDto model)
        {
            if (model.Id == 0)
                Create(model);
            else
                Update(model);
        }

        protected override void Create(DynamicMenuManageDto model)
        {
            var menuItem = new Category
            {
                ShowOnSite = model.ShowOnSite,
                ShowOnMainPageBox = model.ShowOnMainPageBox,
                WordFilePath = model.WordFilePath,
                PdfFilePath = model.PdfFilePath,
                SchemeSvg = model.SchemeSvg,
                FileUserReadableName = model.FileUserReadableName,
                CategoryType = model.CategoryType,
                SortIndex = model.SortIndex,
                DontShowOnResponsive = model.DontShowOnResponsive,
                ShowInSideNavigation = model.ShowInSideNavigation,
                MainPhotoPath = model.MainPhotoPath
            };

            if (model.SelectedParentCategoryId != 0)
                menuItem.ParentCategoryId = model.SelectedParentCategoryId;

            if (model.SchemeItems != null)
            {
                foreach (var schemeItemViewModel in model.SchemeItems.ToList())
                {
                    menuItem.SchemeItems.Add(new SchemeItem
                    {
                        Height = schemeItemViewModel.Height,
                        Value = schemeItemViewModel.Value,
                        Width = schemeItemViewModel.Width,
                        X = schemeItemViewModel.X,
                        Y = schemeItemViewModel.Y,
                        ConnectToId = schemeItemViewModel.ConnectToId,
                        SourceId = schemeItemViewModel.SourceId,
                        Style = schemeItemViewModel.Style
                    });
                }
            }

            foreach (var translation in model.Translations)
                menuItem.Translations.Add(new CategoryTranslation
                {
                    LanguageId = translation.LanguageId,
                    Description = translation.Description,
                    Name = translation.Name
                });

            if (model.Tags != null)
            {
                var tags = model.Tags.Split(",");

                foreach (var tag in tags)
                {
                    menuItem.SearchTags.Add(new SearchTag { Tag = tag });
                }
            }

            _unitOfWork.CategoryRepo.Insert(menuItem);
            _unitOfWork.Save();
        }

        protected override void Update(DynamicMenuManageDto model)
        {
            var dboCategory = _unitOfWork.CategoryRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Include(a => a.SchemeItems)
                .First(a => a.Id == model.Id);

            dboCategory.ShowOnSite = model.ShowOnSite;
            dboCategory.ShowOnMainPageBox = model.ShowOnMainPageBox;
            dboCategory.CategoryType = model.CategoryType;
            dboCategory.DontShowOnResponsive = model.DontShowOnResponsive;
            dboCategory.ShowInSideNavigation = model.ShowInSideNavigation;
            dboCategory.SchemeSvg = model.SchemeSvg;

            if (model.WordFilePath != null)
                dboCategory.WordFilePath = model.WordFilePath;
            if (model.PdfFilePath != null)
                dboCategory.PdfFilePath = model.PdfFilePath;
            if (model.MainPhotoPath != null)
                dboCategory.MainPhotoPath = model.MainPhotoPath;
            dboCategory.SortIndex = model.SortIndex;
            dboCategory.FileUserReadableName = model.FileUserReadableName;

            if (model.SelectedParentCategoryId == 0)
                dboCategory.ParentCategoryId = null;
            else
                dboCategory.ParentCategoryId = model.SelectedParentCategoryId;

            foreach (var dboTranslation in dboCategory.Translations)
                foreach (var modelTranslation in model.Translations)
                {
                    if (dboTranslation.Id != modelTranslation.Id)
                        continue;

                    dboTranslation.Description = modelTranslation.Description;
                    dboTranslation.Name = modelTranslation.Name;
                }

            dboCategory.SchemeItems.Clear();

            if (model.SchemeItems != null)
            {
                foreach (var schemeItemViewModel in model.SchemeItems.ToList())
                {
                    dboCategory.SchemeItems.Add(new SchemeItem
                    {
                        Height = schemeItemViewModel.Height,
                        Value = schemeItemViewModel.Value,
                        Width = schemeItemViewModel.Width,
                        X = schemeItemViewModel.X,
                        Y = schemeItemViewModel.Y,
                        ConnectToId = schemeItemViewModel.ConnectToId,
                        SourceId = schemeItemViewModel.SourceId,
                        Style = schemeItemViewModel.Style
                    });
                }
            }

            if (model.Tags != null)
            {
                dboCategory.SearchTags.Clear();

                foreach (var tag in model.Tags.Split(","))
                {
                    dboCategory.SearchTags.Add(new SearchTag { Tag = tag });
                }
            }

            _unitOfWork.CategoryRepo.Update(dboCategory);
            _unitOfWork.Save();
        }

        public override IEnumerable<CategoryMenuViewModel> GetNavigation()
        {
            var categories = _unitOfWork.CategoryRepo.Get(includeProperties: "Translations,ChildCategories", filter: a => !a.IsDeleted && a.ShowOnSite).ToList();

            var model = _unitOfWork.CategoryRepo.Get(includeProperties: "Translations,ChildCategories",
                filter: a => !a.IsDeleted && a.ParentCategoryId == null && a.ShowOnSite).Select(a => new CategoryMenuViewModel
                {
                    Id = a.Id,
                    SortIndex = a.SortIndex,
                    CategoryName = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    ParentId = a.ParentCategoryId,
                    DontShowOnResponsive = a.DontShowOnResponsive,
                    ShowInSideNavigation = a.ShowInSideNavigation,
                    CategoryType = a.CategoryType,
                    Categories = RecursionHelper.GetChildren(categories, a.Id, LocalizationRetrieveHelper.CurrentLanguageToEnum())
                }).OrderBy(a => a.SortIndex).ToList();

            RecursionHelper.HieararchyWalk(model);

            foreach (var category in model)
            {
                switch (category.CategoryType)
                {
                    case CategoryType.Basic:
                        category.Controller = "About";
                        category.Action = "Work";
                        break;
                    case CategoryType.Home:
                        category.Controller = "Home";
                        category.Action = "Index";
                        break;
                    case CategoryType.Aparat:
                        category.Controller = "About";
                        category.Action = "Aparat";
                        break;
                    case CategoryType.Decisions:
                        category.Controller = "Legislation";
                        category.Action = "Decision";
                        break;
                    case CategoryType.PhotoGallery:
                        break;
                    case CategoryType.News:
                        category.Controller = "News";
                        category.Action = "Index";
                        break;
                    case CategoryType.Disciplinary:
                        category.Controller = "DisciplineJustice";
                        category.Action = "Index";
                        break;
                }
            }

            return model;
        }

        public override IEnumerable<CategoryMenuViewModel> GetSideNavigation()
        {
            var categories = _unitOfWork.CategoryRepo.Get(includeProperties: "Translations,ChildCategories", filter: a => !a.IsDeleted).ToList();

            var model = _unitOfWork.CategoryRepo.Get(includeProperties: "Translations,ChildCategories",
                filter: a => !a.IsDeleted && a.ShowInSideNavigation).Select(a => new CategoryMenuViewModel
                {
                    Id = a.Id,
                    SortIndex = a.SortIndex,
                    CategoryName = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    ParentId = a.ParentCategoryId,
                    DontShowOnResponsive = a.DontShowOnResponsive,
                    ShowInSideNavigation = a.ShowInSideNavigation,
                    CategoryType = a.CategoryType,
                    Categories = RecursionHelper.GetChildren(categories, a.Id, LocalizationRetrieveHelper.CurrentLanguageToEnum())
                }).OrderBy(a => a.SortIndex).ToList();

            RecursionHelper.HieararchyWalk(model);

            foreach (var category in model)
            {
                switch (category.CategoryType)
                {
                    case CategoryType.Basic:
                        category.Controller = "About";
                        category.Action = "Work";
                        break;
                    case CategoryType.Home:
                        category.Controller = "Home";
                        category.Action = "Index";
                        break;
                    case CategoryType.Aparat:
                        category.Controller = "About";
                        category.Action = "Aparat";
                        break;
                    case CategoryType.Decisions:
                        category.Controller = "Legislation";
                        category.Action = "Decision";
                        break;
                    case CategoryType.PhotoGallery:
                        break;
                    case CategoryType.News:
                        category.Controller = "News";
                        category.Action = "Index";
                        break;
                    case CategoryType.Disciplinary:
                        category.Controller = "DisciplineJustice";
                        category.Action = "Index";
                        break;
                }
            }

            return model;
        }

        public override CategoryMiddleStepViewModel GetCategoryMiddleLevel(int id)
        {
            var dboCategory = _unitOfWork.CategoryRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Include(a => a.ChildCategories)
                .ThenInclude(b => b.Translations)
                .Where(a => !a.IsDeleted && a.ShowOnSite)
                .First(a => a.Id == id);

            var model = new CategoryMiddleStepViewModel
            {
                Id = dboCategory.Id,
                CategoryName = dboCategory.Translations.First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                ClildCategories = dboCategory.ChildCategories
                .Where(a => !a.IsDeleted && a.ShowOnSite)
                .Select(a => new CategoryCildMiddleStep
                {
                    Id = a.Id,
                    CategoryName = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    MainPhotoPath = a.MainPhotoPath,
                    CategoryType = a.CategoryType
                }).ToList()
            };

            foreach (var category in model.ClildCategories)
            {
                switch (category.CategoryType)
                {
                    case CategoryType.Basic:
                        category.Controller = "About";
                        category.Action = "Work";
                        break;
                    case CategoryType.Home:
                        category.Controller = "Home";
                        category.Action = "Index";
                        break;
                    case CategoryType.Aparat:
                        category.Controller = "About";
                        category.Action = "Aparat";
                        break;
                    case CategoryType.Decisions:
                        category.Controller = "Legislation";
                        category.Action = "Decision";
                        break;
                    case CategoryType.PhotoGallery:
                        break;
                    case CategoryType.News:
                        category.Controller = "News";
                        category.Action = "Index";
                        break;
                    case CategoryType.Disciplinary:
                        category.Controller = "DisciplineJustice";
                        category.Action = "Index";
                        break;
                }
            }
            model.CategoryNames = CategoryNamesExtractor.Extractor(_unitOfWork, id);

            return model;
        }

        public override IEnumerable<FooterCategoryViewModel> GetFooterNavigation()
        {
            var model = _unitOfWork.CategoryRepo
                .Set()
                .Include(a => a.ChildCategories)
                .Include(a => a.Translations)
                .Where(a => !a.IsDeleted && a.ShowOnMainPageBox)
                .Select(a => new FooterCategoryViewModel
                {
                    Id = a.Id,
                    CategoryType = a.CategoryType,
                    CategoryName = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    SortIndex = a.SortIndex,
                    Categories = a.ChildCategories.ToList()
                }).ToList();

            foreach (var category in model)
            {
                switch (category.CategoryType)
                {
                    case CategoryType.Basic:
                        category.Controller = "About";
                        category.Action = "Work";
                        break;
                    case CategoryType.Home:
                        category.Controller = "Home";
                        category.Action = "Index";
                        break;
                    case CategoryType.Aparat:
                        category.Controller = "About";
                        category.Action = "Aparat";
                        break;
                    case CategoryType.Decisions:
                        category.Controller = "Legislation";
                        category.Action = "Decision";
                        break;
                    case CategoryType.PhotoGallery:
                        break;
                    case CategoryType.News:
                        category.Controller = "News";
                        category.Action = "Index";
                        break;
                    case CategoryType.Disciplinary:
                        category.Controller = "DisciplineJustice";
                        category.Action = "Index";
                        break;
                }
            }

            return model;
        }
    }
}
