﻿using Kate.Services;
using Kate.DynamicMenu.Dto;
using System.Collections.Generic;
using Kate.DynamicMenu.ViewModels;

namespace Kate.DynamicMenu
{
    public abstract class BaseDynamicMenuService : GeneralAdminService<DynamicMenuManageDto, DynamicMenuTableDto>
    {
        public abstract void FillManageDtoWithInitialData(DynamicMenuManageDto model);

        public abstract IEnumerable<CategoryMenuViewModel> GetNavigation();

        public abstract IEnumerable<CategoryMenuViewModel> GetSideNavigation();

        public abstract IEnumerable<FooterCategoryViewModel> GetFooterNavigation();

        public abstract CategoryMiddleStepViewModel GetCategoryMiddleLevel(int id);
    }
}