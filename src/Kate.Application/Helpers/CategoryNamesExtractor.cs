﻿using Kate.EntityFrameworkCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Kate.Localization;
using Kate.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Kate.Helpers
{
    static class CategoryNamesExtractor
    {
        public static string[] Extractor(ICustomUnitOfWork _unitOfWork, int categoryId)
        {
            var model = Recursion(categoryId, null, _unitOfWork, null);
            model.Reverse();
            return model.ToArray();
        }
        static private List<string> Recursion(int CategoryId, Category category, ICustomUnitOfWork _unitOfWork, List<string> names)
        {
            category = category ?? _unitOfWork.CategoryRepo.Set().Include(x => x.ParentCategory).
                Include(x=>x.Translations).
                Include(x=>x.ParentCategory.Translations).
                First(x => x.Id == CategoryId);
            if (names == null)
                names = new List<string>();
            names.Add(category.Translations.First(x => x.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name);
            if (category.ParentCategoryId == null)
                return names;
            else
            {
                return Recursion(category.ParentCategoryId.Value, category.ParentCategory, _unitOfWork, names);
            }
        }
    }
}
