﻿using System.Collections;
using System.Collections.Generic;
using Kate.BaseViewModels;

namespace Kate.Disciplinary.ViewModels
{
    public class OuterPageViewModel :  BaseViewModelForPagesWithHeaderStrip
    {
        public int Id { get; set; }

        public string PhotoPath { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SchemeSvg { get; set; }

        public List<MiddlePageDto> Disciplinaries {get; set;}
    }

    public class MiddlePageDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string PhotoPath { get; set; }
    }
}
