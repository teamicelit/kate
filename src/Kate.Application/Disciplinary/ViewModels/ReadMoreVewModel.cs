﻿using System;
using System.Collections.Generic;
using System.Text;
using Kate.BaseViewModels;

namespace Kate.Disciplinary.ViewModels
{
    public class ReadMoreVewModel : BaseViewModelForPagesWithHeaderStrip
    {
        public string Content { get; set; }
    }
}
