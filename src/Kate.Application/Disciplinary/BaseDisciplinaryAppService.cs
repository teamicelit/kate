﻿using Kate.Disciplinary.Dto;
using Kate.Disciplinary.ViewModels;
using Kate.Services;
using System.Collections.Generic;

namespace Kate.Disciplinary
{
    public abstract class BaseDisciplinaryAppService : GeneralAdminService<DisciplinaryManageDto, DisciplinaryTableDto>
    {
        public abstract void FillManageDtoWithInitialData(DisciplinaryManageDto model);

        public abstract OuterPageViewModel GetDisciplinary(int id);

        public abstract ReadMoreVewModel GetReadMore(int id);
    }
}
