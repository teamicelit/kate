﻿using System;
using System.Collections.Generic;
using System.Text;
using Kate.Disciplinary.Dto;
using Kate.EntityFrameworkCore.Repositories;
using System.Linq;
using Kate.Localization;
using Microsoft.EntityFrameworkCore;
using Kate.EntityFrameworkCore.Entities;
using Kate.Disciplinary.ViewModels;
using Kate.Helpers;

namespace Kate.Disciplinary
{
    public class DisciplinaryAppService : BaseDisciplinaryAppService
    {
        private readonly ICustomUnitOfWork _unitOfWork;

        public DisciplinaryAppService(ICustomUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override void Delete(int id)
        {
            var dboDisciplinary = _unitOfWork.DisciplinaryRepo.GetById(id);

            dboDisciplinary.IsDeleted = true;

            _unitOfWork.DisciplinaryRepo.Update(dboDisciplinary);
            _unitOfWork.Save();
        }

        public override void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public override void FillManageDtoWithInitialData(DisciplinaryManageDto model)
        {
            model.Dropdown = _unitOfWork.CategoryRepo.Set()
               .Where(a => !a.IsDeleted)
               .Select(a => new DisciplinaryDropdownDto
               {
                   Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                   Id = a.Id,
                   IsSelected = a.Id == model.SelectedCategoryId
               }).ToList();
        }

        public override OuterPageViewModel GetDisciplinary(int id)
        {
            var model = _unitOfWork.CategoryRepo
                .Set()
                .Where(a => !a.IsDeleted && a.Id == id)
                .Include(a => a.Translations)
                .Include(a => a.Disciplinaries)
                .Select(a => new OuterPageViewModel
                {
                    Id = id,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Description = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Description,
                    PhotoPath = a.MainPhotoPath,
                    Disciplinaries = a.Disciplinaries.Select(b => new MiddlePageDto
                    {
                        Id = b.Id,
                        Name = b.Translations.First(c => c.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                        PhotoPath = b.MainPhotoPath
                    }).ToList(),
                    SchemeSvg = a.SchemeSvg
                }).First();
            model.CategoryNames = CategoryNamesExtractor.Extractor(_unitOfWork, id);
            return model;
        }

        public override ReadMoreVewModel GetReadMore(int id)
        {
            var dbEntity = _unitOfWork.DisciplinaryRepo.Set()
              .Where(a => !a.IsDeleted)
              .Include(a => a.Translations).FirstOrDefault(a => a.Id == id);
            var model = new ReadMoreVewModel
            {
                Content = dbEntity              
              .Translations
              .First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum())
              .Description
            };
            model.CategoryNames = CategoryNamesExtractor.Extractor(_unitOfWork, dbEntity.CategoryId);
            return model;
        }

        public override IEnumerable<DisciplinaryTableDto> GetTableViewModels()
        {
            var model = _unitOfWork.DisciplinaryRepo.Set()
                  .Include(a => a.Translations)
                  .Where(a => !a.IsDeleted)
                  .Select(a => new DisciplinaryTableDto
                  {
                      Id = a.Id,
                      FileUserReadableName = a.FileUserReadableName,
                      MainPhotoPath = a.MainPhotoPath,
                      CategoryName = a.Category.Translations.FirstOrDefault(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name ?? "",
                      PdfFilePath = a.PdfFilePath,
                      ShowOnSite = a.ShowOnSite,
                      WordFilePath = a.WordFilePath,
                      SortIndex = a.SortIndex,
                      Translation = a.Translations.Select(b => new DisciplinaryTranslationDto
                      {
                          Id = b.Id,
                          LanguageId = b.LanguageId,
                          Description = b.Description,
                          Name = b.Name
                      }).First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum())
                  }).OrderByDescending(a => a.Id).ToList();

            return model;
        }

        public override DisciplinaryManageDto GetViewModel(int id)
        {
            var model = new DisciplinaryManageDto
            {
                Translations = new List<DisciplinaryTranslationDto>
                {
                    new DisciplinaryTranslationDto {LanguageId = 1},
                    new DisciplinaryTranslationDto {LanguageId = 2},
                    new DisciplinaryTranslationDto {LanguageId = 3},
                    new DisciplinaryTranslationDto {LanguageId = 4}
                },
                Dropdown = _unitOfWork.CategoryRepo.Set()
                .Select(a => new DisciplinaryDropdownDto
                {
                    Id = a.Id,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name
                })
            };

            if (id == 0)
                return model;

            var dboDisciplinary = _unitOfWork.DisciplinaryRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .First(a => a.Id == id);

            model.Id = dboDisciplinary.Id;
            model.FileUserReadableName = dboDisciplinary.FileUserReadableName;
            model.MainPhotoPath = dboDisciplinary.MainPhotoPath;
            model.PdfFilePath = dboDisciplinary.PdfFilePath;
            model.WordFilePath = dboDisciplinary.WordFilePath;
            model.ShowOnSite = dboDisciplinary.ShowOnSite;
            model.SelectedCategoryId = dboDisciplinary.CategoryId;
            model.SortIndex = dboDisciplinary.SortIndex;

            model.Translations = dboDisciplinary.Translations
                .Select(a => new DisciplinaryTranslationDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    Description = a.Description,
                    LanguageId = a.LanguageId
                }).ToList();

            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new DisciplinaryDropdownDto
                {
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Id = a.Id,
                    IsSelected = a.Id == model.SelectedCategoryId
                }).ToList();

            return model;
        }

        public override void Save(DisciplinaryManageDto model)
        {
            if (model.Id == 0)
                Create(model);
            else
                Update(model);
        }

        protected override void Create(DisciplinaryManageDto model)
        {
            var disciplinary = new Kate.EntityFrameworkCore.Entities.Disciplinary
            {
                ShowOnSite = model.ShowOnSite,
                WordFilePath = model.WordFilePath,
                PdfFilePath = model.PdfFilePath,
                FileUserReadableName = model.FileUserReadableName,
                SortIndex = model.SortIndex,
                MainPhotoPath = model.MainPhotoPath
            };

            if (model.SelectedCategoryId != 0)
                disciplinary.CategoryId = model.SelectedCategoryId;

            foreach (var translation in model.Translations)
                disciplinary.Translations.Add(new DisciplinaryTranslation
                {
                    LanguageId = translation.LanguageId,
                    Description = translation.Description,
                    Name = translation.Name
                });

            _unitOfWork.DisciplinaryRepo.Insert(disciplinary);
            _unitOfWork.Save();
        }

        protected override void Update(DisciplinaryManageDto model)
        {
            var dboDisciplinary = _unitOfWork.DisciplinaryRepo.Set()
                   .Include(a => a.Translations)
                   .Include(a => a.SearchTags)
                   .First(a => a.Id == model.Id);

            dboDisciplinary.ShowOnSite = model.ShowOnSite;
            dboDisciplinary.SortIndex = model.SortIndex;
            if (model.WordFilePath != null)
                dboDisciplinary.WordFilePath = model.WordFilePath;
            if (model.PdfFilePath != null)
                dboDisciplinary.PdfFilePath = model.PdfFilePath;
            if (model.MainPhotoPath != null)
                dboDisciplinary.MainPhotoPath = model.MainPhotoPath;
            dboDisciplinary.FileUserReadableName = model.FileUserReadableName;
            dboDisciplinary.CategoryId = model.SelectedCategoryId;

            foreach (var dboTranslation in dboDisciplinary.Translations)
                foreach (var modelTranslation in model.Translations)
                {
                    if (dboTranslation.Id != modelTranslation.Id)
                        continue;

                    dboTranslation.Description = modelTranslation.Description;
                    dboTranslation.Name = modelTranslation.Name;
                }

            _unitOfWork.DisciplinaryRepo.Update(dboDisciplinary);
            _unitOfWork.Save();
        }
    }
}