﻿namespace Kate.Disciplinary.Dto
{
    public class DisciplinaryTableDto
    {
        public int Id { get; set; }
        public int SortIndex { get; set; }

        public string CategoryName { get; set; }
        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }

        public bool ShowOnSite { get; set; }
        
        public DisciplinaryTranslationDto Translation { get; set; }
    }
}
