﻿using System.Collections.Generic;

namespace Kate.Disciplinary.Dto
{
    public class DisciplinaryManageDto
    {
        public int Id { get; set; }
        public int SelectedCategoryId { get; set; }
        public int SortIndex { get; set; }

        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }

        public bool ShowOnSite { get; set; }

        public IEnumerable<DisciplinaryDropdownDto> Dropdown { get; set; }
        public IEnumerable<DisciplinaryTranslationDto> Translations { get; set; }
    }
}
