﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Kate.Configuration.Dto;

namespace Kate.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : KateAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
