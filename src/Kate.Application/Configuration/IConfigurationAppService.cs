﻿using System.Threading.Tasks;
using Kate.Configuration.Dto;

namespace Kate.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
