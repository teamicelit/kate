﻿using Kate.Services.GenericDtos;
using System.Collections.Generic;

namespace Kate.PhotoVideoGallery.Dto
{
   public class PhotoVideoGalleryTableDto
    {
        public int Id { get; set; }
        
        public string CategoryName { get; set; }

        public PhotoVideoGalleryTranslationDto Translation { get; set; }

        public IEnumerable<SearchTagDto> SearchTags { get; set; }

    }
}
