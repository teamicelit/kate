﻿using Kate.EntityFrameworkCore.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.PhotoVideoGallery.Dto
{
    public class PhotoVideoGalleryFileDto
    {
        public string FilePath { get; set; }
        public string Embed { get; set; }        

        public FileType FileType { get; set; }

        public IFormFile File { get; set; }
    }
}
