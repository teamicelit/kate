﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.PhotoVideoGallery.Dto
{
   public class PhotoVideoGalleryManageDto
    {
        public int Id { get; set; }
        public int SelectedCategoryId { get; set; }

        public string Tags { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }

        public DateTime GalleryDate { get; set; }

        public IEnumerable<PhotoVideoGalleryDropdownDto> Dropdown { get; set; }
        public IEnumerable<PhotoVideoGalleryTranslationDto> Translations { get; set; }
        public IEnumerable<PhotoVideoGalleryFileDto> Files { get; set; }

    }
}
