﻿using Kate.PhotoVideoGallery.Dto;
using Kate.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.PhotoVideoGallery
{
    public abstract class BasePhotoVideoGalleryAppService : GeneralAdminService<PhotoVideoGalleryManageDto, PhotoVideoGalleryTableDto>
    {
    }
}
