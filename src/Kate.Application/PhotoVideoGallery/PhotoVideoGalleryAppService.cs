﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kate.EntityFrameworkCore.Repositories;
using Kate.Localization;
using Kate.PhotoVideoGallery.Dto;
using Microsoft.EntityFrameworkCore;
using Kate.EntityFrameworkCore.Entities;
using Kate.Services.GenericDtos;

namespace Kate.PhotoVideoGallery
{
    public class PhotoVideoGalleryAppService : BasePhotoVideoGalleryAppService
    {
        private readonly ICustomUnitOfWork _unitOfWork;

        public PhotoVideoGalleryAppService(ICustomUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override void Delete(int id)
        {
            var dboPhotoVideoGallery = _unitOfWork.PhotoVideoGalleryRepo.GetById(id);

            dboPhotoVideoGallery.IsDeleted = true;

            _unitOfWork.PhotoVideoGalleryRepo.Update(dboPhotoVideoGallery);
            _unitOfWork.Save();
        }

        public override void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public override IEnumerable<PhotoVideoGalleryTableDto> GetTableViewModels()
        {
            var model =
                _unitOfWork.PhotoVideoGalleryRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Where(a => !a.IsDeleted)
                .Select(a => new PhotoVideoGalleryTableDto
                {
                    Id = a.Id,
                    CategoryName = a.Category.Translations.FirstOrDefault(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name ?? "",
                    Translation = a.Translations.Select(b => new PhotoVideoGalleryTranslationDto
                    {
                        Id = b.Id,
                        LanguageId = b.LanguageId,
                        Description = b.Description,
                        Name = b.Name
                    }).First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()),
                    SearchTags = a.SearchTags.Select(b => new SearchTagDto { Id = b.Id, Tag = b.Tag })
                }).OrderByDescending(a => a.Id).ToList();

            return model;
        }

        public override PhotoVideoGalleryManageDto GetViewModel(int id)
        {
            var model = new PhotoVideoGalleryManageDto
            {
                Translations = new List<PhotoVideoGalleryTranslationDto>
                {
                    new PhotoVideoGalleryTranslationDto {LanguageId = 1},
                    new PhotoVideoGalleryTranslationDto {LanguageId = 2},
                    new PhotoVideoGalleryTranslationDto {LanguageId = 3},
                    new PhotoVideoGalleryTranslationDto {LanguageId = 4}
                },
                Dropdown = _unitOfWork.CategoryRepo.Set()
                .Select(a => new PhotoVideoGalleryDropdownDto
                {
                    Id = a.Id,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name
                })
            };

            if (id == 0)
                return model;


             
            var dboPhotoVideoGallery = _unitOfWork.PhotoVideoGalleryRepo
                .Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Include(a=> a.GalleryFiles).First(x => x.Id == id);

            model.SelectedCategoryId = dboPhotoVideoGallery.CategoryId;
            model.ShowOnMainPageBox = dboPhotoVideoGallery.ShowOnMainPageBox;
            model.ShowOnSite = dboPhotoVideoGallery.ShowOnSite;
            model.Tags = string.Join(",", dboPhotoVideoGallery.SearchTags.Select(a => a.Tag).ToList());

            model.Translations = dboPhotoVideoGallery.Translations
                 .Select(a => new PhotoVideoGalleryTranslationDto
                 {
                     Id = a.Id,
                     Name = a.Name,
                     Description = a.Description,
                     LanguageId = a.LanguageId
                 }).ToList();

            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                 .Where(a => !a.IsDeleted)
                 .Select(a => new PhotoVideoGalleryDropdownDto
                 {
                     Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                     Id = a.Id,
                     IsSelected = a.Id == model.SelectedCategoryId
                 }).ToList();

            model.Files = dboPhotoVideoGallery.GalleryFiles
                .Select(a => new PhotoVideoGalleryFileDto
                {
                    Embed = a.Embed,
                    FilePath = a.FilePath,
                    FileType = a.Type
                });

            return model;
        }

        public override void Save(PhotoVideoGalleryManageDto model)
        {
            if (model.Id == 0)
                Create(model);
            else
                Update(model);
        }

        protected override void Create(PhotoVideoGalleryManageDto model)
        {
            var PhotoVideoGallery = new EntityFrameworkCore.Entities.PhotoVideoGallery
            {
                ShowOnSite = model.ShowOnSite,
                ShowOnMainPageBox = model.ShowOnMainPageBox,
                GalleryDate = model.GalleryDate,
                CategoryId = model.SelectedCategoryId,
            };

            foreach (var translation in model.Translations)
                PhotoVideoGallery.Translations.Add(new PhotoVideoGalleryTranslation
                {
                    LanguageId = translation.LanguageId,
                    Description = translation.Description,
                    Name = translation.Name
                });

            var tags = model.Tags.Split(",");

            foreach (var tag in tags)
            {
                PhotoVideoGallery.SearchTags.Add(new SearchTag { Tag = tag });
            }

            foreach (var File in model.Files)
            {
                PhotoVideoGallery.GalleryFiles.Add(new GalleryFile()
                {
                    FilePath = File.FilePath,
                    Embed = File.Embed,
                    Type = File.FileType
                });
            }
            _unitOfWork.PhotoVideoGalleryRepo.Insert(PhotoVideoGallery);
            _unitOfWork.Save();

        }

        protected override void Update(PhotoVideoGalleryManageDto model)
        {
            var dboPhotoVideoGallery = _unitOfWork.PhotoVideoGalleryRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Include(a => a.GalleryFiles)
                .First();

            dboPhotoVideoGallery.ShowOnSite = model.ShowOnSite;
            dboPhotoVideoGallery.ShowOnMainPageBox = model.ShowOnMainPageBox;
            dboPhotoVideoGallery.GalleryDate = model.GalleryDate;
            dboPhotoVideoGallery.CategoryId = model.SelectedCategoryId;

            foreach (var dboTranslation in dboPhotoVideoGallery.Translations)
            {
                foreach (var modelTranslation in dboPhotoVideoGallery.Translations)
                {
                    if (dboTranslation.Id != modelTranslation.Id)
                        continue;

                    dboTranslation.Description = modelTranslation.Description;
                    dboTranslation.Name = modelTranslation.Name;
                }
            }

            dboPhotoVideoGallery.SearchTags.Clear();

            foreach (var tag in model.Tags.Split(","))
            {
                dboPhotoVideoGallery.SearchTags.Add(new SearchTag { Tag = tag });
            }

            dboPhotoVideoGallery.GalleryFiles.Clear();

            foreach (var file in model.Files)
            {
                dboPhotoVideoGallery.GalleryFiles.Add(new GalleryFile
                {
                    FilePath = file.FilePath,
                    Type = file.FileType,
                    Embed = file.Embed
                });
            }

            _unitOfWork.PhotoVideoGalleryRepo.Update(dboPhotoVideoGallery);
            _unitOfWork.Save();
        }
    }
}
