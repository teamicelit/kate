using System.ComponentModel.DataAnnotations;

namespace Kate.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}