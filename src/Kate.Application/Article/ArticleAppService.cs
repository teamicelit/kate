﻿using System;
using System.Collections.Generic;
using Kate.EntityFrameworkCore.Entities;
using Kate.Article.Dto;
using Kate.EntityFrameworkCore.Repositories;
using System.Linq;
using Kate.Localization;
using Microsoft.EntityFrameworkCore;
using Kate.Services.GenericDtos;
using Kate.Article.ViewModels;
using Kate.Helpers;
using Kate.Search.ViewModels;
using Kate.Decision.ViewModels;

namespace Kate.Article
{
    public class ArticleAppService : BaseArticleAppService
    {
        private readonly ICustomUnitOfWork _unitOfWork;

        public ArticleAppService(ICustomUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override void Delete(int id)
        {
            var dboArticle = _unitOfWork.ArticleRepo.GetById(id);

            dboArticle.IsDeleted = true;

            _unitOfWork.ArticleRepo.Update(dboArticle);
            _unitOfWork.Save();
        }

        public override void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public override void FillManageDtoWithInitialData(ArticleManageDto model)
        {
            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new ArticleDropdownDto
                {
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Id = a.Id,
                    IsSelected = a.Id == model.SelectedCategoryId
                }).ToList();
        }

        public override ReadMoreViewModel GetNews(int id)
        {
            var dboArticle = _unitOfWork.ArticleRepo.Set()
                .Where(a => !a.IsDeleted)
                .Include(a => a.Translations)
                .FirstOrDefault(a => a.Id == id);

            var model = new ReadMoreViewModel
            {
                Id = dboArticle.Id,
                PhotoPath = dboArticle.MainPhotoPath,
                Name = dboArticle.Translations.First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                Description = dboArticle.Translations.First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Description,
                ShortDescription = dboArticle.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).ShortDescription
            };
            model.CategoryNames = CategoryNamesExtractor.Extractor(_unitOfWork, dboArticle.CategoryId);
            return model;
        }

        public override GetNewsFeedModel GetNewsFeed(int id)
        {

            if (id == 0)
            {
                var model = new GetNewsFeedModel { Data = GetNewsFeedForHomePage() };

                return model;
            }

            else
                return new GetNewsFeedModel
                {
                    Data = GetNewsFeedForNewsPage(id),
                    CategoryNames = CategoryNamesExtractor.Extractor(_unitOfWork, id)
                };
        }

        public override IEnumerable<NewsFeedViewModel> GetNewsFeedForHomePage()
        {
            var model = _unitOfWork.ArticleRepo.Set()
                .Where(a => !a.IsDeleted && a.ShowOnSite)
                .Include(a => a.Translations)
                .Select(a => new NewsFeedViewModel
                {
                    Id = a.Id,
                    CreateDate = a.CreateDate,
                    Description = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Description,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    ShortDescription = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).ShortDescription,
                    PhotoName = a.MainPhotoPath,
                    SortIndex = a.SortIndex
                }).OrderByDescending(a => a.SortIndex).ToList();

            return model;
        }

        public override IEnumerable<NewsFeedViewModel> GetNewsFeedForNewsPage(int id)
        {
            var model = _unitOfWork.ArticleRepo.Set()
                .Where(a => !a.IsDeleted && a.CategoryId == id && a.ShowOnSite)
                .Include(a => a.Translations)
                .Select(a => new NewsFeedViewModel
                {
                    Id = a.Id,
                    CreateDate = a.CreateDate,
                    Description = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Description,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    ShortDescription = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).ShortDescription,
                    PhotoName = a.MainPhotoPath,
                    SortIndex = a.SortIndex
                }).OrderByDescending(a => a.SortIndex).ToList();

            return model;
        }

        public override IEnumerable<ArticleTableDto> GetTableViewModels()
        {
            var model = _unitOfWork.ArticleRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Where(a => !a.IsDeleted)
                .Select(a => new ArticleTableDto
                {
                    Id = a.Id,
                    IsNews = a.IsNews,
                    ShowInMainSlider = a.ShowInMainSlider,
                    FileUserReadableName = a.FileUserReadableName,
                    MainPhotoPath = a.MainPhotoPath,
                    CategoryName = a.Category.Translations.FirstOrDefault(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name ?? "",
                    PdfFilePath = a.PdfFilePath,
                    ShowOnMainPageBox = a.ShowOnMainPageBox,
                    ShowOnSite = a.ShowOnSite,
                    SortIndex = a.SortIndex,
                    CreateDate = a.CreateDate,
                    VideoEmbed = a.VideoEmbed,
                    WordFilePath = a.WordFilePath,
                    Translation = a.Translations.Select(b => new ArticleTranslationDto
                    {
                        Id = b.Id,
                        LanguageId = b.LanguageId,
                        Description = b.Description,
                        Name = b.Name,
                        ShortDescription = b.ShortDescription
                    }).First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()),
                    SearchTags = a.SearchTags.Select(b => new SearchTagDto { Id = b.Id, Tag = b.Tag })
                }).OrderByDescending(a => a.Id).ToList();

            return model;
        }

        public override ArticleManageDto GetViewModel(int id)
        {
            var model = new ArticleManageDto
            {
                Translations = new List<ArticleTranslationDto>
                {
                    new ArticleTranslationDto {LanguageId = 1},
                    new ArticleTranslationDto {LanguageId = 2},
                    new ArticleTranslationDto {LanguageId = 3},
                    new ArticleTranslationDto {LanguageId = 4}
                },
                Dropdown = _unitOfWork.CategoryRepo.Set()
                .Select(a => new ArticleDropdownDto
                {
                    Id = a.Id,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name
                })
            };

            if (id == 0)
                return model;

            var dboArticle = _unitOfWork.ArticleRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .First(a => a.Id == id);

            model.Id = dboArticle.Id;
            model.CreateDate = dboArticle.CreateDate;
            model.FileUserReadableName = dboArticle.FileUserReadableName;
            model.MainPhotoPath = dboArticle.MainPhotoPath;
            model.PdfFilePath = dboArticle.PdfFilePath;
            model.WordFilePath = dboArticle.WordFilePath;
            model.SortIndex = dboArticle.SortIndex;
            model.ShowOnMainPageBox = dboArticle.ShowOnMainPageBox;
            model.VideoEmbed = dboArticle.VideoEmbed;
            model.ShowOnSite = dboArticle.ShowOnSite;
            model.ShowInMainSlider = dboArticle.ShowInMainSlider;
            model.IsNews = dboArticle.IsNews;
            model.SelectedCategoryId = dboArticle.CategoryId;
            model.Tags = string.Join(",", dboArticle.SearchTags.Select(a => a.Tag).ToList());

            model.Translations = dboArticle.Translations
                .Select(a => new ArticleTranslationDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    Description = a.Description,
                    LanguageId = a.LanguageId,
                    ShortDescription = a.ShortDescription
                }).ToList();

            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new ArticleDropdownDto
                {
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Id = a.Id,
                    IsSelected = a.Id == model.SelectedCategoryId
                }).ToList();

            return model;
        }

        public override IEnumerable<Search.ViewModels.SearchViewModel> GlobalSearch(string search, bool OnlyArticle = false)
        {
            var articles = _unitOfWork.ArticleRepo.Set()
                 .Where(x => !x.IsDeleted && (x.Translations.Any(y => y.Name.Contains(search)
                 || y.Description.Contains(search)))).Select(a => new NewsFeedViewModel
                 {
                     Id = a.Id,
                     CreateDate = a.CreateDate,
                     Description = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Description,
                     Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                     PhotoName = a.MainPhotoPath,
                     SortIndex = a.SortIndex
                 }).OrderBy(a => a.SortIndex).ThenByDescending(x => x.Id).ToList();

            var decisions = new List<DecisionViewModel>();
            if (!OnlyArticle)
            {
                decisions = _unitOfWork.DecisionRepo.Set()
                     .Include(a => a.Translations)
                     .Include(a => a.SearchTags)
                     .Where(a => !a.IsDeleted && (
                     a.SearchTags.Any(x => x.Tag.Contains(search)) ||
                     a.Translations.Any(x => x.Name.Contains(search) ||
                     x.Description.Contains(search)))).Select(a => new DecisionViewModel
                     {
                         Id = a.Id,
                         Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                         Description = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Description,
                         PhotoName = a.ComplaintPicturePhotoPath,
                         DocumentCameInAction = a.DocumentCameInAction,
                         PdfFilePath = a.PdfFilePath,
                         LinkToFile = a.LinkToFile,
                         WordFilePath = a.WordFilePath
                     }).OrderByDescending(a => a.Id).ToList();
            }
            var CastedArticles = articles.Select(x => new Search.ViewModels.SearchViewModel { Article = x }).ToList();
            var CastedDecisions = decisions.Select(x => new Search.ViewModels.SearchViewModel { Dicision = x }).ToList();

            var result = new List<Search.ViewModels.SearchViewModel>();

            var ArticleEndFlag = false;
            var DecisionsEndFlag = false;
            var indexer = 0;
            while (true)
            {
                if (CastedArticles.Count > indexer)
                {
                    result.Add(CastedArticles[indexer]);
                }
                else
                {
                    ArticleEndFlag = true;
                }

                if (CastedDecisions.Count > indexer)
                {
                    result.Add(CastedDecisions[indexer]);
                }
                else
                {
                    DecisionsEndFlag = true;
                }
                indexer++;
                if (ArticleEndFlag && DecisionsEndFlag)
                    break;
            }

            return result;

        }

        public override void Save(ArticleManageDto model)
        {
            if (model.Id == 0)
                Create(model);
            else
                Update(model);
        }

        protected override void Create(ArticleManageDto model)
        {
            var article = new Kate.EntityFrameworkCore.Entities.Article
            {
                ShowOnSite = model.ShowOnSite,
                ShowOnMainPageBox = model.ShowOnMainPageBox,
                WordFilePath = model.WordFilePath,
                PdfFilePath = model.PdfFilePath,
                MainPhotoPath = model.MainPhotoPath,
                FileUserReadableName = model.FileUserReadableName,
                ShowInMainSlider = model.ShowInMainSlider,
                IsNews = model.IsNews,
                VideoEmbed = model.VideoEmbed,
                SortIndex = model.SortIndex,
                CreateDate = DateTime.Now
            };

            if (model.SelectedCategoryId != 0)
                article.CategoryId = model.SelectedCategoryId;

            foreach (var translation in model.Translations)
                article.Translations.Add(new ArticleTranslation
                {
                    LanguageId = translation.LanguageId,
                    Description = translation.Description,
                    Name = translation.Name,
                    ShortDescription = translation.ShortDescription
                });

            if (model.Tags != null)
            {
                var tags = model.Tags.Split(",");

                foreach (var tag in tags)
                {
                    article.SearchTags.Add(new SearchTag { Tag = tag });
                }
            }

            _unitOfWork.ArticleRepo.Insert(article);
            _unitOfWork.Save();
        }

        protected override void Update(ArticleManageDto model)
        {
            var dboArticle = _unitOfWork.ArticleRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .First(a => a.Id == model.Id);

            dboArticle.ShowOnSite = model.ShowOnSite;
            dboArticle.ShowOnMainPageBox = model.ShowOnMainPageBox;
            dboArticle.VideoEmbed = model.VideoEmbed;
            dboArticle.ShowInMainSlider = model.ShowInMainSlider;
            dboArticle.IsNews = model.IsNews;
            if (model.WordFilePath != null)
                dboArticle.WordFilePath = model.WordFilePath;
            if (model.PdfFilePath != null)
                dboArticle.PdfFilePath = model.PdfFilePath;
            if (model.MainPhotoPath != null)
                dboArticle.MainPhotoPath = model.MainPhotoPath;
            dboArticle.FileUserReadableName = model.FileUserReadableName;
            dboArticle.CategoryId = model.SelectedCategoryId;
            dboArticle.SortIndex = model.SortIndex;

            foreach (var dboTranslation in dboArticle.Translations)
                foreach (var modelTranslation in model.Translations)
                {
                    if (dboTranslation.Id != modelTranslation.Id)
                        continue;

                    dboTranslation.Description = modelTranslation.Description;
                    dboTranslation.Name = modelTranslation.Name;
                    dboTranslation.ShortDescription = modelTranslation.ShortDescription;
                }

            if (model.Tags != null)
            {
                dboArticle.SearchTags.Clear();

                foreach (var tag in model.Tags.Split(","))
                {
                    dboArticle.SearchTags.Add(new SearchTag { Tag = tag });
                }
            }

            _unitOfWork.ArticleRepo.Update(dboArticle);
            _unitOfWork.Save();
        }
    }
}
