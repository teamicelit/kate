﻿using Kate.Article.Dto;
using Kate.Article.ViewModels;
using Kate.Decision.ViewModels;
using Kate.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Article
{
    public abstract class BaseArticleAppService : GeneralAdminService<ArticleManageDto, ArticleTableDto>
    {
        public abstract void FillManageDtoWithInitialData(ArticleManageDto model);

        public abstract GetNewsFeedModel GetNewsFeed(int id);

        public abstract IEnumerable<NewsFeedViewModel> GetNewsFeedForHomePage();

        public abstract IEnumerable<NewsFeedViewModel> GetNewsFeedForNewsPage(int id);

        public abstract ReadMoreViewModel GetNews(int id);
        public abstract IEnumerable<Search.ViewModels.SearchViewModel> GlobalSearch(string search, bool OnlyArticle = false);
    }
}
