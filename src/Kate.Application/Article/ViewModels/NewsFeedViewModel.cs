﻿using System;

namespace Kate.Article.ViewModels
{
    public class NewsFeedViewModel
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        public int SortIndex { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string PhotoName { get; set; }
    }
}