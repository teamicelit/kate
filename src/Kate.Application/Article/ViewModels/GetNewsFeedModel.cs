﻿using Kate.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Article.ViewModels
{
    public class GetNewsFeedModel : BaseViewModelForPagesWithHeaderStrip
    {
        public IEnumerable<NewsFeedViewModel> Data { get; set; }
    }
}
