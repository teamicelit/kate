﻿using Kate.BaseViewModels;

namespace Kate.Article.ViewModels
{
    public class ReadMoreViewModel : BaseViewModelForPagesWithHeaderStrip
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string PhotoPath { get; set; }
    }
}
