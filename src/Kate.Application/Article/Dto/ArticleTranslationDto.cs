﻿using Kate.Services.GenericDtos;

namespace Kate.Article.Dto
{
    public class ArticleTranslationDto : StandartTranslationDto
    {
        public string ShortDescription { get; set; }
    }
}
