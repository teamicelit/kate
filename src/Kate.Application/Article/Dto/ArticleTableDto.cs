﻿using Kate.Services.GenericDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Article.Dto
{
    public class ArticleTableDto
    {
        public int Id { get; set; }
        public int SortIndex { get; set; }

        public DateTime CreateDate { get; set; }

        public string CategoryName { get; set; }
        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }
        public string VideoEmbed { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }
        public bool ShowInMainSlider { get; set; }
        public bool IsNews { get; set; }

        public ArticleTranslationDto Translation { get; set; }

        public IEnumerable<SearchTagDto> SearchTags { get; set; }
    }
}
