﻿using System;
using System.Collections.Generic;

namespace Kate.Article.Dto
{
    public class ArticleManageDto
    {
        public int Id { get; set; }
        public int SelectedCategoryId { get; set; }
        public int SortIndex { get; set; }

        public DateTime CreateDate { get; set; }

        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }
        public string VideoEmbed { get; set; }
        public string Tags { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }
        public bool ShowInMainSlider { get; set; }
        public bool IsNews { get; set; }

        public IEnumerable<ArticleDropdownDto> Dropdown { get; set; }
        public IEnumerable<ArticleTranslationDto> Translations { get; set; }
    }
}
