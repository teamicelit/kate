﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Kate.Sessions.Dto;

namespace Kate.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
