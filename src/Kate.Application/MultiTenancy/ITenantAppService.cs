﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Kate.MultiTenancy.Dto;

namespace Kate.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
