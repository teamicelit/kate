﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Kate.AboutUs.Dto;
using Kate.Authorization;
using Kate.InnerComplaint.Dto;

namespace Kate
{
    [DependsOn(
        typeof(KateCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class KateApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<KateAuthorizationProvider>();
            Configuration.Modules.AbpAutoMapper().Configurators.Add(config =>
            {
                config.CreateMap<AboutUsDto, EntityFrameworkCore.Entities.AboutUs>();
                config.CreateMap<InnerComplaintDto, EntityFrameworkCore.Entities.InnerComplaint>();
            });
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(KateApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );


        }
    }
}
