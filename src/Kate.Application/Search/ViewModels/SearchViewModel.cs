﻿using Kate.Article.ViewModels;
using Kate.Decision.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Search.ViewModels
{
    public class SearchViewModel
    {
        public NewsFeedViewModel Article { get; set; }
        public DecisionViewModel Dicision { get; set; }
    }
}
