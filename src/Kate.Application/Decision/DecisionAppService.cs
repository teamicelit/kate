﻿using System;
using System.Collections.Generic;
using System.Text;
using Kate.Decision.Dto;
using Kate.EntityFrameworkCore.Repositories;
using System.Linq;
using Kate.Localization;
using Kate.Services.GenericDtos;
using Microsoft.EntityFrameworkCore;
using Kate.EntityFrameworkCore.Entities;
using Kate.Decision.ViewModels;
using Kate.Helpers;

namespace Kate.Decision
{
    public class DecisionAppService : BaseDecisionAppService
    {
        private readonly ICustomUnitOfWork _unitOfWork;

        public DecisionAppService(ICustomUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override void Delete(int id)
        {
            var dboDecision = _unitOfWork.DecisionRepo.GetById(id);

            dboDecision.IsDeleted = true;

            _unitOfWork.DecisionRepo.Update(dboDecision);
            _unitOfWork.Save();
        }

        public override void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public override void FillManageDtoWithInitialData(DecisionManageDto model)
        {
            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new DecisionDropdownDto
                {
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Id = a.Id,
                    IsSelected = a.Id == model.SelectedCategoryId
                }).ToList();
        }

        public override DecisionsViewModel GetDecisionViewModels(int CategoryId)
        {
            var Decisions = _unitOfWork.DecisionRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Where(a => !a.IsDeleted && a.CategoryId == CategoryId)
                .Select(a => new DecisionViewModel
                {
                    Id = a.Id,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Description = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Description,
                    PhotoName = a.ComplaintPicturePhotoPath,
                    DocumentCameInAction = a.DocumentCameInAction,
                    SortIndex = a.SortIndex,
                    PdfFilePath = a.PdfFilePath,
                    LinkToFile = a.LinkToFile,
                    WordFilePath = a.WordFilePath
                }).OrderBy(a => a.SortIndex).ThenByDescending(a => a.Id).ToList();

            var model = new DecisionsViewModel() { Decisions = Decisions };
            model.CategoryNames = CategoryNamesExtractor.Extractor(_unitOfWork, CategoryId);
            return model;

        }

        public override IEnumerable<DecisionTableDto> GetTableViewModels()
        {
            var model = _unitOfWork.DecisionRepo.Set()
                 .Include(a => a.Translations)
                 .Include(a => a.SearchTags)
                 .Where(a => !a.IsDeleted)
                 .Select(a => new DecisionTableDto
                 {
                     Id = a.Id,
                     ShowOnMainPage = a.ShowOnMainPage,
                     FileUserReadableName = a.FileUserReadableName,
                     MainPhotoPath = a.ComplaintPicturePhotoPath,
                     CategoryName = a.Category.Translations.FirstOrDefault(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name ?? "",
                     PdfFilePath = a.PdfFilePath,
                     ShowOnMainPageBox = a.ShowOnMainPageBox,
                     ShowOnSite = a.ShowOnSite,
                     SortIndex = a.SortIndex,
                     LinkToFile = a.LinkToFile,
                     DecisionDate = a.DocumentCameInAction,
                     WordFilePath = a.WordFilePath,
                     Translation = a.Translations.Select(b => new DecisionTranslationDto
                     {
                         Id = b.Id,
                         LanguageId = b.LanguageId,
                         Description = b.Description,
                         Name = b.Name
                     }).First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()),
                     SearchTags = a.SearchTags.Select(b => new SearchTagDto { Id = b.Id, Tag = b.Tag })
                 }).OrderByDescending(a => a.Id).ToList();

            return model;
        }

        public override DecisionManageDto GetViewModel(int id)
        {
            var model = new DecisionManageDto
            {
                Translations = new List<DecisionTranslationDto>
                {
                    new DecisionTranslationDto {LanguageId = 1},
                    new DecisionTranslationDto {LanguageId = 2},
                    new DecisionTranslationDto {LanguageId = 3},
                    new DecisionTranslationDto {LanguageId = 4}
                },
                Dropdown = _unitOfWork.CategoryRepo.Set()
                .Select(a => new DecisionDropdownDto
                {
                    Id = a.Id,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name
                })
            };

            if (id == 0)
                return model;

            var dboDecision = _unitOfWork.DecisionRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .First(a => a.Id == id);

            model.Id = dboDecision.Id;
            model.FileUserReadableName = dboDecision.FileUserReadableName;
            model.MainPhotoPath = dboDecision.ComplaintPicturePhotoPath;
            model.PdfFilePath = dboDecision.PdfFilePath;
            model.WordFilePath = dboDecision.WordFilePath;
            model.ShowOnMainPageBox = dboDecision.ShowOnMainPageBox;
            model.LinkToFile = dboDecision.LinkToFile;
            model.SortIndex = dboDecision.SortIndex;
            model.ShowOnSite = dboDecision.ShowOnSite;
            model.DocumentCameInAction = dboDecision.DocumentCameInAction;
            model.ShowOnMainPage = dboDecision.ShowOnMainPage;
            model.SelectedCategoryId = dboDecision.CategoryId;
            model.Tags = string.Join(",", dboDecision.SearchTags.Select(a => a.Tag).ToList());

            model.Translations = dboDecision.Translations
                .Select(a => new DecisionTranslationDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    Description = a.Description,
                    LanguageId = a.LanguageId
                }).ToList();

            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new DecisionDropdownDto
                {
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Id = a.Id,
                    IsSelected = a.Id == model.SelectedCategoryId
                }).ToList();

            return model;
        }

        public override void Save(DecisionManageDto model)
        {
            if (model.Id == 0)
                Create(model);
            else
                Update(model);
        }

        public override SearchViewModel Search(string SearchString, int CategoryId)
        {
            var model = new SearchViewModel() { SearchString = SearchString };
            model.Data = _unitOfWork.DecisionRepo.Set()
                .Include(a => a.Translations)
                .Include(a => a.SearchTags)
                .Where(a => !a.IsDeleted && a.CategoryId == CategoryId && (
                a.SearchTags.Any(x => x.Tag.Contains(SearchString)) ||
                a.Translations.Any(x => x.Name.Contains(SearchString) ||
                x.Description.Contains(SearchString)))).Select(a => new DecisionViewModel
                {
                    Id = a.Id,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Description = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Description,
                    PhotoName = a.ComplaintPicturePhotoPath,
                    DocumentCameInAction = a.DocumentCameInAction,
                    SortIndex = a.SortIndex,
                    PdfFilePath = a.PdfFilePath,
                    LinkToFile = a.LinkToFile,
                    WordFilePath = a.WordFilePath
                }).OrderBy(a => a.SortIndex).ThenByDescending(a => a.Id).ToList();
            return model;
        }

        protected override void Create(DecisionManageDto model)
        {
            var decision = new Kate.EntityFrameworkCore.Entities.Decision
            {
                ShowOnSite = model.ShowOnSite,
                ShowOnMainPageBox = model.ShowOnMainPageBox,
                WordFilePath = model.WordFilePath,
                PdfFilePath = model.PdfFilePath,
                ComplaintPicturePhotoPath = model.MainPhotoPath,
                FileUserReadableName = model.FileUserReadableName,
                ShowOnMainPage = model.ShowOnMainPage,
                SortIndex = model.SortIndex,
                DocumentCameInAction = model.DocumentCameInAction,
                LinkToFile = model.LinkToFile
            };

            if (model.SelectedCategoryId != 0)
                decision.CategoryId = model.SelectedCategoryId;

            foreach (var translation in model.Translations)
                decision.Translations.Add(new DecisionTranslation
                {
                    LanguageId = translation.LanguageId,
                    Description = translation.Description,
                    Name = translation.Name
                });

            if (model.Tags != null)
            {
                var tags = model.Tags.Split(",");

                foreach (var tag in tags)
                {
                    decision.SearchTags.Add(new SearchTag { Tag = tag });
                }
            }

            _unitOfWork.DecisionRepo.Insert(decision);
            _unitOfWork.Save();
        }

        protected override void Update(DecisionManageDto model)
        {
            var dboDecision = _unitOfWork.DecisionRepo.Set()
                   .Include(a => a.Translations)
                   .Include(a => a.SearchTags)
                   .First(a => a.Id == model.Id);

            dboDecision.ShowOnSite = model.ShowOnSite;
            dboDecision.ShowOnMainPageBox = model.ShowOnMainPageBox;
            dboDecision.LinkToFile = model.LinkToFile;
            dboDecision.ShowOnMainPage = model.ShowOnMainPage;
            dboDecision.DocumentCameInAction = model.DocumentCameInAction;
            if (model.WordFilePath != null)
                dboDecision.WordFilePath = model.WordFilePath;
            if (model.PdfFilePath != null)
                dboDecision.PdfFilePath = model.PdfFilePath;
            if (model.MainPhotoPath != null)
                dboDecision.ComplaintPicturePhotoPath = model.MainPhotoPath;
            dboDecision.FileUserReadableName = model.FileUserReadableName;
            dboDecision.SortIndex = model.SortIndex;
            dboDecision.CategoryId = model.SelectedCategoryId;

            foreach (var dboTranslation in dboDecision.Translations)
                foreach (var modelTranslation in model.Translations)
                {
                    if (dboTranslation.Id != modelTranslation.Id)
                        continue;

                    dboTranslation.Description = modelTranslation.Description;
                    dboTranslation.Name = modelTranslation.Name;
                }

            if (model.Tags != null)
            {
                dboDecision.SearchTags.Clear();

                foreach (var tag in model.Tags.Split(","))
                {
                    dboDecision.SearchTags.Add(new SearchTag { Tag = tag });
                }
            }

            _unitOfWork.DecisionRepo.Update(dboDecision);
            _unitOfWork.Save();
        }
    }
}