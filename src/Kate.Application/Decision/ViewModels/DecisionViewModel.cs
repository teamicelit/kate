﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Decision.ViewModels
{
    public class DecisionViewModel
    {
        public int Id { get; set; }

        public DateTime DocumentCameInAction { get; set; }
        public int SortIndex { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string PhotoName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string LinkToFile { get; set; }
    }
}
