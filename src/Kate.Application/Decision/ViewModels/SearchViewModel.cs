﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Decision.ViewModels
{
    public class SearchViewModel
    {
        public IEnumerable<DecisionViewModel> Data { get; set; }
        public string SearchString { get; set; }
    }
}
