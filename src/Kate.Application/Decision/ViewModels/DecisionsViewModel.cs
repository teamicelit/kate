﻿using System;
using System.Collections.Generic;
using System.Text;
using Kate.BaseViewModels;

namespace Kate.Decision.ViewModels
{
    public class DecisionsViewModel : BaseViewModelForPagesWithHeaderStrip
    {
        public IEnumerable<DecisionViewModel> Decisions { get; set; }
    }
}
