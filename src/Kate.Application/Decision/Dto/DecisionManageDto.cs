﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Decision.Dto
{
    public class DecisionManageDto
    {
        public int Id { get; set; }
        public int SelectedCategoryId { get; set; }
        public int SortIndex { get; set; }

        public DateTime DocumentCameInAction { get; set; }

        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string LinkToFile { get; set; }
        public string MainPhotoPath { get; set; }
        public string Tags { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }
        public bool ShowOnMainPage { get; set; }

        public IEnumerable<DecisionDropdownDto> Dropdown { get; set; }
        public IEnumerable<DecisionTranslationDto> Translations { get; set; }
    }
}
