﻿using Kate.Services.GenericDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Decision.Dto
{
    public class DecisionTableDto
    {
        public int Id { get; set; }

        public DateTime DecisionDate { get; set; }
        public int SortIndex { get; set; }

        public string CategoryName { get; set; }
        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }
        public string LinkToFile { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }
        public bool ShowOnMainPage { get; set; }

        public DecisionTranslationDto Translation { get; set; }

        public IEnumerable<SearchTagDto> SearchTags { get; set; }
    }
}
