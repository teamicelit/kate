﻿using Kate.Decision.Dto;
using Kate.Decision.ViewModels;
using Kate.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Decision
{
    public abstract class BaseDecisionAppService : GeneralAdminService<DecisionManageDto, DecisionTableDto>
    {
        public abstract void FillManageDtoWithInitialData(DecisionManageDto model);
        public abstract DecisionsViewModel GetDecisionViewModels(int CategoryId);
        public abstract SearchViewModel Search(string SearchString, int CategoryId);
    }
}
