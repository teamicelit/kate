﻿using Kate.InnerComplaint.Dto;

namespace Kate.InnerComplaint
{
    public abstract class BaseInnerComplaintAppService 
    {
        public abstract InnerComplaintReadDto GetAboutUsReadViewModel();

        public abstract InnerComplaintDto GetAboutUsManageViewModel();

        public abstract void Update(InnerComplaintDto model);

        public abstract void Dispose();
    }
}