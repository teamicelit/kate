﻿using System.Collections.Generic;

namespace Kate.InnerComplaint.Dto
{
    public class InnerComplaintDto
    {
        public int Id { get; set; }

        public IEnumerable<InnerComplaintTranslationDto> Translations { get; set; }
    }
}
