﻿namespace Kate.InnerComplaint.Dto
{
    public class InnerComplaintReadDto
    {
        public int Id { get; set; }

        public InnerComplaintTranslationDto Translation { get; set; }
    }
}
