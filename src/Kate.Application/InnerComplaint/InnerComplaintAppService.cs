﻿using Kate.InnerComplaint.Dto;
using Kate.EntityFrameworkCore.Repositories;
using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Kate.Localization;

namespace Kate.InnerComplaint
{
    public class InnerComplaintAppService : BaseInnerComplaintAppService
    {
        private readonly ICustomUnitOfWork _unitOfWork;
        private readonly IObjectMapper _objectMapper;

        public InnerComplaintAppService(ICustomUnitOfWork unitOfWork, IObjectMapper objectMapper)
        {
            _unitOfWork = unitOfWork;
            _objectMapper = objectMapper;
        }

        public override void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public override InnerComplaintDto GetAboutUsManageViewModel()
        {
            var model = new InnerComplaintDto();

            model = _objectMapper.Map<InnerComplaintDto>(
                _unitOfWork.InnerComplaintRepo.Set()
                .Include(a => a.Translations)
                .Select(a => new { a.Id, a.IsDeleted, Translations = a.Translations })
                .First(a => !a.IsDeleted));

            return model;
        }

        public override InnerComplaintReadDto GetAboutUsReadViewModel()
        {
            var model = new InnerComplaintReadDto();

            model = _objectMapper.Map<InnerComplaintReadDto>(
                _unitOfWork.InnerComplaintRepo.Set()
                .Include(a => a.Translations)
                .Select(a => new { a.Id, a.IsDeleted, Translation = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()) })
                .First(a => !a.IsDeleted));

            return model;
        }

        public override void Update(InnerComplaintDto model)
        {
            var dboInnerComplaint = _unitOfWork.InnerComplaintRepo.Set()
                  .Include(a => a.Translations)
                  .First(a => a.Id == model.Id);

            foreach (var dboTranslation in dboInnerComplaint.Translations)
                foreach (var modelTranslation in model.Translations)
                {
                    if (dboTranslation.Id != modelTranslation.Id)
                        continue;

                    dboTranslation.Description = modelTranslation.Description;
                    dboTranslation.Name = modelTranslation.Name;
                }

            _unitOfWork.InnerComplaintRepo.Update(dboInnerComplaint);
            _unitOfWork.Save();
        }
    }
}
