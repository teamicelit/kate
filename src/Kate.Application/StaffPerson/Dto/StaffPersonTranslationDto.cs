﻿using Kate.Services.GenericDtos;

namespace Kate.StaffPerson.Dto
{
    public class StaffPersonTranslationDto : BaseTranslationDto
    {
        public string Name { get; set; }
        
        public string HeaderText { get; set; }

        public string Possition { get; set; }

        public string Surname { get; set; }

        public string ResumeDescription { get; set; }
    }
}