﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.StaffPerson.Dto
{
    public class StaffPersonTableDto
    {
        public int Id { get; set; }

        public DateTime DateOfBirth { get; set; }
        public int SortIndex { get; set; }

        public string CategoryName { get; set; }
        public string Mail { get; set; }
        public string PhoneNumber { get; set; }
        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }

        public bool ShowOnSite { get; set; }

        public StaffPersonTranslationDto Translation { get; set; }
    }
}
