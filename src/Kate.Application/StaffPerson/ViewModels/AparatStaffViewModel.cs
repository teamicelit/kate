﻿using System;
using System.Collections.Generic;
using System.Text;
using Kate.BaseViewModels;

namespace Kate.StaffPerson.ViewModels
{
    public class AparatStaffViewModel : BaseViewModelForPagesWithHeaderStrip
    {
        public IEnumerable<AparatViewModel> Content { get; set; }
    }
}
