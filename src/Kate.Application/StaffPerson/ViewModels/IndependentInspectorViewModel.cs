﻿using System;
using Kate.BaseViewModels;

namespace Kate.StaffPerson.ViewModels
{
    public class FullEmployeeViewModel : BaseViewModelForPagesWithHeaderStrip
    {
        public int Id { get; set; }

        public string PhotoPath { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string HeaderText { get; set; }
        public string Description { get; set; }
        public string Possition { get; set; }

        public DateTime BirthDate { get; set; }
    }
}
