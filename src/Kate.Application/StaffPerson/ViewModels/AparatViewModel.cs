﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.StaffPerson.ViewModels
{
    public class AparatViewModel
    {
        public int Id { get; set; }
        public int SortIndex { get; set; }

        public string PhotoPath { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Possition { get; set; }
    }
}
