﻿using System;
using System.Collections.Generic;
using System.Text;
using Kate.BaseViewModels;

namespace Kate.StaffPerson.ViewModels
{
    public class BasicAboutViewModel : BaseViewModelForPagesWithHeaderStrip
    {
        public string Content { get; set; }
    }
}
