﻿using Kate.Services;
using Kate.StaffPerson.Dto;
using Kate.StaffPerson.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.StaffPerson
{
    public abstract class BaseStaffPersonAppService : GeneralAdminService<StaffPersonManageDto, StaffPersonTableDto>
    {
        public abstract void FillManageDtoWithInitialData(StaffPersonManageDto model);

        public abstract AparatStaffViewModel GetAparatStaff(int id);

        public abstract FullEmployeeViewModel GetFullEmployee(int id, bool calledFromAparat);
        
        public abstract BasicAboutViewModel GetBasicAbout (int id);
    }
}