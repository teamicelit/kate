﻿using System;
using System.Collections.Generic;
using System.Text;
using Kate.StaffPerson.Dto;
using Kate.EntityFrameworkCore.Repositories;
using System.Linq;
using Kate.Localization;
using Microsoft.EntityFrameworkCore;
using Kate.EntityFrameworkCore.Entities;
using Kate.StaffPerson.ViewModels;
using Kate.Helpers;

namespace Kate.StaffPerson
{
    public class StaffPersonAppService : BaseStaffPersonAppService
    {
        private readonly ICustomUnitOfWork _unitOfWork;

        public StaffPersonAppService(ICustomUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override void Delete(int id)
        {
            var dboStaffPerson = _unitOfWork.StaffPersonRepo.GetById(id);

            dboStaffPerson.IsDeleted = true;

            _unitOfWork.StaffPersonRepo.Update(dboStaffPerson);
            _unitOfWork.Save();
        }

        public override void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public override void FillManageDtoWithInitialData(StaffPersonManageDto model)
        {
            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new StaffPersonDropdownDto
                {
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Id = a.Id,
                    IsSelected = a.Id == model.SelectedCategoryId
                }).ToList();
        }

        public override AparatStaffViewModel GetAparatStaff(int id)
        {
            var model = new AparatStaffViewModel
            {
                Content = _unitOfWork.StaffPersonRepo.Set()
                .Where(a => !a.IsDeleted && a.CategoryId == id)
            .Include(a => a.Translations)
            .Select(a => new AparatViewModel
            {
                Id = a.Id,
                Surname = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Surname,
                Possition = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Possition,
                Description = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).ResumeDescription,
                Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                PhotoPath = a.MainPhotoPath,
                Email = a.Mail,
                Mobile = a.PhoneNumber,
                SortIndex = a.SortIndex
            }).OrderBy(a => a.SortIndex).ThenByDescending(a => a.Id).ToList()
            };
            model.CategoryNames = CategoryNamesExtractor.Extractor(_unitOfWork, id);
            return model;
        }

        public override BasicAboutViewModel GetBasicAbout(int id)
        {
            var model = new BasicAboutViewModel
            {
                Content = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Include(a => a.Translations)
                .FirstOrDefault(a => a.Id == id)
                .Translations
                .First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum())
                .Description
            };
            model.CategoryNames = CategoryNamesExtractor.Extractor(_unitOfWork, id);
            return model;
        }

        public override FullEmployeeViewModel GetFullEmployee(int id, bool calledFromAparat)
        {
            EntityFrameworkCore.Entities.StaffPerson dboFullEmployee = null;

            if (calledFromAparat)
                dboFullEmployee =
                _unitOfWork.StaffPersonRepo.Set()
                .Where(a => !a.IsDeleted)
                .Include(a => a.Translations)
                .First(a => a.Id == id);
            else dboFullEmployee =
                _unitOfWork.StaffPersonRepo.Set()
                .Where(a => !a.IsDeleted)
                .Include(a => a.Translations)
                .First(a => a.CategoryId == id);
            var model = new FullEmployeeViewModel
            {
                Id = dboFullEmployee.Id,
                PhotoPath = dboFullEmployee.MainPhotoPath,
                Name = dboFullEmployee.Translations.First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                Description = dboFullEmployee.Translations.First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).ResumeDescription,
                Surname = dboFullEmployee.Translations.First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Surname,
                HeaderText = dboFullEmployee.Translations.First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).HeaderText,
                Possition = dboFullEmployee.Translations.First(a => a.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Possition,
                BirthDate = dboFullEmployee.DateOfBirth
            };
            model.CategoryNames = CategoryNamesExtractor.Extractor(_unitOfWork, id);

            return model;
        }

        public override IEnumerable<StaffPersonTableDto> GetTableViewModels()
        {
            var model = _unitOfWork.StaffPersonRepo.Set()
                .Include(a => a.Translations)
                .Where(a => !a.IsDeleted)
                .Select(a => new StaffPersonTableDto
                {
                    Id = a.Id,
                    FileUserReadableName = a.FileUserReadableName,
                    MainPhotoPath = a.MainPhotoPath,
                    CategoryName = a.Category.Translations.FirstOrDefault(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name ?? "",
                    PdfFilePath = a.PdfFilePath,
                    Mail = a.Mail,
                    SortIndex = a.SortIndex,
                    PhoneNumber = a.PhoneNumber,
                    ShowOnSite = a.ShowOnSite,
                    WordFilePath = a.WordFilePath,
                    DateOfBirth = a.DateOfBirth,
                    Translation = a.Translations.Select(b => new StaffPersonTranslationDto
                    {
                        Id = b.Id,
                        LanguageId = b.LanguageId,
                        ResumeDescription = b.ResumeDescription,
                        HeaderText = b.HeaderText,
                        Possition = b.Possition,
                        Surname = b.Surname,
                        Name = b.Name
                    }).First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()),
                }).OrderByDescending(a => a.Id).ToList();

            return model;
        }

        public override StaffPersonManageDto GetViewModel(int id)
        {
            var model = new StaffPersonManageDto
            {
                Translations = new List<StaffPersonTranslationDto>
                {
                    new StaffPersonTranslationDto {LanguageId = 1},
                    new StaffPersonTranslationDto {LanguageId = 2},
                    new StaffPersonTranslationDto {LanguageId = 3},
                    new StaffPersonTranslationDto {LanguageId = 4}
                },
                Dropdown = _unitOfWork.CategoryRepo.Set()
                .Select(a => new StaffPersonDropdownDto
                {
                    Id = a.Id,
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name
                })
            };

            if (id == 0)
                return model;

            var dboStaffPerson = _unitOfWork.StaffPersonRepo.Set()
                .Include(a => a.Translations)
                .First(a => a.Id == id);

            model.Id = dboStaffPerson.Id;
            model.FileUserReadableName = dboStaffPerson.FileUserReadableName;
            model.MainPhotoPath = dboStaffPerson.MainPhotoPath;
            model.PdfFilePath = dboStaffPerson.PdfFilePath;
            model.WordFilePath = dboStaffPerson.WordFilePath;
            model.DateOfBirth = dboStaffPerson.DateOfBirth;
            model.SortIndex = dboStaffPerson.SortIndex;
            model.PhoneNumber = dboStaffPerson.PhoneNumber;
            model.Mail = dboStaffPerson.Mail;
            model.ShowOnSite = dboStaffPerson.ShowOnSite;
            model.SelectedCategoryId = dboStaffPerson.CategoryId;

            model.Translations = dboStaffPerson.Translations
                .Select(a => new StaffPersonTranslationDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    ResumeDescription = a.ResumeDescription,
                    HeaderText = a.HeaderText,
                    Possition = a.Possition,
                    Surname = a.Surname,
                    LanguageId = a.LanguageId
                }).ToList();

            model.Dropdown = _unitOfWork.CategoryRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new StaffPersonDropdownDto
                {
                    Name = a.Translations.First(b => b.LanguageId == LocalizationRetrieveHelper.CurrentLanguageToEnum()).Name,
                    Id = a.Id,
                    IsSelected = a.Id == model.SelectedCategoryId
                }).ToList();

            return model;
        }

        public override void Save(StaffPersonManageDto model)
        {
            if (model.Id == 0)
                Create(model);
            else
                Update(model);
        }

        protected override void Create(StaffPersonManageDto model)
        {
            var staffPerson = new Kate.EntityFrameworkCore.Entities.StaffPerson
            {
                ShowOnSite = model.ShowOnSite,
                WordFilePath = model.WordFilePath,
                PdfFilePath = model.PdfFilePath,
                MainPhotoPath = model.MainPhotoPath,
                FileUserReadableName = model.FileUserReadableName,
                DateOfBirth = model.DateOfBirth,
                SortIndex = model.SortIndex,
                Mail = model.Mail,
                PhoneNumber = model.PhoneNumber
            };

            if (model.SelectedCategoryId != 0)
                staffPerson.CategoryId = model.SelectedCategoryId;

            foreach (var translation in model.Translations)
                staffPerson.Translations.Add(new StaffPersonTranslation
                {
                    LanguageId = translation.LanguageId,
                    ResumeDescription = translation.ResumeDescription,
                    Surname = translation.Surname,
                    HeaderText = translation.HeaderText,
                    Possition = translation.Possition,
                    Name = translation.Name
                });

            _unitOfWork.StaffPersonRepo.Insert(staffPerson);
            _unitOfWork.Save();
        }

        protected override void Update(StaffPersonManageDto model)
        {
            var dboStaffPerson = _unitOfWork.StaffPersonRepo.Set()
                      .Include(a => a.Translations)
                      .First(a => a.Id == model.Id);

            dboStaffPerson.ShowOnSite = model.ShowOnSite;
            dboStaffPerson.DateOfBirth = model.DateOfBirth;
            if (model.WordFilePath != null)
                dboStaffPerson.WordFilePath = model.WordFilePath;
            if (model.PdfFilePath != null)
                dboStaffPerson.PdfFilePath = model.PdfFilePath;
            if (model.MainPhotoPath != null)
                dboStaffPerson.MainPhotoPath = model.MainPhotoPath;
            dboStaffPerson.FileUserReadableName = model.FileUserReadableName;
            dboStaffPerson.CategoryId = model.SelectedCategoryId;
            dboStaffPerson.PhoneNumber = model.PhoneNumber;
            dboStaffPerson.SortIndex = model.SortIndex;
            dboStaffPerson.Mail = model.Mail;

            foreach (var dboTranslation in dboStaffPerson.Translations)
                foreach (var modelTranslation in model.Translations)
                {
                    if (dboTranslation.Id != modelTranslation.Id)
                        continue;

                    dboTranslation.ResumeDescription = modelTranslation.ResumeDescription;
                    dboTranslation.HeaderText = modelTranslation.HeaderText;
                    dboTranslation.Possition = modelTranslation.Possition;
                    dboTranslation.Surname = modelTranslation.Surname;
                    dboTranslation.Name = modelTranslation.Name;
                }

            _unitOfWork.StaffPersonRepo.Update(dboStaffPerson);
            _unitOfWork.Save();
        }
    }
}
