﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Kate.Authorization.Accounts.Dto;

namespace Kate.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
