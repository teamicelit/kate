﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.PartnerCompany.Dto
{
   public class PartnerCompanyManageDto
    {
        public int Id { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }

        public string PartnerLogo { get; set; }
        public string Link { get; set; }
        public string FileUserReadableName { get; set; }

        public IEnumerable<PartnerCompanyTranslationDto> Translations { get; set; }
    }
}

