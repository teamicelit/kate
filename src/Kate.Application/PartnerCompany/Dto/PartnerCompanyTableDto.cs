﻿namespace Kate.PartnerCompany.Dto
{
    public class PartnerCompanyTableDto
    {
        public int  Id { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }

        public string PartnerLogo { get; set; }
        public string Link { get; set; }
    }
}
