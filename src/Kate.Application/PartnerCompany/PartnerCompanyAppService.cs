﻿using System;
using System.Collections.Generic;
using System.Text;
using Kate.EntityFrameworkCore.Repositories;
using Kate.EntityFrameworkCore.Entities;
using Kate.PartnerCompany.Dto;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Kate.PartnerCompany
{
    public class PartnerCompanyAppService : BasePartnerCompanyAppService
    {
        private readonly ICustomUnitOfWork _unitOfWork;

        public PartnerCompanyAppService(ICustomUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override void Delete(int id)
        {
            var dboPartnerCompany = _unitOfWork.PartnerCompanyRepo.GetById(id);
            dboPartnerCompany.IsDeleted = true;
            _unitOfWork.PartnerCompanyRepo.Update(dboPartnerCompany);
            _unitOfWork.Save();
        }

        public override void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public override IEnumerable<PartnerCompanyTableDto> GetTableViewModels()
        {
            var model = _unitOfWork.PartnerCompanyRepo.Set()
                .Where(a => !a.IsDeleted)
                .Select(a => new PartnerCompanyTableDto
                {
                    Id = a.Id,
                    ShowOnSite = a.ShowOnSite,
                    ShowOnMainPageBox = a.ShowOnMainPageBox,
                    PartnerLogo = a.PartnerLogo,
                    Link = a.Link
                }).OrderByDescending(x => x.Id).ToList();

            return model;
        }

        public override PartnerCompanyManageDto GetViewModel(int id)
        {
            var model = new PartnerCompanyManageDto
            {
                Translations = new List<PartnerCompanyTranslationDto>()
                {
                new PartnerCompanyTranslationDto {LanguageId =1},
                new PartnerCompanyTranslationDto {LanguageId =2},
                new PartnerCompanyTranslationDto {LanguageId = 3},
                new PartnerCompanyTranslationDto {LanguageId = 4}
                }
            };

            if (id == 0)
                return model;

            var dboPartnerCompany = _unitOfWork.PartnerCompanyRepo.Set()
                .Include(a => a.Translations)
                .First(a => a.Id == id);

            model.PartnerLogo = dboPartnerCompany.PartnerLogo;
            model.ShowOnSite = dboPartnerCompany.ShowOnSite;
            model.ShowOnMainPageBox = dboPartnerCompany.ShowOnMainPageBox;
            model.Link = dboPartnerCompany.Link;

            model.Translations = dboPartnerCompany.Translations.Select(a => new PartnerCompanyTranslationDto()
            {
                Id = a.Id,
                LanguageId = a.LanguageId,
                Name = a.Name,
                Description = a.Description,
            });

            return model;
        }

        public override void Save(PartnerCompanyManageDto model)
        {
            if (model.Id == 0)
                Create(model);
            else
                Update(model);
        }

        protected override void Create(PartnerCompanyManageDto model)
        {
            var partnerCompany = new EntityFrameworkCore.Entities.PartnerCompany()
            {
                ShowOnSite = model.ShowOnSite,
                ShowOnMainPageBox = model.ShowOnMainPageBox,
                PartnerLogo = model.PartnerLogo,
                Link = model.Link
            };

            foreach (var translation in model.Translations)
                partnerCompany.Translations.Add(new PartnerCompaniesTranslation
                {
                    LanguageId = translation.LanguageId,
                    Description = translation.Description,
                    Name = translation.Name
                });

            _unitOfWork.PartnerCompanyRepo.Insert(partnerCompany);
            _unitOfWork.Save();
        }

        protected override void Update(PartnerCompanyManageDto model)
        {
            var dboPartnerCompany = _unitOfWork.PartnerCompanyRepo.Set()
                 .Include(a => a.Translations)
                 .First(a => a.Id == model.Id);

            dboPartnerCompany.ShowOnSite = model.ShowOnSite;
            dboPartnerCompany.ShowOnMainPageBox = model.ShowOnMainPageBox;
            if (model.PartnerLogo != null)
                dboPartnerCompany.PartnerLogo = model.PartnerLogo;
            dboPartnerCompany.Link = model.Link;

            foreach (var dboTranslation in dboPartnerCompany.Translations)
            {
                foreach (var modelTranslation in model.Translations)
                {
                    if (dboTranslation.Id != modelTranslation.Id)
                        continue;
                    dboTranslation.Name = modelTranslation.Name;
                    dboTranslation.Description = modelTranslation.Description;
                }
            }

            _unitOfWork.PartnerCompanyRepo.Update(dboPartnerCompany);
            _unitOfWork.Save();
        }
    }
}
