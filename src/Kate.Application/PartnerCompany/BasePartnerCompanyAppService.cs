﻿using Kate.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Kate.PartnerCompany.Dto;

namespace Kate.PartnerCompany
{
    public abstract class BasePartnerCompanyAppService : GeneralAdminService<PartnerCompanyManageDto, PartnerCompanyTableDto>
    {
    }
}
