﻿namespace Kate.Localization
{
    public enum LanguageEnum
    {
        English = 1,
        Georgian = 2,
        French = 3,
        German = 4
    }
}
