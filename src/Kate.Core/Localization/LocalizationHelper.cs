﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Kate.Localization
{
    public static class LocalizationRetrieveHelper
    {
        public static DateTime GetLanguageCookieExpirationDate()
        {
            return DateTime.Now.AddYears(1);
        }

        public static int CurrentLanguageToEnum()
        {
            switch (CultureInfo.CurrentCulture.TwoLetterISOLanguageName)
            {
                case "en":
                    return (int)LanguageEnum.English;
                case "ka":
                    return (int)LanguageEnum.Georgian;
                case "fr":
                    return (int)LanguageEnum.French;
                case "de":
                    return (int)LanguageEnum.German;
            }

            return (int)LanguageEnum.Georgian;
        }
    }
}
