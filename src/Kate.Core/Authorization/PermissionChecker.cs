﻿using Abp.Authorization;
using Kate.Authorization.Roles;
using Kate.Authorization.Users;

namespace Kate.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
