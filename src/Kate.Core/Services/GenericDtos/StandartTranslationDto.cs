﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Services.GenericDtos
{
    public class StandartTranslationDto : BaseTranslationDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}