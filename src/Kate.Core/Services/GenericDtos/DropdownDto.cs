﻿namespace Kate.Services.GenericDtos
{
    public class DropdownDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public bool IsSelected { get; set; }

        public bool IsDisabled { get; set; }
    }
}
