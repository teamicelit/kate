﻿using Microsoft.AspNetCore.Http;

namespace Kate.Services.GenericDtos
{
    public class FileUpload
    {
        public IFormFile UploadPdf { get; set; }
        
        public IFormFile UploadWord { get; set; }
        
        public IFormFile UploadPicture { get; set; }
    }
}
