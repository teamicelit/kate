﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Services.GenericDtos
{
    public class BaseTranslationDto
    {
        public int Id { get; set; }
        public int LanguageId { get; set; }
    }
}
