﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Services.GenericDtos
{
    public class SearchTagDto
    {
        public int Id { get; set; }

        public string Tag { get; set; }
    }
}
