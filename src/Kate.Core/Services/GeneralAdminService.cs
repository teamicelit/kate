﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.Services
{
    public abstract class GeneralAdminService<TEntity, UEntity>
           where TEntity : class
           where UEntity : class
    {
        public abstract IEnumerable<UEntity> GetTableViewModels();

        public abstract TEntity GetViewModel(int id);

        public abstract void Save(TEntity model);

        protected abstract void Create(TEntity model);

        protected abstract void Update(TEntity model);

        public abstract void Delete(int id);

        public abstract void Dispose();
    }
}
