﻿namespace Kate
{
    public class KateConsts
    {
        public const string LocalizationSourceName = "Kate";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
