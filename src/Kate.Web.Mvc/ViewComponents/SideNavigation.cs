﻿using System.Linq;
using Kate.DynamicMenu;
using Kate.Web.Models.Navigation;
using Microsoft.AspNetCore.Mvc;

namespace Kate.Web.ViewComponents
{
    public class SideNavigationViewComponent : ViewComponent
    {
        private readonly BaseDynamicMenuService _dynamicMenuService;

        public SideNavigationViewComponent(BaseDynamicMenuService dynamicMenuService)
        {
            _dynamicMenuService = dynamicMenuService;
        }

        public IViewComponentResult Invoke(bool isVoicePage = false)
        {
            return View(new NavigationViewModel
            {
                IsVoicePage = isVoicePage,
                Categories = _dynamicMenuService.GetSideNavigation()
            });
        }
    }
}
