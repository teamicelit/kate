﻿using Kate.PartnerCompany;
using Microsoft.AspNetCore.Mvc;

namespace Kate.Web.ViewComponents
{
    public class PartnerCompanyViewComponent : ViewComponent
    {
        private readonly BasePartnerCompanyAppService _partnerCompanyService;

        public PartnerCompanyViewComponent(BasePartnerCompanyAppService partnerCompanyService)
        {
            _partnerCompanyService = partnerCompanyService;
        }

        public IViewComponentResult Invoke()
        {
            return View(_partnerCompanyService.GetTableViewModels());
        }
    }
}
