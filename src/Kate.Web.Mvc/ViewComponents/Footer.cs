﻿using Kate.AboutUs;
using Kate.DynamicMenu;
using Kate.DynamicMenu.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Kate.Web.ViewComponents
{
    public class FooterViewComponent : ViewComponent
    {
        private readonly BaseDynamicMenuService _dynamicMenuService;
        private readonly BaseAboutUsAppService _aboutUsService;

        public FooterViewComponent(BaseDynamicMenuService dynamicMenuService, BaseAboutUsAppService aboutUsService)
        {
            _dynamicMenuService = dynamicMenuService;
            _aboutUsService = aboutUsService;
        }

        public IViewComponentResult Invoke()
        {
            var model = new FooterViewModel
            {
                AboutUsText = _aboutUsService.GetAboutUsReadViewModel(),
                Categories = _dynamicMenuService.GetFooterNavigation()
            };

            return View(model);
        }
    }
}