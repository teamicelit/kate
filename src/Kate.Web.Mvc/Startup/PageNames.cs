﻿namespace Kate.Web.Startup
{
    public class PageNames
    {
        public const string Home = "Home";
        public const string About = "About";
        public const string Tenants = "Tenants";
        public const string Users = "Users";
        public const string Roles = "Roles";
        public const string DynamicMenu = "Dynamic Menu";
        public const string PartnerCompany = "Partner Company";
        public const string Decision = "Decision";
        public const string Disciplinary = "Disciplinary";
        public const string Article = "Article";
        public const string PhotoVideoGallery = "Photo Video Gallery";
        public const string StaffPerson = "Staff";
        public const string StatisticalData = "StatisticalData";
        public const string Complaint = "Complaint";
    }
}
