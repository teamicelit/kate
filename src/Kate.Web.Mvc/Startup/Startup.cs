﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Castle.Facilities.Logging;
using Abp.AspNetCore;
using Abp.Castle.Logging.Log4Net;
using Kate.Authentication.JwtBearer;
using Kate.Configuration;
using Kate.Identity;
using Kate.Web.Resources;
using Kate.EntityFrameworkCore.Repositories;
using Kate.AboutUs;
using Microsoft.AspNetCore.Http;
using Kate.DynamicMenu;
using Kate.Utils;
using Kate.PartnerCompany;
using Kate.Article;
using Kate.PhotoVideoGallery;
using Kate.Decision;
using Kate.StaffPerson;
using Kate.Web.Models.EmailSettings;
using Kate.Disciplinary;
using Kate.InnerComplaint;
using System.Globalization;
using System.Collections.Generic;
using CodingBlast;
using Kate.DocumentSign;
using Kate.DocumentSign.Configurations;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Razor.TagHelpers;
#if FEATURE_SIGNALR
using Owin;
using Abp.Owin;
using Kate.Owin;
#elif FEATURE_SIGNALR_ASPNETCORE
using Abp.AspNetCore.SignalR.Hubs;
#endif

namespace Kate.Web.Startup
{
    public class Startup
    {
        private readonly IConfigurationRoot _appConfiguration;
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _appConfiguration = env.GetAppConfiguration();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // MVC
            services.AddMvc();

            services.AddSingleton<ITagHelperComponent>(new GoogleAnalyticsTagHelperComponent("UA-119585527-1"));

            IdentityRegistrar.Register(services);
            AuthConfigurer.Configure(services, _appConfiguration);

            services.ConfigureApplicationCookie(options => options.LoginPath = "/admin/Account/Login");

            services.AddScoped<IWebResourceManager, WebResourceManager>();


            //services.Configure<RequestLocalizationOptions>(
            //    opts =>
            //    {
            //        var supportedCultures = new List<CultureInfo>
            //        {
            //            new CultureInfo("en-GB"),
            //            new CultureInfo("en-US"),
            //            new CultureInfo("en"),
            //            new CultureInfo("fr-FR"),
            //            new CultureInfo("fr"),
            //        };

            //        opts.DefaultRequestCulture = new RequestCulture("ka");
            //        // Formatting numbers, dates, etc.
            //        opts.SupportedCultures = supportedCultures;
            //        // UI strings that we have localized.
            //        opts.SupportedUICultures = supportedCultures;
            //    });

#if FEATURE_SIGNALR_ASPNETCORE
            services.AddSignalR();
#endif

            services.AddCors();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ICustomUnitOfWork, CustomUnitOfWork>();
            services.AddTransient<BaseAboutUsAppService, AboutUsAppService>();
            services.AddTransient<BaseDynamicMenuService, DynamicMenuAppService>();
            services.AddTransient<BasePartnerCompanyAppService, PartnerCompanyAppService>();
            services.AddTransient<BaseArticleAppService, ArticleAppService>();
            services.AddTransient<BasePhotoVideoGalleryAppService, PhotoVideoGalleryAppService>();
            services.AddTransient<BaseDecisionAppService, DecisionAppService>();
            services.AddTransient<BaseDisciplinaryAppService, DisciplinaryAppService>();
            services.AddTransient<BaseStaffPersonAppService, StaffPersonAppService>();
            services.AddTransient<BaseInnerComplaintAppService, InnerComplaintAppService>();
            services.AddTransient<IDocumentSignatureService, DocumentSignatureService>();

            services.AddSingleton<IConfiguration>(Configuration);
            services.Configure<FilePathSetting>(Configuration.GetSection(nameof(FilePathSetting)));
            services.Configure<SmtpSettings>(Configuration.GetSection(nameof(SmtpSettings)));
            services.Configure<MailDestinationSettings>(Configuration.GetSection(nameof(MailDestinationSettings)));
            services.Configure<DocumentSignatureConfiguration>(Configuration.GetSection(nameof(DocumentSignatureConfiguration)));

            // Configure Abp and Dependency Injection
            return services.AddAbp<KateWebMvcModule>(
                // Configure Log4Net logging
                options => options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig("log4net.config")
                )
            );
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAbp(x => { x.UseSecurityHeaders = false; }); // Initializes ABP framework.

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseCors(x =>
            {
                x.AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin()
                    .AllowCredentials();
            });
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseJwtTokenMiddleware();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "defaultWithArea",
                    template: "{area}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

#if FEATURE_SIGNALR
        private static void ConfigureOwinServices(IAppBuilder app)
        {
            app.Properties["host.AppName"] = "Kate";

            app.UseAbp();

            app.MapSignalR();
        }
#endif
    }
}

