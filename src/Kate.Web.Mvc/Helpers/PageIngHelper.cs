﻿using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kate.Web.Helpers
{
    public class PageingHelper<T>
    {
        public PageingHelper(IEnumerable<T> list, int page, int pageSize)
        {
            DataForPartial = new PageingData();
            var totalRecords = list.Count();
            DataForPartial.PageCount = (totalRecords / pageSize) + ((totalRecords % pageSize) > 0 ? 1 : 0);
            DataForPartial.CurrentPage = page;
            DataForPartial.PageSize = pageSize;
            List = list.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<T> List { get; }

        private PageingData DataForPartial;

        public PageingData GetPageingData(RouteValueDictionary routevalue)
        {
            routevalue.TryGetValue("controller", out object controller);
            routevalue.TryGetValue("action", out object action);
            routevalue.TryGetValue("id", out object categoryId);
            DataForPartial.Controller = (string)controller;
            DataForPartial.Action = (string)action;
            DataForPartial.CategoryId = Convert.ToInt32(categoryId);
            return DataForPartial;
        }

        public PageingData GetPageingData(RouteValueDictionary routevalue,string Search)
        {
            routevalue.TryGetValue("controller", out object controller);
            routevalue.TryGetValue("action", out object action);
            routevalue.TryGetValue("id", out object categoryId);
            DataForPartial.Controller = (string)controller;
            DataForPartial.Action = (string)action;
            DataForPartial.CategoryId = Convert.ToInt32(categoryId);
            DataForPartial.SearchString = Search;
            return DataForPartial;
        }

    }
    public class PageingData
    {
        public int PageCount { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int CategoryId { get; set; }

        public string Controller { get; set; }
        public string Action { get; set; }
        public string SearchString { get; set; }
    }
}

