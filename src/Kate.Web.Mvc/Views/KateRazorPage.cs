﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace Kate.Web.Views
{
    public abstract class KateRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected KateRazorPage()
        {
            LocalizationSourceName = KateConsts.LocalizationSourceName;
        }
    }
}
