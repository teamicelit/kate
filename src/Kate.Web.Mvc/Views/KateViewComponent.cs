﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Kate.Web.Views
{
    public abstract class KateViewComponent : AbpViewComponent
    {
        protected KateViewComponent()
        {
            LocalizationSourceName = KateConsts.LocalizationSourceName;
        }
    }
}
