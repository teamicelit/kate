﻿using System.Collections.Generic;
using Abp.Localization;

namespace Kate.Web.Views.Shared.Components
{
    public class LanguageSwitchViewModel
    {
        public LanguageInfo CurrentLanguage { get; set; }

        public IReadOnlyList<LanguageInfo> Languages { get; set; }

        public bool IsVoicePage { get; set; }
    }
}