﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Abp.Localization;

namespace Kate.Web.Views.Shared.Components
{
    public class LanguageSwitchViewComponent : KateViewComponent
    {
        private readonly ILanguageManager _languageManager;

        public LanguageSwitchViewComponent(ILanguageManager languageManager)
        {
            _languageManager = languageManager;
        }

        public IViewComponentResult Invoke(bool isVoicePage = false)
        {
            var model = new LanguageSwitchViewModel
            {
                CurrentLanguage = _languageManager.CurrentLanguage,
                Languages = _languageManager.GetLanguages().Where(l => !l.IsDisabled && l.Name!="fr" && l.Name != "de").ToList(),
                IsVoicePage = isVoicePage
            };

            return View(model);
        }
    }
}