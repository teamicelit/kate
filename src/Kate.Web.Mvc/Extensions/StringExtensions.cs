﻿namespace Kate.Web.Extensions
{
    public static class StringExtensions
    {
        public static string Slice(this string Str, int MaxSize)
        {
            return !string.IsNullOrEmpty(Str) ? Str.Substring(0, Str.Length > MaxSize ? MaxSize : Str.Length) : "";
        }
        public static string PhotoUrlOrDefault(this string Str)
        {
            return !string.IsNullOrEmpty(Str) ? Str : "default.png";
        }
    }
}
