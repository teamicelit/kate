﻿using Kate.Controllers;
using Microsoft.AspNetCore.Mvc;
using Kate.InnerComplaint;

namespace Kate.Web.Mvc.Controllers
{
    public class ComplaintController : KateControllerBase
    {
        private readonly BaseInnerComplaintAppService _innerComplaintService;

        public ComplaintController(BaseInnerComplaintAppService innerComplaintService)
        {
            _innerComplaintService = innerComplaintService;
        }

        public IActionResult Index()
        {
            return View(_innerComplaintService.GetAboutUsReadViewModel());
        }
    }
}