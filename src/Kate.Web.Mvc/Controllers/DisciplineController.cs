﻿using Abp.Localization;
using Kate.Controllers;
using Kate.Disciplinary;
using Microsoft.AspNetCore.Mvc;

namespace Kate.Web.Mvc.Controllers
{
    public class DisciplineController : KateControllerBase
    {
        private readonly BaseDisciplinaryAppService _disciplinaryService;
        private readonly ILanguageManager _languageManager;
         
        public DisciplineController(BaseDisciplinaryAppService disciplinaryService, ILanguageManager languageManager)
        {
            _disciplinaryService = disciplinaryService;
            _languageManager = languageManager;
        }

        public IActionResult Index(int id)
        {
            ViewBag.cl = _languageManager.CurrentLanguage.Name;
            var model = _disciplinaryService.GetDisciplinary(id);
            ViewBag.CategoryNames = model.CategoryNames;
            return View(model);
        }

        public IActionResult ReadMore(int id)
        {
            var data = _disciplinaryService.GetReadMore(id);
            ViewBag.CategoryNames = data.CategoryNames;
            return View("ReadMore", data.Content);
        }
    }
}