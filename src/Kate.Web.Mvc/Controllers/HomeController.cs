﻿using Microsoft.AspNetCore.Mvc;
using Kate.Controllers;
using Kate.DynamicMenu;
using Kate.Article;
using Kate.Search.ViewModels;
using Kate.Web.Helpers;

namespace Kate.Web.Mvc.Controllers
{
    public class HomeController : KateControllerBase
    {
        private readonly BaseArticleAppService _articleService;

        public HomeController(BaseArticleAppService articleService)
        {
            _articleService = articleService;
        }

        public IActionResult Index()
        {
            return View(_articleService.GetNewsFeed(0));
        }  
        
        
        public IActionResult Search(string search,int? page)
        {
            var CurrentPage = page ?? 0;
            var Model = _articleService.GlobalSearch(search);
            var model = new PageingHelper<SearchViewModel>(Model, CurrentPage, 10);
            ViewData["GlobalSearch"] = search;
            return View(model);
        }
    }
}