﻿using Kate.Controllers;
using Kate.Decision;
using Microsoft.AspNetCore.Mvc;
using Kate.Web.Helpers;
using Kate.Decision.ViewModels;
using Abp.Localization;

namespace Kate.Web.Mvc.Controllers
{
    public class LegislationController : KateControllerBase
    {
        private readonly BaseDecisionAppService _decisionService;
        private readonly ILanguageManager _languageManager;

        public LegislationController(BaseDecisionAppService decisionService, ILanguageManager languageManager)
        {
            _decisionService = decisionService;
            _languageManager = languageManager;
        }

        public IActionResult Decision(int id, int? page)
        {

            ViewBag.cl = _languageManager.CurrentLanguage.Name;
            var CurrentPage = page ?? 1;
            var data = _decisionService.GetDecisionViewModels(id);
            var model = new PageingHelper<DecisionViewModel>(data.Decisions, CurrentPage, 10);
            ViewBag.CatId = id;
            ViewBag.CategoryNames = data.CategoryNames;
            return View(model);
        }

        public IActionResult Search(string SearchString, int CategoryId)
        {
            ViewBag.CatId = CategoryId;
            var model = _decisionService.Search(SearchString, CategoryId);
            return View(model);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult International()
        {
            return View();
        }

        public IActionResult Foreign()
        {
            return View();
        }

        public IActionResult InternalLegislation()
        {
            return View();
        }
    }
}