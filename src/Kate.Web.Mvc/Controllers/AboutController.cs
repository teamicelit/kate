﻿using Kate.Controllers;
using Kate.DynamicMenu;
using Kate.StaffPerson;
using Microsoft.AspNetCore.Mvc;

namespace Kate.Web.Mvc.Controllers
{
    public class AboutController : KateControllerBase
    {
        private readonly BaseStaffPersonAppService _staffPersonService;

        public AboutController(BaseStaffPersonAppService staffPersonService)
        {
            _staffPersonService = staffPersonService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Aparat(int id)
        {
            var data = _staffPersonService.GetAparatStaff(id);
            ViewBag.CategoryNames = data.CategoryNames;
            return View(data.Content);
        }

        public IActionResult Inspector(int id, bool calledFromAparat)
        {
            var model = _staffPersonService.GetFullEmployee(id, calledFromAparat);
            ViewBag.CategoryNames = model.CategoryNames;
            return View(model);
        }

        public IActionResult Work(int id)
        {
            var data = _staffPersonService.GetBasicAbout(id);
            ViewBag.CategoryNames = data.CategoryNames;
            return View("Work", data.Content);
        }
        public IActionResult Test()
        {
            return View();
        }

    }
}