﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using Kate.Controllers;
using Kate.Web.Models.Contact;
using Kate.Web.Models.EmailSettings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using Attachment = SendGrid.Helpers.Mail.Attachment;

namespace Kate.Web.Mvc.Controllers
{
    public class ContactController : KateControllerBase
    {
        private readonly IOptions<SmtpSettings> _smtpServiceSettings;
        private readonly IOptions<MailDestinationSettings> _mailDestination;

        public ContactController(IOptions<SmtpSettings> SmtpServiceSettings, IOptions<MailDestinationSettings> MailDestination)
        {
            _smtpServiceSettings = SmtpServiceSettings;
            _mailDestination = MailDestination;
        }

        public static byte[] ToByteArray(Stream stream)
        {
            var bytes = new List<byte>();

            int b;
            while ((b = stream.ReadByte()) != -1)
                bytes.Add((byte)b);

            return bytes.ToArray();
        }

        private Attachment GetAttachmentFromFormFile(IFormFile formFile)
        {
            using (var formFileStream = formFile.OpenReadStream())
            {
                var attachment = new Attachment();
                var formFileBytes = ToByteArray(formFileStream);

                attachment.Content = Convert.ToBase64String(formFileBytes);
                attachment.Filename = formFile.FileName;
                attachment.Type = formFile.ContentType;
                attachment.Disposition = formFile.ContentDisposition;

                return attachment;
            }
        }

        private SendGridMessage GetSendGridMessage(ContactViewModel model)
        {
            var mailDestinationSettings = _mailDestination.Value;

            var from = new EmailAddress(mailDestinationSettings.From);
            var subject = model.Subject;
            var to = new EmailAddress(mailDestinationSettings.To);
            var plainTextContent = model.Body;

            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, "");

            if (model.Attachment != null)
            {
                var attachement = GetAttachmentFromFormFile(model.Attachment);
                msg.AddAttachment(attachement.Filename, attachement.Content);
            }

            return msg;
        }

        public IActionResult Index()
        {
            return View();
        }

        public bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Send(ContactViewModel model)
        {
            if (string.IsNullOrEmpty(model.Name) ||
                string.IsNullOrEmpty(model.Subject) ||
                string.IsNullOrEmpty(model.Body) ||
                string.IsNullOrEmpty(model.Email) ||
                !IsValid(model.Email))
            {
                if (string.IsNullOrEmpty(model.Name))
                    ModelState.AddModelError("Name", L("Form-Name-Required"));
                if (string.IsNullOrEmpty(model.Subject))
                    ModelState.AddModelError("Subject", L("Form-Title-Required"));
                if (string.IsNullOrEmpty(model.Body))
                    ModelState.AddModelError("Body", L("Form-Message-Required"));
                if (string.IsNullOrEmpty(model.Email))
                    ModelState.AddModelError("Email", L("Form-Email-Required"));
                else if(!IsValid(model.Email))
                    ModelState.AddModelError("Email", L("Form-Email-ErrorFormat"));
                return View("index", model);
            }

            model.Body += Environment.NewLine;
            model.Body += model.Email;

            TempData["success"] = true;

            var message = GetSendGridMessage(model);

            await SendMail(message);

            return RedirectToAction("Index");
        }

        private async Task SendMail(SendGridMessage message)
        {
            var smtpServiceSettings = _smtpServiceSettings.Value;
            var client = new SendGridClient(smtpServiceSettings.ApiKey);
            var response = await client.SendEmailAsync(message);
        }
    }
}