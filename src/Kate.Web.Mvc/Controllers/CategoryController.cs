﻿using Microsoft.AspNetCore.Mvc;
using Kate.DynamicMenu;
using Kate.Controllers;

namespace Kate.Web.Mvc.Controllers
{
    public class CategoryController : KateControllerBase
    {
        private readonly BaseDynamicMenuService _dynamicMenuService;

        public CategoryController(BaseDynamicMenuService dynamicMenuService)
        {
            _dynamicMenuService = dynamicMenuService;
        }
        
        public IActionResult MiddleLevel(int id)
        {
            var model = _dynamicMenuService.GetCategoryMiddleLevel(id);
            ViewBag.CategoryNames = model.CategoryNames;
            return View(model);
        }
    }
}