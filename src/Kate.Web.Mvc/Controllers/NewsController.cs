﻿using Kate.Article;
using Kate.Article.ViewModels;
using Kate.Controllers;
using Kate.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Kate.Web.Mvc.Controllers
{
    public class NewsController : KateControllerBase
    {
        private readonly BaseArticleAppService _articleService;

        public NewsController(BaseArticleAppService articleService)
        {
            _articleService = articleService;
        }

        public IActionResult Index(int id, int? page)
        {
            var CurrentPage = page ?? 1;
            var data = _articleService.GetNewsFeed(id);
            var model = new PageingHelper<NewsFeedViewModel>(data.Data, CurrentPage, 10);
            ViewBag.CategoryNames = data.CategoryNames;
            return View(model);
        }

        public IActionResult ReadMore(int id)
        {
            var model = _articleService.GetNews(id);
            ViewBag.CategoryNames = model.CategoryNames;
            return View(model);
        }
        public IActionResult Search(string search)
        {
            var Model = _articleService.GlobalSearch(search, true);
            var Articles = Model.Select(x => x.Article);
            return View(Articles);
        }
    }
}