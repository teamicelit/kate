﻿using Kate.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Kate.Web.Mvc.Controllers
{
    public class PublicationsController : KateControllerBase
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult General()
        {
            return View();
        }

        public IActionResult StatisticalData()
        {
            return View();
        }

        public IActionResult Annual()
        {
            return View();
        }
    }
}