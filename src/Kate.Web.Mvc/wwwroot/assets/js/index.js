var Main = function () {
    this.initializeJqueryEvents();
    this.initializeOptions();
}

Main.prototype.initializeJqueryEvents = function () {
    var self = this;

    $('[data-font-opt]').click(function () {
        var fontSize = parseInt($('html, body').css('font-size'));
        var canIncrease = $(this).data('font-opt') === 1;
        var calculatedFontSize = canIncrease ? ++fontSize : --fontSize;

        self.saveOption('font-size', calculatedFontSize);

        $('html, body').css('font-size', calculatedFontSize);
    });

    this.descriptionAddTabindex();

    $('*').focus(function () {
        speak($(this).attr('title') || $(this).text());
        $(this).addClass('readed');
    });
}

Main.prototype.initializeOptions = function () {
    this.appendSavedFontSize();
}

Main.prototype.objectToJsonStringify = function (value) {
    return JSON.stringify(value);
}

Main.prototype.jsonStringifyToObject = function (value) {
    return JSON.parse(value);
}

Main.prototype.saveOption = function (name, value) {
    var stringifyValue = this.objectToJsonStringify(value);

    localStorage.setItem(name, stringifyValue);
}

Main.prototype.getOption = function (name) {
    var localStorageGetResult = localStorage.getItem(name);

    return this.jsonStringifyToObject(localStorageGetResult);
}

Main.prototype.appendSavedFontSize = function () {
    var fontSize = this.getOption('font-size');

    $('html, body').css('font-size', fontSize);
}

Main.prototype.descriptionAddTabindex = function () {
    var $description = $('.description');

    if (!$description.html()) {
        return;
    }

    var descriptionTexts = $('.description').html().split('.');
    var html = "";

    var lastTabIndex = document.querySelectorAll('button, a[href], input, select, textarea, [tabindex]:not([tabindex="-1"])');

    for (var i = 0; i < descriptionTexts.length; i++) {
        html += "<p>" + descriptionTexts[i] + "</p>";
    }

    $('.description').html(html);

    $('p').addClass('item');

    $('.item').each(function () {
        $(this).attr('tabindex', $('.item').index($(this)) + 1);
    });
}

var main = new Main();