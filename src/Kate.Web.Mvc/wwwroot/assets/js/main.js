﻿var ctx = new (window.AudioContext || window.webkitAudioContext)();
var convolver = ctx.createConvolver();
convolver.connect(ctx.destination);
var espeak;
var pusher;

function stop() {
    if (pusher) {
        pusher.disconnect();
        pusher = null;
    }
}

function speak(preach) {
    stop();
    var samples_queue = [];

    espeak.set_rate(150);
    espeak.set_pitch(20);
    espeak.setVoice.apply(espeak, ["default", window.currentLanguage]);

    var now = Date.now();

    pusher = new PushAudioNode(ctx);

    pusher.connect(ctx.destination);

    espeak.synth(preach,
        function (samples, events) {
            if (!samples) {
                pusher.close();
                return;
            }
            pusher.push(new Float32Array(samples));
            if (now) {
                now = 0;
            }
        });
}

espeak = new Espeak('/assets/js/sw.worker.js', function cb() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/data/church-schellingwoude.ogg', true);
    xhr.responseType = 'arraybuffer';

    function convolverLoadFail(e) {
        console.log("Error with decoding audio data", e);
        document.getElementById('preachit').disabled = true;
        document.body.classList.remove('loading');
    }

    xhr.onerror = convolverLoadFail;

    xhr.onload = function () {
        var audioData = xhr.response;
        ctx.decodeAudioData(audioData, function (buffer) {
            convolver.buffer = buffer;
            document.body.classList.remove('loading');
        }, convolverLoadFail);
    }
    xhr.send();
});