﻿function initMap() {
    var center = { lat: 41.686657, lng: 44.827574 };
    var adress = { lat: 41.686319, lng: 44.8287456 };
    if (document.getElementById('map') !== null) {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: center
        });
        var marker = new google.maps.Marker({
            position: adress,
            map: map
        });
    }
    if (document.getElementById('popup-map') !== null) {
        var map = new google.maps.Map(document.getElementById('popup-map'), {
            zoom: 17,
            center: center
        });
        var marker = new google.maps.Marker({
            position: adress,
            map: map
        });
    }
} 
$(document).on("click", ".footer-map", function () {
    swal({
        title: Address,
        html:
        '<div id="popup-map" class="popup-map"></div>',
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        showConfirmButton: false
    });
    initMap();
});