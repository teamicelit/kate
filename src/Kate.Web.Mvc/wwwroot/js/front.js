﻿
$(document).on("click", ".dropdown-toggle", function () {
    if ($(window).width() > 995) {
        window.location.href = $(this).attr("href");
        return;
    }

});
setTimeout(function () {
    $('#myCarousel').bind('slide.bs.carousel', function (e) {
        var active = $(e.target).find('.carousel-inner > .item.active');
        var from = active.index();
        var slidePointer = $(e.relatedTarget);
        var text = slidePointer.data("text") + "...";
        var name = slidePointer.data("name");
        var id = slidePointer.data("id");
        var articleLinkPointer = $("#carusel-article-read-more-link");
        var urlFragments = articleLinkPointer.attr("href").split("/");
        urlFragments[urlFragments.length - 1] = id;
        var newUrl = urlFragments.join("/");
        $("#carusel-article-conteiner").html("<div style='display:inline'><p>" + text + "</p></div>");
        $("#carusel-article-name").html(name);
        articleLinkPointer.attr("href", newUrl);
    });
}, 3000);


$(document).on("change", "#Attachment", function () {
    var fakepath = $(this).val();
    var pragments = fakepath.split('\\');
    $("#FilePath").text(pragments[pragments.length - 1]);
});
$(document).on("keydown", "#Body", function (e) {
    if (e.keyCode == 8)
        $("#Body").trigger('keypress');
});
$(document).on("keypress", "#Body", function () {
    var counterPointer = $("#body-text-counter");
    var limit = 350;
    var currentText = $(this).val();
    if (currentText.length + 1 > limit) {
        $(this).val(currentText.substring(0, limit));
        counterPointer.text("0");
    }
    else {
        counterPointer.text(limit - currentText.length + 1);
    }

});
$(document).on("click", ".qestion-continer", function () {
    if ($(this).parent().find(".unswear-continer").hasClass('t')) {
        $("#question-unswer").find(".unswear-continer").slideUp().removeClass('t');
        return;
    }

    $("#question-unswer").find(".unswear-continer").slideUp().removeClass('t');
    $(this).parent().find(".unswear-continer").slideDown().addClass('t');
});
