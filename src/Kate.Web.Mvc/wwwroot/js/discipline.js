﻿var saveAs = saveAs || "undefined" != typeof navigator && navigator.msSaveOrOpenBlob && navigator.msSaveOrOpenBlob.bind(navigator) || function (e) { "use strict"; if ("undefined" == typeof navigator || !/MSIE [1-9]\./.test(navigator.userAgent)) { var t = e.document, n = function () { return e.URL || e.webkitURL || e }, o = t.createElementNS("http://www.w3.org/1999/xhtml", "a"), r = !e.externalHost && "download" in o, i = e.webkitRequestFileSystem, a = e.requestFileSystem || i || e.mozRequestFileSystem, u = function (t) { (e.setImmediate || e.setTimeout)(function () { throw t }, 0) }, c = "application/octet-stream", d = 0, f = [], s = function () { for (var e = f.length; e--;) { var t = f[e]; "string" == typeof t ? n().revokeObjectURL(t) : t.remove() } f.length = 0 }, l = function (e, t, n) { for (var o = (t = [].concat(t)).length; o--;) { var r = e["on" + t[o]]; if ("function" == typeof r) try { r.call(e, n || e) } catch (e) { u(e) } } }, v = function (u, s) { var v, w, p, y, h, m = this, E = u.type, g = !1, O = function () { var e = n().createObjectURL(u); return f.push(e), e }, S = function () { l(m, "writestart progress write writeend".split(" ")) }, R = function () { !g && v || (v = O()), w ? w.location.href = v : window.open(v, "_blank"), m.readyState = m.DONE, S() }, b = function (e) { return function () { if (m.readyState !== m.DONE) return e.apply(this, arguments) } }, N = { create: !0, exclusive: !1 }; if (m.readyState = m.INIT, s || (s = "download"), r) return v = O(), o.href = v, o.download = s, y = o, (h = t.createEvent("MouseEvents")).initMouseEvent("click", !0, !1, e, 0, 0, 0, 0, 0, !1, !1, !1, !1, 0, null), y.dispatchEvent(h), m.readyState = m.DONE, void S(); e.chrome && E && E !== c && (p = u.slice || u.webkitSlice, u = p.call(u, 0, u.size, c), g = !0), i && "download" !== s && (s += ".download"), (E === c || i) && (w = e), a ? (d += u.size, a(e.TEMPORARY, d, b(function (e) { e.root.getDirectory("saved", N, b(function (e) { var t = function () { e.getFile(s, N, b(function (e) { e.createWriter(b(function (t) { t.onwriteend = function (t) { w.location.href = e.toURL(), f.push(e), m.readyState = m.DONE, l(m, "writeend", t) }, t.onerror = function () { var e = t.error; e.code !== e.ABORT_ERR && R() }, "writestart progress write abort".split(" ").forEach(function (e) { t["on" + e] = m["on" + e] }), t.write(u), m.abort = function () { t.abort(), m.readyState = m.DONE }, m.readyState = m.WRITING }), R) }), R) }; e.getFile(s, { create: !1 }, b(function (e) { e.remove(), t() }), b(function (e) { e.code === e.NOT_FOUND_ERR ? t() : R() })) }), R) }), R)) : R() }, w = v.prototype, p = function (e, t) { return new v(e, t) }; return w.abort = function () { this.readyState = this.DONE, l(this, "abort") }, w.readyState = w.INIT = 0, w.WRITING = 1, w.DONE = 2, w.error = w.onwritestart = w.onprogress = w.onwrite = w.onabort = w.onerror = w.onwriteend = null, e.addEventListener("unload", s, !1), p.unload = function () { s(), e.removeEventListener("unload", s, !1) }, p } }("undefined" != typeof self && self || "undefined" != typeof window && window || this.content); "undefined" != typeof module && null !== module ? module.exports = saveAs : "undefined" != typeof define && null !== define && null != define.amd && define([], function () { return saveAs }); $('[type="checkbox"]').change(function () {
    var isChecked = $(this).is(':checked');
    console.log(isChecked);
    if (isChecked)
        $(this).attr('checked', 'true');
    else
        $(this).removeAttr('checked');
});

$('.download').click(function () {
    $(this).hide();
    $(this).text('');
    var element = document.documentElement.outerHTML;
    html2pdf().from(element).set({
        margin: .3,
        filename: 'myfile.pdf',
        image: { type: 'jpeg', quality: 1 },
        jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
    }).output('blob').then(function (r) {
        var formData = new FormData();
        formData.append('file', r);

        $('#loading-wrapper').show();
        $('html, body').css('overflow', 'hidden');
        var origin = location.origin;
        $.ajax({
            type: "POST",
            url: "/DocumentSign/UploadPdf",
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
                location.href = 'geoeid-unitool://' + origin + '/DocumentSign/getJsonDocument?fileName=' + data.url;

                startDownloading(data, data.id);
            }
        });
    });

    function startDownloading(data, id) {
        if (!data.isProcessed) {
            $.post('/DocumentSign/ValidateProgress', { id: id }, function (res) {
                setTimeout(function() {
                    startDownloading(res.result, id);
                }, 2000);
            });

            return;
        }

        function downloadURI(uri, name) {
            var link = document.createElement("a");
            link.download = name;
            link.href = uri;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            delete link;

            $('#loading-wrapper').hide();
            $('html, body').css('overflow', '');
        }

        downloadURI(data.filePath, "document.pdf");
    }

    $(this).show();
    $(this).text('გადმოწერა');
});

$(document).on('paste', '[contenteditable]', function (e) {
    e.preventDefault();
    var text = (e.originalEvent || e).clipboardData.getData('text/plain');
    window.document.execCommand('insertText', false, text);
});