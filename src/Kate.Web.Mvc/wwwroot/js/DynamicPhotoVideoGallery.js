﻿$(document).on("click", "#AddDynamicPhotoVideoGalleryInput", function () {
    var fileUploadContiner = $("#PhotoVideoGalleryFileUploadConteiner");
    var indexer = fileUploadContiner.find(".card").length;
    var Template = $("#FileUploadTemplete").html().split("{index}").join(indexer);
    fileUploadContiner.append(Template);
});
$(document).on("click", ".DeleteDynamicPhotoVideoGalleryInput", function () {
    var fileUploadContiner = $(this).closest(".card");
    fileUploadContiner.find("input").val("");
    fileUploadContiner.hide();
});