﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kate.Controllers;
using Kate.PhotoVideoGallery;
using Kate.PhotoVideoGallery.Dto;
using Kate.UploadFile;
using Kate.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Abp.AspNetCore.Mvc.Authorization;

namespace Kate.Web.Mvc.Areas.Admin.Controllers
{
    [Area("Admin")]
    [AbpMvcAuthorize]
    public class PhotoVideoGalleryController : KateControllerBase
    {
        private readonly BasePhotoVideoGalleryAppService _PhotoVideoGalleryService;
        private readonly IOptions<FilePathSetting> _filePath;
        private readonly IHostingEnvironment _appEnvironment;

        public PhotoVideoGalleryController(BasePhotoVideoGalleryAppService PhotoVideoGalleryService, IOptions<FilePathSetting> filePath, IHostingEnvironment appEnvironment)
        {
            _PhotoVideoGalleryService = PhotoVideoGalleryService;
            _filePath = filePath;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            var model = _PhotoVideoGalleryService.GetTableViewModels();

            return View(model);
        }

        public IActionResult Manage(int id)
        {
            var model = _PhotoVideoGalleryService.GetViewModel(id);

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Save(PhotoVideoGalleryManageDto model)
        {
            if(!ModelState.IsValid)
            {
                return View(model);
            }
            model.Files = model.Files.Where(a => a.File != null || a.FilePath != null || a.Embed != null);

            foreach (var File in model.Files.Where(a => a.File != null && a.File.Length != 0))
            {
                File.FilePath = await SaveFileOnDisk.ProcessFormFile(File.File, ModelState, _appEnvironment.WebRootPath + _filePath.Value.ImageFolderPath);
            }
            if(!ModelState.IsValid)
            {
                return View(model);
            }


            _PhotoVideoGalleryService.Save(model);

            return RedirectToAction("index");
        }
    }
}