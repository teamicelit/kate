﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Kate.Controllers;
using Kate.AboutUs;
using Kate.Localization;
using Kate.AboutUs.Dto;

namespace Kate.Web.Controllers
{
    [Area("Admin")]
    [AbpMvcAuthorize]
    public class AboutController : KateControllerBase
    {
        private readonly BaseAboutUsAppService _aboutUsService;

        public AboutController(BaseAboutUsAppService aboutUsServise)
        {
            _aboutUsService = aboutUsServise;
        }

        public ActionResult Index()
        {
            var model = _aboutUsService.GetAboutUsReadViewModel();

            return View(model);
        }

        public IActionResult Manage()
        {
            var model = _aboutUsService.GetAboutUsManageViewModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Save(AboutUsDto model)
        {
            if (ModelState.IsValid)
                _aboutUsService.Update(model);
            else
                return View(model);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _aboutUsService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
