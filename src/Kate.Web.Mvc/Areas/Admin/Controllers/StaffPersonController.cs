﻿using Microsoft.AspNetCore.Mvc;
using Kate.StaffPerson;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Kate.Utils;
using Kate.Services.GenericDtos;
using Kate.StaffPerson.Dto;
using Kate.UploadFile;
using Kate.Controllers;
using Abp.AspNetCore.Mvc.Authorization;

namespace Kate.Web.Mvc.Areas.Admin.Controllers
{
    [Area("Admin")]
    [AbpMvcAuthorize]
    public class StaffPersonController : KateControllerBase
    {
        private readonly BaseStaffPersonAppService _staffPersonService;
        private readonly IOptions<FilePathSetting> _filePath;
        private readonly IHostingEnvironment _appEnvironment;

        [BindProperty]
        public FileUpload FileUpload { get; set; }

        public StaffPersonController(BaseStaffPersonAppService staffPersonService, IOptions<FilePathSetting> filePath, IHostingEnvironment appEnvironment)
        {
            _staffPersonService = staffPersonService;
            _filePath = filePath;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            var model = _staffPersonService.GetTableViewModels();

            return View(model);
        }

        public IActionResult Manage(int id)
        {
            var model = _staffPersonService.GetViewModel(id);

            return View(model);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> Save(StaffPersonManageDto model)
        {
            if (!ModelState.IsValid)
            {
                _staffPersonService.FillManageDtoWithInitialData(model);
                return View("Manage", model);
            }

            if (FileUpload.UploadPdf != null && FileUpload.UploadPdf.Length != 0)
                model.PdfFilePath = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadPdf, ModelState, _appEnvironment.WebRootPath + _filePath.Value.PdfWordFolderPath);
            if (FileUpload.UploadWord != null && FileUpload.UploadWord.Length != 0)
                model.WordFilePath = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadWord, ModelState, _appEnvironment.WebRootPath + _filePath.Value.PdfWordFolderPath);
            if (FileUpload.UploadPicture != null && FileUpload.UploadPicture.Length != 0)
                model.MainPhotoPath = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadPicture, ModelState, _appEnvironment.WebRootPath + _filePath.Value.ImageFolderPath);

            if (!ModelState.IsValid)
            {
                _staffPersonService.FillManageDtoWithInitialData(model);
                return View("Manage", model);
            }
            else
                _staffPersonService.Save(model);

            return RedirectToAction("index");
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            _staffPersonService.Delete(id);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _staffPersonService.Dispose();

            base.Dispose(disposing);
        }
    }
}