﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kate.Controllers;
using Kate.PartnerCompany;
using Kate.PartnerCompany.Dto;
using Kate.Services.GenericDtos;
using Kate.UploadFile;
using Kate.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Abp.AspNetCore.Mvc.Authorization;

namespace Kate.Web.Mvc.Areas.Admin.Controllers
{
    [Area("Admin")]
    [AbpMvcAuthorize]
    public class PartnerCompanyController : KateControllerBase
    {
        private readonly BasePartnerCompanyAppService _partnerCompanyService;
        private readonly IOptions<FilePathSetting> _filePath;
        private readonly IHostingEnvironment _appEnvironment;

        [BindProperty]
        public FileUpload FileUpload { get; set; }

        public PartnerCompanyController(BasePartnerCompanyAppService partnerCompanyService, IOptions<FilePathSetting> filePath, IHostingEnvironment appEnvironment)
        {
            _partnerCompanyService = partnerCompanyService;
            _filePath = filePath;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            var model = _partnerCompanyService.GetTableViewModels();

            return View(model);
        }

        public IActionResult Manage(int id)
        {
            var model = _partnerCompanyService.GetViewModel(id);

            return View(model);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> Save(PartnerCompanyManageDto model)
        {
            if (ModelState.IsValid)
            {
                if (FileUpload.UploadPicture != null && FileUpload.UploadPicture.Length != 0)
                    model.PartnerLogo = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadPicture, ModelState, _appEnvironment.WebRootPath + _filePath.Value.ImageFolderPath);

                _partnerCompanyService.Save(model);

                return RedirectToAction("index");
            }
            else
                return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            _partnerCompanyService.Delete(id);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _partnerCompanyService.Dispose();

            base.Dispose(disposing);
        }
    }
}