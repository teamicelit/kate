﻿using Microsoft.AspNetCore.Mvc;
using Kate.Controllers;
using Kate.DynamicMenu;
using Kate.DynamicMenu.Dto;
using Kate.Utils;
using Microsoft.Extensions.Options;
using Kate.Services.GenericDtos;
using Kate.UploadFile;
using Microsoft.AspNetCore.Hosting;
using Abp.AspNetCore.Mvc.Authorization;

namespace Kate.Web.Mvc.Controllers
{
    [Area("Admin")]
    [AbpMvcAuthorize]
    public class DynamicMenuController : KateControllerBase
    {
        private readonly BaseDynamicMenuService _dynamicMenuService;
        private readonly IOptions<FilePathSetting> _filePath;
        private readonly IHostingEnvironment _appEnvironment;

        [BindProperty]
        public FileUpload FileUpload { get; set; }

        public DynamicMenuController(BaseDynamicMenuService dynamicMenuService, IOptions<FilePathSetting> filePath, IHostingEnvironment appEnvironment)
        {
            _dynamicMenuService = dynamicMenuService;
            _filePath = filePath;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            var model = _dynamicMenuService.GetTableViewModels();

            return View(model);
        }

        public IActionResult Manage(int id)
        {
            var model = _dynamicMenuService.GetViewModel(id);

            return View(model);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> Save(DynamicMenuManageDto model)
        {
            if (!ModelState.IsValid)
            {
                _dynamicMenuService.FillManageDtoWithInitialData(model);
                return View("Manage",model);
            }

            if(FileUpload.UploadPdf != null && FileUpload.UploadPdf.Length != 0)
                model.PdfFilePath = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadPdf, ModelState, _appEnvironment.WebRootPath + _filePath.Value.PdfWordFolderPath);
            if (FileUpload.UploadWord != null && FileUpload.UploadWord.Length != 0)
                model.WordFilePath = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadWord, ModelState, _appEnvironment.WebRootPath + _filePath.Value.PdfWordFolderPath);
            if (FileUpload.UploadPicture != null && FileUpload.UploadPicture.Length != 0)
                model.MainPhotoPath = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadPicture, ModelState, _appEnvironment.WebRootPath + _filePath.Value.ImageFolderPath);
            
            if (!ModelState.IsValid)
            {
                _dynamicMenuService.FillManageDtoWithInitialData(model);
                return View("Manage",model);
            }
            else
                _dynamicMenuService.Save(model);

            return RedirectToAction("index");
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            _dynamicMenuService.Delete(id);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dynamicMenuService.Dispose();

            base.Dispose(disposing);
        }
    }
}