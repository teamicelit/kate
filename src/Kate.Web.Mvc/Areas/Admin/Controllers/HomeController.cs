﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Kate.Controllers;

namespace Kate.Web.Controllers
{
    [Area("Admin")]
    [AbpMvcAuthorize]
    public class HomeController : KateControllerBase
    {

        public HomeController()
        {
        }

        public ActionResult Index()
        {
            return View();
        }
	}
}