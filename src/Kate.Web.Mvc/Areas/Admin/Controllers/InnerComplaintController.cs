﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Kate.InnerComplaint;
using Kate.Controllers;
using Abp.AspNetCore.Mvc.Authorization;
using Kate.InnerComplaint.Dto;

namespace Kate.Web.Mvc.Areas.Admin.Controllers
{
    [Area("Admin")]
    [AbpMvcAuthorize]
    public class InnerComplaintController : KateControllerBase
    {
        private readonly BaseInnerComplaintAppService _innerComplaintService;

        public InnerComplaintController(BaseInnerComplaintAppService innerComplaintService)
        {
           _innerComplaintService = innerComplaintService;
        }

        public ActionResult Index()
        {
            var model = _innerComplaintService.GetAboutUsReadViewModel();

            return View(model);
        }

        public IActionResult Manage()
        {
            var model = _innerComplaintService.GetAboutUsManageViewModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Save(InnerComplaintDto model)
        {
            if (ModelState.IsValid)
                _innerComplaintService.Update(model);
            else
                return View(model);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _innerComplaintService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}