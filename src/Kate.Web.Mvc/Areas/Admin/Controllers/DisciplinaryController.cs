﻿using Abp.AspNetCore.Mvc.Authorization;
using Kate.Controllers;
using Kate.Disciplinary;
using Kate.Disciplinary.Dto;
using Kate.Services.GenericDtos;
using Kate.UploadFile;
using Kate.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Kate.Web.Mvc.Areas.Admin.Controllers
{
    [Area("Admin")]
    [AbpMvcAuthorize]
    public class DisciplinaryController : KateControllerBase
    {
        private readonly BaseDisciplinaryAppService _disciplinaryAppService;
        private readonly IOptions<FilePathSetting> _filePath;
        private readonly IHostingEnvironment _appEnvironment;

        [BindProperty]
        public FileUpload FileUpload { get; set; }

        public DisciplinaryController(BaseDisciplinaryAppService disciplinaryAppService, IOptions<FilePathSetting> filePath, IHostingEnvironment appEnvironment)
        {
            _disciplinaryAppService = disciplinaryAppService;
            _filePath = filePath;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            var model = _disciplinaryAppService.GetTableViewModels();

            return View(model);
        }

        public IActionResult Manage(int id)
        {
            var model = _disciplinaryAppService.GetViewModel(id);

            return View(model);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> Save(DisciplinaryManageDto model)
        {
            if (!ModelState.IsValid)
            {
                _disciplinaryAppService.FillManageDtoWithInitialData(model);
                return View("Manage", model);
            }

            if (FileUpload.UploadPdf != null && FileUpload.UploadPdf.Length != 0)
                model.PdfFilePath = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadPdf, ModelState, _appEnvironment.WebRootPath + _filePath.Value.PdfWordFolderPath);
            if (FileUpload.UploadWord != null && FileUpload.UploadWord.Length != 0)
                model.WordFilePath = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadWord, ModelState, _appEnvironment.WebRootPath + _filePath.Value.PdfWordFolderPath);
            if (FileUpload.UploadPicture != null && FileUpload.UploadPicture.Length != 0)
                model.MainPhotoPath = await SaveFileOnDisk.ProcessFormFile(FileUpload.UploadPicture, ModelState, _appEnvironment.WebRootPath + _filePath.Value.ImageFolderPath);

            if (!ModelState.IsValid)
            {
                _disciplinaryAppService.FillManageDtoWithInitialData(model);
                return View("Manage", model);
            }
            else
                _disciplinaryAppService.Save(model);

            return RedirectToAction("index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            _disciplinaryAppService.Delete(id);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _disciplinaryAppService.Dispose();

            base.Dispose(disposing);
        }
    }
}