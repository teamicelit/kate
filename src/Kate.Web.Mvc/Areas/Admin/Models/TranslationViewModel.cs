﻿using Kate.Services.GenericDtos;

namespace Kate.Web.Areas.Admin.Models
{
    public class TranslationViewModel
    { 
        public int Indexer { get; set; }

        public StandartTranslationDto Translation { get; set; }
    }
}
