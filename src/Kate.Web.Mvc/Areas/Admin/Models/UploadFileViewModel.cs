﻿namespace Kate.Web.Areas.Admin.Models
{
    public class UploadFileViewModel
    {
        public string FileUserReadableName { get; set; }        
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
    }
}
