﻿using Kate.Services.GenericDtos;
using System.Collections.Generic;

namespace Kate.Web.Areas.Admin.Models
{
    public class DropdownWiewModel
    {
        public IEnumerable<DropdownDto> Dropdown { get; set; }

        public string DropdownName { get; set; }

        public string BindName { get; set; }
    }
}
