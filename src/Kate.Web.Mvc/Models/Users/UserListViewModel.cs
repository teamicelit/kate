using System.Collections.Generic;
using Kate.Roles.Dto;
using Kate.Users.Dto;

namespace Kate.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
