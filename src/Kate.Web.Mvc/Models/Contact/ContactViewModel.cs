﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Abp.Localization;
using Microsoft.AspNetCore.Http;

namespace Kate.Web.Models.Contact
{
    public class ContactViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }        
        public string Subject { get; set; }
        public string Body { get; set; }
        
        public IFormFile Attachment { get; set; }
    }
}
