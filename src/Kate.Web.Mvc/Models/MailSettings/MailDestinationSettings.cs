﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kate.Web.Models.EmailSettings
{
    public class MailDestinationSettings
    {
        public string From { get; set; }
        public string To { get; set; }
             
    }
}
