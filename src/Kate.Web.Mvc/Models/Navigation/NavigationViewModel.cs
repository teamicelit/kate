﻿using System.Collections.Generic;
using Kate.DynamicMenu.ViewModels;

namespace Kate.Web.Models.Navigation
{
    public class NavigationViewModel
    {
        public IEnumerable<CategoryMenuViewModel> Categories { get; set; }

        public bool IsVoicePage { get; set; }
    }
}