﻿using Kate.DynamicMenu.ViewModels;

namespace Kate.Web.Models.Navigation
{
    public class NavigationCategoryMenuViewModel
    {
        public CategoryMenuViewModel PrincipalCategory { get; set; }

        public bool IsVoicePage { get; set; }
    }
}
