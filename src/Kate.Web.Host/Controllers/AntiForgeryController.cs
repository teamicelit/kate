using Microsoft.AspNetCore.Antiforgery;
using Kate.Controllers;

namespace Kate.Web.Host.Controllers
{
    public class AntiForgeryController : KateControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
