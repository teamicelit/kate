﻿using Kate.Services.GenericDtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace Kate.UploadFile
{
    public class SaveFileOnDisk
    {
        public static async Task<string> ProcessFormFile(IFormFile formFile, ModelStateDictionary modelState, string filePath)
        {
            var fieldDisplayName = string.Empty;

            MemberInfo property =
                typeof(FileUpload).GetProperty(formFile.Name.Substring(formFile.Name.IndexOf(".") + 1));

            if (property != null)
            {
                var displayAttribute =
                    property.GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute;

                if (displayAttribute != null)
                    fieldDisplayName = $"{displayAttribute.Name} ";
            }

            var fileName = WebUtility.HtmlEncode(Path.GetFileName(formFile.FileName));
            var fileNameInFolder = Guid.NewGuid().ToString().Replace("-", string.Empty) + Path.GetExtension(formFile.FileName);
            
            try
            {
                using (var fileStream = new FileStream(Path.Combine(filePath, fileNameInFolder), FileMode.Create))
                {
                    await formFile.CopyToAsync(fileStream);
                }
            }
            catch (Exception ex)
            {
                modelState.AddModelError(formFile.Name,
                                         $"The {fieldDisplayName}file ({fileName}) upload failed. " +
                                         $"Please contact the Help Desk for support. Error: {ex.Message}");
            }
            
            return fileNameInFolder;
        }
    }
}
