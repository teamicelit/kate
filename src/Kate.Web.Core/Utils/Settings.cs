﻿namespace Kate.Utils
{
    public class FilePathSetting
    {
        public string ImageFolderPath { get; set; }
        public string PdfWordFolderPath { get; set; }
    }
}
