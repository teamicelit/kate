﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class categorytypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DynamicMenuType",
                table: "StaffPersons");

            migrationBuilder.DropColumn(
                name: "DynamicMenuType",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "DynamicMenuType",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "DynamicMenuType",
                table: "Articles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DynamicMenuType",
                table: "StaffPersons",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DynamicMenuType",
                table: "PhotoVideoGalleries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DynamicMenuType",
                table: "Decisions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DynamicMenuType",
                table: "Articles",
                nullable: false,
                defaultValue: 0);
        }
    }
}
