﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class addrelationbetweencategoryandschemeitems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SchemeItem_Categories_CategoryId",
                table: "SchemeItem");

            migrationBuilder.AddForeignKey(
                name: "FK_SchemeItem_Categories_CategoryId",
                table: "SchemeItem",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SchemeItem_Categories_CategoryId",
                table: "SchemeItem");

            migrationBuilder.AddForeignKey(
                name: "FK_SchemeItem_Categories_CategoryId",
                table: "SchemeItem",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
