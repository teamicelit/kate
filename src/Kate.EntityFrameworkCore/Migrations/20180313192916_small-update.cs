﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class smallupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionName",
                table: "Categories");

            migrationBuilder.RenameColumn(
                name: "LinkedFileName",
                table: "Categories",
                newName: "WordFileName");

            migrationBuilder.RenameColumn(
                name: "IsHeaderCategory",
                table: "Categories",
                newName: "ShowOnMainPageBox");

            migrationBuilder.RenameColumn(
                name: "ControllerName",
                table: "Categories",
                newName: "PdfFileName");

            migrationBuilder.AddColumn<bool>(
                name: "ShowOnMainPageBox",
                table: "PhotoVideoGalleries",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ShowOnMainPageBox",
                table: "PartnerCompanies",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ShowOnMainPageBox",
                table: "Disciplinaries",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ShowOnMainPageBox",
                table: "Decisions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ShowOnMainPageBox",
                table: "Articles",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShowOnMainPageBox",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "ShowOnMainPageBox",
                table: "PartnerCompanies");

            migrationBuilder.DropColumn(
                name: "ShowOnMainPageBox",
                table: "Disciplinaries");

            migrationBuilder.DropColumn(
                name: "ShowOnMainPageBox",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "ShowOnMainPageBox",
                table: "Articles");

            migrationBuilder.RenameColumn(
                name: "WordFileName",
                table: "Categories",
                newName: "LinkedFileName");

            migrationBuilder.RenameColumn(
                name: "ShowOnMainPageBox",
                table: "Categories",
                newName: "IsHeaderCategory");

            migrationBuilder.RenameColumn(
                name: "PdfFileName",
                table: "Categories",
                newName: "ControllerName");

            migrationBuilder.AddColumn<string>(
                name: "ActionName",
                table: "Categories",
                nullable: true);
        }
    }
}
