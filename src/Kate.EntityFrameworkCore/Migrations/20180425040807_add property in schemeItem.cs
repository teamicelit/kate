﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class addpropertyinschemeItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ConnectToId",
                table: "SchemeItem",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SourceId",
                table: "SchemeItem",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConnectToId",
                table: "SchemeItem");

            migrationBuilder.DropColumn(
                name: "SourceId",
                table: "SchemeItem");
        }
    }
}
