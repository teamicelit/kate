﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class lastupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Mail",
                table: "StaffPersons",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "StaffPersons",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ShowInSideNavigation",
                table: "Categories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Test",
                table: "Categories",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Mail",
                table: "StaffPersons");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "StaffPersons");

            migrationBuilder.DropColumn(
                name: "ShowInSideNavigation",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "Test",
                table: "Categories");
        }
    }
}
