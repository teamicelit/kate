﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class staffpersoneditedcvfileadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HeaderText",
                table: "StaffPersonTranslations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileUserReadableName",
                table: "StaffPersons",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MainPhotoPath",
                table: "StaffPersons",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PdfFilePath",
                table: "StaffPersons",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WordFilePath",
                table: "StaffPersons",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HeaderText",
                table: "StaffPersonTranslations");

            migrationBuilder.DropColumn(
                name: "FileUserReadableName",
                table: "StaffPersons");

            migrationBuilder.DropColumn(
                name: "MainPhotoPath",
                table: "StaffPersons");

            migrationBuilder.DropColumn(
                name: "PdfFilePath",
                table: "StaffPersons");

            migrationBuilder.DropColumn(
                name: "WordFilePath",
                table: "StaffPersons");
        }
    }
}
