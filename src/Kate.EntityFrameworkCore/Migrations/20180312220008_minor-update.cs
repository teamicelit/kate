﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class minorupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DisciplinaryId",
                table: "SearchTags",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DynamicMenuType",
                table: "PhotoVideoGalleries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DynamicMenuType",
                table: "Decisions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DynamicMenuType",
                table: "Articles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Disciplinaries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    MainPhotoName = table.Column<string>(nullable: true),
                    PdfFileName = table.Column<string>(nullable: true),
                    ShowOnSite = table.Column<bool>(nullable: false),
                    SortIndex = table.Column<int>(nullable: false),
                    VideoEmbed = table.Column<string>(nullable: true),
                    WordFileName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Disciplinaries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StaffPersons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CategoryId = table.Column<int>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DynamicMenuType = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ShowOnSite = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StaffPersons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StaffPersons_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DisciplinaryTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DisciplinaryId = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LanguageId = table.Column<int>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    MainText = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DisciplinaryTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DisciplinaryTranslations_Disciplinaries_DisciplinaryId",
                        column: x => x.DisciplinaryId,
                        principalTable: "Disciplinaries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DisciplinaryTranslations_DataLanguages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "DataLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StaffPersonTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LanguageId = table.Column<int>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ResumeDescription = table.Column<string>(nullable: true),
                    StaffPersonId = table.Column<int>(nullable: false),
                    Surname = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StaffPersonTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StaffPersonTranslations_DataLanguages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "DataLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StaffPersonTranslations_StaffPersons_StaffPersonId",
                        column: x => x.StaffPersonId,
                        principalTable: "StaffPersons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SearchTags_DisciplinaryId",
                table: "SearchTags",
                column: "DisciplinaryId");

            migrationBuilder.CreateIndex(
                name: "IX_DisciplinaryTranslations_DisciplinaryId",
                table: "DisciplinaryTranslations",
                column: "DisciplinaryId");

            migrationBuilder.CreateIndex(
                name: "IX_DisciplinaryTranslations_LanguageId",
                table: "DisciplinaryTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_StaffPersons_CategoryId",
                table: "StaffPersons",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_StaffPersonTranslations_LanguageId",
                table: "StaffPersonTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_StaffPersonTranslations_StaffPersonId",
                table: "StaffPersonTranslations",
                column: "StaffPersonId");

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Disciplinaries_DisciplinaryId",
                table: "SearchTags",
                column: "DisciplinaryId",
                principalTable: "Disciplinaries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Disciplinaries_DisciplinaryId",
                table: "SearchTags");

            migrationBuilder.DropTable(
                name: "DisciplinaryTranslations");

            migrationBuilder.DropTable(
                name: "StaffPersonTranslations");

            migrationBuilder.DropTable(
                name: "Disciplinaries");

            migrationBuilder.DropTable(
                name: "StaffPersons");

            migrationBuilder.DropIndex(
                name: "IX_SearchTags_DisciplinaryId",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "DisciplinaryId",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "DynamicMenuType",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "DynamicMenuType",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "DynamicMenuType",
                table: "Articles");
        }
    }
}
