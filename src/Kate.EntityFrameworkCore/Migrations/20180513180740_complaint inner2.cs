﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class complaintinner2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InnerComplaintTranslations_AboutUs_AboutUsId",
                table: "InnerComplaintTranslations");

            migrationBuilder.DropForeignKey(
                name: "FK_InnerComplaintTranslations_InnerComplaints_InnerComplaintId",
                table: "InnerComplaintTranslations");

            migrationBuilder.DropIndex(
                name: "IX_InnerComplaintTranslations_AboutUsId",
                table: "InnerComplaintTranslations");

            migrationBuilder.DropColumn(
                name: "AboutUsId",
                table: "InnerComplaintTranslations");

            migrationBuilder.AlterColumn<int>(
                name: "InnerComplaintId",
                table: "InnerComplaintTranslations",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_InnerComplaintTranslations_InnerComplaints_InnerComplaintId",
                table: "InnerComplaintTranslations",
                column: "InnerComplaintId",
                principalTable: "InnerComplaints",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InnerComplaintTranslations_InnerComplaints_InnerComplaintId",
                table: "InnerComplaintTranslations");

            migrationBuilder.AlterColumn<int>(
                name: "InnerComplaintId",
                table: "InnerComplaintTranslations",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AboutUsId",
                table: "InnerComplaintTranslations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_InnerComplaintTranslations_AboutUsId",
                table: "InnerComplaintTranslations",
                column: "AboutUsId");

            migrationBuilder.AddForeignKey(
                name: "FK_InnerComplaintTranslations_AboutUs_AboutUsId",
                table: "InnerComplaintTranslations",
                column: "AboutUsId",
                principalTable: "AboutUs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InnerComplaintTranslations_InnerComplaints_InnerComplaintId",
                table: "InnerComplaintTranslations",
                column: "InnerComplaintId",
                principalTable: "InnerComplaints",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
