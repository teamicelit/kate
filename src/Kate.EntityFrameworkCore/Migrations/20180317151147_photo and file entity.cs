﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class photoandfileentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FileName",
                table: "GalleryFile",
                newName: "FilePath");

            migrationBuilder.RenameColumn(
                name: "MainText",
                table: "DisciplinaryTranslations",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "WordFileName",
                table: "Disciplinaries",
                newName: "WordFilePath");

            migrationBuilder.RenameColumn(
                name: "PdfFileName",
                table: "Disciplinaries",
                newName: "PdfFilePath");

            migrationBuilder.RenameColumn(
                name: "MainPhotoName",
                table: "Disciplinaries",
                newName: "MainPhotoPath");

            migrationBuilder.RenameColumn(
                name: "WordFileName",
                table: "Decisions",
                newName: "WordFilePath");

            migrationBuilder.RenameColumn(
                name: "PdfFileName",
                table: "Decisions",
                newName: "PdfFilePath");

            migrationBuilder.RenameColumn(
                name: "ComplaintPicturePhoto",
                table: "Decisions",
                newName: "FileUserReadableName");

            migrationBuilder.RenameColumn(
                name: "MainText",
                table: "CategoryTranslations",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "WordFileName",
                table: "Categories",
                newName: "WordFilePath");

            migrationBuilder.RenameColumn(
                name: "PdfFileName",
                table: "Categories",
                newName: "PdfFilePath");

            migrationBuilder.RenameColumn(
                name: "MainPhotoName",
                table: "Categories",
                newName: "MainPhotoPath");

            migrationBuilder.RenameColumn(
                name: "MainText",
                table: "ArticleTranslations",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "WordFileName",
                table: "Articles",
                newName: "WordFilePath");

            migrationBuilder.RenameColumn(
                name: "PdfFileName",
                table: "Articles",
                newName: "PdfFilePath");

            migrationBuilder.RenameColumn(
                name: "MainPhotoName",
                table: "Articles",
                newName: "MainPhotoPath");

            migrationBuilder.RenameColumn(
                name: "MainText",
                table: "AboutUsTranslations",
                newName: "Description");

            migrationBuilder.AddColumn<string>(
                name: "FileUserReadableName",
                table: "Disciplinaries",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PdfFileSize",
                table: "Disciplinaries",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "WordFileSize",
                table: "Disciplinaries",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "ComplaintPicturePhotoPath",
                table: "Decisions",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PdfFileSize",
                table: "Decisions",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "WordFileSize",
                table: "Decisions",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "PdfFileSize",
                table: "Categories",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "WordFileSize",
                table: "Categories",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "FileUserReadableName",
                table: "Articles",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "PdfFileSize",
                table: "Articles",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "WordFileSize",
                table: "Articles",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileUserReadableName",
                table: "Disciplinaries");

            migrationBuilder.DropColumn(
                name: "PdfFileSize",
                table: "Disciplinaries");

            migrationBuilder.DropColumn(
                name: "WordFileSize",
                table: "Disciplinaries");

            migrationBuilder.DropColumn(
                name: "ComplaintPicturePhotoPath",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "PdfFileSize",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "WordFileSize",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "PdfFileSize",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "WordFileSize",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "FileUserReadableName",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "PdfFileSize",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "WordFileSize",
                table: "Articles");

            migrationBuilder.RenameColumn(
                name: "FilePath",
                table: "GalleryFile",
                newName: "FileName");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "DisciplinaryTranslations",
                newName: "MainText");

            migrationBuilder.RenameColumn(
                name: "WordFilePath",
                table: "Disciplinaries",
                newName: "WordFileName");

            migrationBuilder.RenameColumn(
                name: "PdfFilePath",
                table: "Disciplinaries",
                newName: "PdfFileName");

            migrationBuilder.RenameColumn(
                name: "MainPhotoPath",
                table: "Disciplinaries",
                newName: "MainPhotoName");

            migrationBuilder.RenameColumn(
                name: "WordFilePath",
                table: "Decisions",
                newName: "WordFileName");

            migrationBuilder.RenameColumn(
                name: "PdfFilePath",
                table: "Decisions",
                newName: "PdfFileName");

            migrationBuilder.RenameColumn(
                name: "FileUserReadableName",
                table: "Decisions",
                newName: "ComplaintPicturePhoto");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "CategoryTranslations",
                newName: "MainText");

            migrationBuilder.RenameColumn(
                name: "WordFilePath",
                table: "Categories",
                newName: "WordFileName");

            migrationBuilder.RenameColumn(
                name: "PdfFilePath",
                table: "Categories",
                newName: "PdfFileName");

            migrationBuilder.RenameColumn(
                name: "MainPhotoPath",
                table: "Categories",
                newName: "MainPhotoName");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "ArticleTranslations",
                newName: "MainText");

            migrationBuilder.RenameColumn(
                name: "WordFilePath",
                table: "Articles",
                newName: "WordFileName");

            migrationBuilder.RenameColumn(
                name: "PdfFilePath",
                table: "Articles",
                newName: "PdfFileName");

            migrationBuilder.RenameColumn(
                name: "MainPhotoPath",
                table: "Articles",
                newName: "MainPhotoName");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "AboutUsTranslations",
                newName: "MainText");
        }
    }
}
