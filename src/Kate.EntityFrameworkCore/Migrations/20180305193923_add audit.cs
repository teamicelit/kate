﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class addaudit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "SearchTags",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "SearchTags",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "SearchTags",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "SearchTags",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "SearchTags",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "SearchTags",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "SearchTags",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "PhotoVideoGalleryTranslations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "PhotoVideoGalleryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "PhotoVideoGalleryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "PhotoVideoGalleryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "PhotoVideoGalleryTranslations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "PhotoVideoGalleryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "PhotoVideoGalleryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "PhotoVideoGalleries",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "PhotoVideoGalleries",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "PhotoVideoGalleries",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "PhotoVideoGalleries",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "PhotoVideoGalleries",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "PhotoVideoGalleries",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "PartnerCompaniesTranslations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "PartnerCompaniesTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "PartnerCompaniesTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "PartnerCompaniesTranslations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "PartnerCompaniesTranslations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "PartnerCompaniesTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "PartnerCompaniesTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "PartnerCompanies",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "PartnerCompanies",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "PartnerCompanies",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "PartnerCompanies",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "PartnerCompanies",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "PartnerCompanies",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Notification",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Notification",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Notification",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Notification",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Notification",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Notification",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "GalleryFile",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "GalleryFile",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "GalleryFile",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "GalleryFile",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "GalleryFile",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "GalleryFile",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "GalleryFile",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "EventTranslations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "EventTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "EventTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "EventTranslations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "EventTranslations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "EventTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "EventTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Events",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Events",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Events",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Events",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Events",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Events",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "DecisionTranslations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "DecisionTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "DecisionTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "DecisionTranslations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "DecisionTranslations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "DecisionTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "DecisionTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Decisions",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Decisions",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Decisions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Decisions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Decisions",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Decisions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "DataLanguages",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "DataLanguages",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "DataLanguages",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "DataLanguages",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "DataLanguages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "DataLanguages",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "DataLanguages",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "CategoryTranslations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "CategoryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "CategoryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "CategoryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "CategoryTranslations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "CategoryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "CategoryTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Categories",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Categories",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Categories",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Categories",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Categories",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Categories",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "ArticleTranslations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "ArticleTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "ArticleTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "ArticleTranslations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "ArticleTranslations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "ArticleTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "ArticleTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Articles",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AboutUsTranslations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AboutUsTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AboutUsTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AboutUsTranslations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AboutUsTranslations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AboutUsTranslations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AboutUsTranslations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AboutUs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AboutUs",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AboutUs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AboutUs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AboutUs",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AboutUs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "PhotoVideoGalleryTranslations");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "PhotoVideoGalleryTranslations");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "PhotoVideoGalleryTranslations");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "PhotoVideoGalleryTranslations");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "PhotoVideoGalleryTranslations");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "PhotoVideoGalleryTranslations");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "PhotoVideoGalleryTranslations");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "PartnerCompaniesTranslations");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "PartnerCompaniesTranslations");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "PartnerCompaniesTranslations");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "PartnerCompaniesTranslations");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "PartnerCompaniesTranslations");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "PartnerCompaniesTranslations");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "PartnerCompaniesTranslations");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "PartnerCompanies");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "PartnerCompanies");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "PartnerCompanies");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "PartnerCompanies");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "PartnerCompanies");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "PartnerCompanies");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "GalleryFile");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "GalleryFile");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "GalleryFile");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "GalleryFile");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "GalleryFile");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "GalleryFile");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "GalleryFile");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "EventTranslations");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "EventTranslations");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "EventTranslations");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "EventTranslations");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "EventTranslations");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "EventTranslations");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "EventTranslations");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "DecisionTranslations");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "DecisionTranslations");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "DecisionTranslations");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "DecisionTranslations");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "DecisionTranslations");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "DecisionTranslations");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "DecisionTranslations");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Decisions");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "DataLanguages");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "DataLanguages");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "DataLanguages");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "DataLanguages");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "DataLanguages");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "DataLanguages");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "DataLanguages");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "CategoryTranslations");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "CategoryTranslations");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "CategoryTranslations");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "CategoryTranslations");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "CategoryTranslations");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "CategoryTranslations");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "CategoryTranslations");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "ArticleTranslations");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "ArticleTranslations");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "ArticleTranslations");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "ArticleTranslations");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "ArticleTranslations");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "ArticleTranslations");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "ArticleTranslations");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AboutUsTranslations");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AboutUsTranslations");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AboutUsTranslations");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AboutUsTranslations");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AboutUsTranslations");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AboutUsTranslations");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AboutUsTranslations");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AboutUs");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AboutUs");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AboutUs");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AboutUs");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AboutUs");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AboutUs");
        }
    }
}
