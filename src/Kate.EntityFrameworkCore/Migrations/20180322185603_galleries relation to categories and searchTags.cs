﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class galleriesrelationtocategoriesandsearchTags : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PhotoVideoGalleryId",
                table: "SearchTags",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "PhotoVideoGalleries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_SearchTags_PhotoVideoGalleryId",
                table: "SearchTags",
                column: "PhotoVideoGalleryId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoVideoGalleries_CategoryId",
                table: "PhotoVideoGalleries",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_PhotoVideoGalleries_Categories_CategoryId",
                table: "PhotoVideoGalleries",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_PhotoVideoGalleries_PhotoVideoGalleryId",
                table: "SearchTags",
                column: "PhotoVideoGalleryId",
                principalTable: "PhotoVideoGalleries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PhotoVideoGalleries_Categories_CategoryId",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_PhotoVideoGalleries_PhotoVideoGalleryId",
                table: "SearchTags");

            migrationBuilder.DropIndex(
                name: "IX_SearchTags_PhotoVideoGalleryId",
                table: "SearchTags");

            migrationBuilder.DropIndex(
                name: "IX_PhotoVideoGalleries_CategoryId",
                table: "PhotoVideoGalleries");

            migrationBuilder.DropColumn(
                name: "PhotoVideoGalleryId",
                table: "SearchTags");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "PhotoVideoGalleries");
        }
    }
}
