﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class complaintinner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Possition",
                table: "StaffPersonTranslations",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "InnerComplaints",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InnerComplaints", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InnerComplaintTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AboutUsId = table.Column<int>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    InnerComplaintId = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LanguageId = table.Column<int>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InnerComplaintTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InnerComplaintTranslations_AboutUs_AboutUsId",
                        column: x => x.AboutUsId,
                        principalTable: "AboutUs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InnerComplaintTranslations_InnerComplaints_InnerComplaintId",
                        column: x => x.InnerComplaintId,
                        principalTable: "InnerComplaints",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InnerComplaintTranslations_DataLanguages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "DataLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InnerComplaintTranslations_AboutUsId",
                table: "InnerComplaintTranslations",
                column: "AboutUsId");

            migrationBuilder.CreateIndex(
                name: "IX_InnerComplaintTranslations_InnerComplaintId",
                table: "InnerComplaintTranslations",
                column: "InnerComplaintId");

            migrationBuilder.CreateIndex(
                name: "IX_InnerComplaintTranslations_LanguageId",
                table: "InnerComplaintTranslations",
                column: "LanguageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InnerComplaintTranslations");

            migrationBuilder.DropTable(
                name: "InnerComplaints");

            migrationBuilder.DropColumn(
                name: "Possition",
                table: "StaffPersonTranslations");
        }
    }
}
