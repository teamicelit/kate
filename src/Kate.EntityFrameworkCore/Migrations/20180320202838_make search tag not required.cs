﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class makesearchtagnotrequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Articles_ArticleId",
                table: "SearchTags");

            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Categories_CategoryId",
                table: "SearchTags");

            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Decisions_DecisionId",
                table: "SearchTags");

            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Disciplinaries_DisciplinaryId",
                table: "SearchTags");

            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Events_EventId",
                table: "SearchTags");

            migrationBuilder.AlterColumn<int>(
                name: "EventId",
                table: "SearchTags",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DisciplinaryId",
                table: "SearchTags",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DecisionId",
                table: "SearchTags",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CategoryId",
                table: "SearchTags",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ArticleId",
                table: "SearchTags",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Articles_ArticleId",
                table: "SearchTags",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Categories_CategoryId",
                table: "SearchTags",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Decisions_DecisionId",
                table: "SearchTags",
                column: "DecisionId",
                principalTable: "Decisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Disciplinaries_DisciplinaryId",
                table: "SearchTags",
                column: "DisciplinaryId",
                principalTable: "Disciplinaries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Events_EventId",
                table: "SearchTags",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Articles_ArticleId",
                table: "SearchTags");

            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Categories_CategoryId",
                table: "SearchTags");

            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Decisions_DecisionId",
                table: "SearchTags");

            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Disciplinaries_DisciplinaryId",
                table: "SearchTags");

            migrationBuilder.DropForeignKey(
                name: "FK_SearchTags_Events_EventId",
                table: "SearchTags");

            migrationBuilder.AlterColumn<int>(
                name: "EventId",
                table: "SearchTags",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DisciplinaryId",
                table: "SearchTags",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DecisionId",
                table: "SearchTags",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CategoryId",
                table: "SearchTags",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ArticleId",
                table: "SearchTags",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Articles_ArticleId",
                table: "SearchTags",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Categories_CategoryId",
                table: "SearchTags",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Decisions_DecisionId",
                table: "SearchTags",
                column: "DecisionId",
                principalTable: "Decisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Disciplinaries_DisciplinaryId",
                table: "SearchTags",
                column: "DisciplinaryId",
                principalTable: "Disciplinaries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SearchTags_Events_EventId",
                table: "SearchTags",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
