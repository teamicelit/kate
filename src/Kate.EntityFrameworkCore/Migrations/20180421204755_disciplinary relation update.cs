﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kate.Migrations
{
    public partial class disciplinaryrelationupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShowOnMainPageBox",
                table: "Disciplinaries");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Disciplinaries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Disciplinaries_CategoryId",
                table: "Disciplinaries",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Disciplinaries_Categories_CategoryId",
                table: "Disciplinaries",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Disciplinaries_Categories_CategoryId",
                table: "Disciplinaries");

            migrationBuilder.DropIndex(
                name: "IX_Disciplinaries_CategoryId",
                table: "Disciplinaries");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Disciplinaries");

            migrationBuilder.AddColumn<bool>(
                name: "ShowOnMainPageBox",
                table: "Disciplinaries",
                nullable: false,
                defaultValue: false);
        }
    }
}
