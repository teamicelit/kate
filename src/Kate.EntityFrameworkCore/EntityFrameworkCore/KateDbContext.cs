﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Kate.Authorization.Roles;
using Kate.Authorization.Users;
using Kate.MultiTenancy;
using Kate.EntityFrameworkCore.Entities;
using Microsoft.Extensions.Configuration;

namespace Kate.EntityFrameworkCore
{
    public class KateDbContext : AbpZeroDbContext<Tenant, Role, User, KateDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<PartnerCompany> PartnerCompanies { get; set; }
        
        public DbSet<StaffPerson> StaffPersons { get; set; }

        public DbSet<StaffPersonTranslation> StaffPersonTranslations { get; set; }

        public DbSet<Disciplinary> Disciplinaries { get; set; }

        public DbSet<DisciplinaryTranslation> DisciplinaryTranslations { get; set; }

        public DbSet<PartnerCompaniesTranslation> PartnerCompaniesTranslations { get; set; }

        public DbSet<Article> Articles { get; set; }

        public DbSet<ArticleTranslation> ArticleTranslations { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<CategoryTranslation> CategoryTranslations { get; set; }

        public DbSet<Event> Events { get; set; }

        public DbSet<EventTranslation> EventTranslations { get; set; }

        public DbSet<Language> DataLanguages { get; set; }

        public DbSet<Notification> MailNotifications { get; set; }

        public DbSet<AboutUs> AboutUs { get; set; }

        public DbSet<AboutUsTranslation> AboutUsTranslations { get; set; }

        public DbSet<InnerComplaint> InnerComplaints { get; set; }

        public DbSet<InnerComplaintTranslation> InnerComplaintTranslations { get; set; }

        public DbSet<PhotoVideoGallery> PhotoVideoGalleries { get; set; }

        public DbSet<PhotoVideoGalleryTranslation> PhotoVideoGalleryTranslations { get; set; }

        public DbSet<SearchTag> SearchTags { get; set; }

        public DbSet<Notification> Letters { get; set; }

        public DbSet<Decision> Decisions { get; set; }

        public DbSet<DecisionTranslation> DecisionTranslations { get; set; }

        public DbSet<DocumentSignature> DocumentSignatures { get; set; }

        public KateDbContext(DbContextOptions<KateDbContext> options)
            : base(options)
        {
            GetConfigurationBuilder();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            string connectionString = Configuration.GetConnectionString("Default");
            builder.UseNpgsql(connectionString);
            // builder.UseNpgsql("Database=KateDbFinal; Server=199.247.17.188; Port=5432; User id=postgres; Password=123456");
            //   builder.UseNpgsql("Database=KateDbFinal; Server=45.77.66.238; Port=5432; User id=postgres; Password=7AosWBL256AePDnyXXwC");
            //  builder.UseNpgsql("Database=KateDbbb; Server=80.241.241.234; Port=5432; User id=postgres; Password=1z2x3c4v");

            base.OnConfiguring(builder);
        }

        public IConfiguration Configuration { get; set; }
        private void GetConfigurationBuilder()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
        }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Article>()
                .HasOne(a => a.Category)
                .WithMany(a => a.Articles)
                .HasForeignKey(a => a.CategoryId)
                .IsRequired();

            builder.Entity<Category>()
                .HasMany(s => s.SchemeItems)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<ArticleTranslation>()
                .HasOne(a => a.Language)
                .WithMany(a => a.ArticleTranslations)
                .HasForeignKey(a => a.LanguageId)
                .IsRequired();

            builder.Entity<ArticleTranslation>()
                .HasOne(a => a.Article)
                .WithMany(a => a.Translations)
                .HasForeignKey(a => a.ArticleId)
                .IsRequired();

            builder.Entity<StaffPerson>()
                .HasOne(a => a.Category)
                .WithMany(a => a.StaffPersons)
                .HasForeignKey(a => a.CategoryId)
                .IsRequired();

            builder.Entity<StaffPersonTranslation>()
                .HasOne(a => a.Language)
                .WithMany(a => a.StaffPersonTranslations)
                .HasForeignKey(a => a.LanguageId)
                .IsRequired();

            builder.Entity<StaffPersonTranslation>()
                .HasOne(a => a.StaffPerson)
                .WithMany(a => a.Translations)
                .HasForeignKey(a => a.StaffPersonId)
                .IsRequired();

            builder.Entity<Disciplinary>()
                .HasOne(a => a.Category)
                .WithMany(a => a.Disciplinaries)
                .HasForeignKey(a => a.CategoryId)
                .IsRequired();

            builder.Entity<DisciplinaryTranslation>()
                .HasOne(a => a.Language)
                .WithMany(a => a.DisciplinaryTranslations)
                .HasForeignKey(a => a.LanguageId)
                .IsRequired();

            builder.Entity<DisciplinaryTranslation>()
                .HasOne(a => a.Disciplinary)
                .WithMany(a => a.Translations)
                .HasForeignKey(a => a.DisciplinaryId)
                .IsRequired();

            builder.Entity<Decision>()
                .HasOne(a => a.Category)
                .WithMany(a => a.Decisions)
                .HasForeignKey(a => a.CategoryId)
                .IsRequired();

            builder.Entity<DecisionTranslation>()
                .HasOne(a => a.Language)
                .WithMany(a => a.DecisionTranslations)
                .HasForeignKey(a => a.LanguageId)
                .IsRequired();

            builder.Entity<DecisionTranslation>()
                .HasOne(a => a.Decision)
                .WithMany(a => a.Translations)
                .HasForeignKey(a => a.DecisionId)
                .IsRequired();

            builder.Entity<Category>()
                .HasOne(a => a.ParentCategory)
                .WithMany(a => a.ChildCategories)
                .HasForeignKey(a => a.ParentCategoryId);

            builder.Entity<CategoryTranslation>()
                .HasOne(a => a.Language)
                .WithMany(a => a.CategoryTranslations)
                .HasForeignKey(a => a.LanguageId)
                .IsRequired();

            builder.Entity<CategoryTranslation>()
                .HasOne(a => a.Category)
                .WithMany(a => a.Translations)
                .HasForeignKey(a => a.CategoryId)
                .IsRequired();

            builder.Entity<EventTranslation>()
                .HasOne(a => a.Language)
                .WithMany(a => a.EventTranslations)
                .HasForeignKey(a => a.LanguageId)
                .IsRequired();

            builder.Entity<EventTranslation>()
                .HasOne(a => a.Event)
                .WithMany(a => a.Translations)
                .HasForeignKey(a => a.EventId)
                .IsRequired();

            builder.Entity<SearchTag>()
                .HasOne(a => a.Category)
                .WithMany(a => a.SearchTags)
                .HasForeignKey(a => a.CategoryId);

            builder.Entity<SearchTag>()
                .HasOne(a => a.Article)
                .WithMany(a => a.SearchTags)
                .HasForeignKey(a => a.ArticleId);

            builder.Entity<SearchTag>()
                .HasOne(a => a.Decision)
                .WithMany(a => a.SearchTags)
                .HasForeignKey(a => a.DecisionId);

            builder.Entity<SearchTag>()
                .HasOne(a => a.Event)
                .WithMany(a => a.SearchTags)
                .HasForeignKey(a => a.EventId);

            builder.Entity<SearchTag>()
                .HasOne(a => a.Disciplinary)
                .WithMany(a => a.SearchTags)
                .HasForeignKey(a => a.DisciplinaryId);

            builder.Entity<SearchTag>()
                .HasOne(a => a.PhotoVideoGallery)
                .WithMany(a => a.SearchTags)
                .HasForeignKey(a => a.PhotoVideoGalleryId);

            builder.Entity<GalleryFile>()
                .HasOne(a => a.Gallery)
                .WithMany(a => a.GalleryFiles)
                .HasForeignKey(a => a.GalleryId)
                .IsRequired();

            builder.Entity<PhotoVideoGalleryTranslation>()
                .HasOne(a => a.Language)
                .WithMany(a => a.PhotoVideoGalleryTranslations)
                .HasForeignKey(a => a.LanguageId)
                .IsRequired();

            builder.Entity<PhotoVideoGalleryTranslation>()
                .HasOne(a => a.PhotoVideoGallery)
                .WithMany(a => a.Translations)
                .HasForeignKey(a => a.PhotoVideoGalleryId)
                .IsRequired();

            builder.Entity<PhotoVideoGallery>()
                .HasOne(a => a.Category)
                .WithMany(a => a.Galleries)
                .HasForeignKey(a => a.CategoryId)
                .IsRequired();

            builder.Entity<PartnerCompaniesTranslation>()
                .HasOne(a => a.Language)
                .WithMany(a => a.PartnerCompanyTranslations)
                .HasForeignKey(a => a.LanguageId)
                .IsRequired();

            builder.Entity<PartnerCompaniesTranslation>()
                .HasOne(a => a.PartnerCompany)
                .WithMany(a => a.Translations)
                .HasForeignKey(a => a.PartnerCompanyId)
                .IsRequired();

            base.OnModelCreating(builder);

            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                foreach (var property in entityType.GetProperties())
                {
                    // max char length value in sqlserver
                    if (property.GetMaxLength() == 67108864)
                        // max char length value in postgresql
                        property.SetMaxLength(10485760);
                }
            }
        }
    }
}
