using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Kate.EntityFrameworkCore
{
    public static class KateDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<KateDbContext> builder, string connectionString)
        {
            //builder.UseSqlServer(connectionString);
            builder.UseNpgsql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<KateDbContext> builder, DbConnection connection)
        {
            //builder.UseSqlServer(connection);
            builder.UseNpgsql(connection);
        }
    }
}
