﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Kate.Configuration;
using Kate.Web;

namespace Kate.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class KateDbContextFactory : IDesignTimeDbContextFactory<KateDbContext>
    {
        public KateDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<KateDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            KateDbContextConfigurer.Configure(builder, configuration.GetConnectionString(KateConsts.ConnectionStringName));

            return new KateDbContext(builder.Options);
        }
    }
}
