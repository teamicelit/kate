﻿using Kate.EntityFrameworkCore.Entities;
using Kate.Localization;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kate.EntityFrameworkCore.Seed.Host
{
    public class DefaultDataCreator
    {
        private readonly KateDbContext _context;

        public static List<Language> InitialLanguages => GetInitialLanguages();
        public static List<AboutUsTranslation> InitialAboutUs => GetInitialAboutUsTransations();
        public static List<InnerComplaintTranslation> InitialInnerComplaint => GetInnerComplaintTransations();

        public DefaultDataCreator(KateDbContext context)
        {
            _context = context;
        }

        private static List<Language> GetInitialLanguages()
        {
            return new List<Language>
            {
                new Language("English","en"),
                new Language("ქართული", "ka"),
                new Language("Français", "fr"),
                new Language("русский", "de")
            };
        }

        private static List<AboutUsTranslation> GetInitialAboutUsTransations()
        {
            return new List<AboutUsTranslation>
            {
                new AboutUsTranslation{ Name= "ქართული", Description="ჩვენს შესახებ აღწერა", LanguageId = (int)LanguageEnum.Georgian},
                new AboutUsTranslation{ Name= "English", Description="About us description", LanguageId = (int)LanguageEnum.English},
                new AboutUsTranslation{ Name= "français", Description="à propos de nous description", LanguageId = (int)LanguageEnum.French},
                new AboutUsTranslation{ Name= "русский", Description="о нас описание", LanguageId = (int)LanguageEnum.German}
            };
        }

        private static List<InnerComplaintTranslation> GetInnerComplaintTransations()
        {
            return new List<InnerComplaintTranslation>
            {
                new InnerComplaintTranslation{ Name= "ქართული", Description="საჩივრის ფორმა ტექსტი", LanguageId = (int)LanguageEnum.Georgian},
                new InnerComplaintTranslation{ Name= "English", Description="Complaint description", LanguageId = (int)LanguageEnum.English},
                new InnerComplaintTranslation{ Name= "français", Description="nous description", LanguageId = (int)LanguageEnum.French},
                new InnerComplaintTranslation{ Name= "русский", Description="описание", LanguageId = (int)LanguageEnum.German}
            };
        }

        public void Create()
        {
            CreateLanguages();
            AddAboutUsIfNotExists();
            AddInnerComplaintIfNotExists();
        }

        private void CreateLanguages()
        {
            foreach (var language in InitialLanguages)
            {
                AddLanguageIfNotExists(language);
            }
        }

        private void AddLanguageIfNotExists(Language language)
        {
            if (_context.DataLanguages.IgnoreQueryFilters().Any(l => l.Name == language.Name))
                return;

            _context.DataLanguages.Add(language);
            _context.SaveChanges();
        }

        public void AddAboutUsIfNotExists()
        {
            if (_context.AboutUs.Any())
                return;

            _context.AboutUs.Add(new AboutUs { Translations = GetInitialAboutUsTransations() });
            _context.SaveChanges();
        }

        public void AddInnerComplaintIfNotExists()
        {
            if (_context.InnerComplaints.Any())
                return;

            _context.InnerComplaints.Add(new InnerComplaint { Translations = GetInnerComplaintTransations() });
            _context.SaveChanges();
        }
    }
}
