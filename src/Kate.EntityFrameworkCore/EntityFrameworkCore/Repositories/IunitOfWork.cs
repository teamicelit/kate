﻿using System;
using Kate.EntityFrameworkCore.Entities;
using Abp.Domain.Repositories;

namespace Kate.EntityFrameworkCore.Repositories
{
    public interface ICustomUnitOfWork : IDisposable
    {
        DocumentSignatureRepository DocumentSignatureRepository { get; }
        ArticleRepo ArticleRepo { get; }
        ArticleTranslationRepo ArticleTranslationRepo { get; }
        CategoryRepo CategoryRepo { get; }
        CategoryTranslationRepo CategoryTranslationRepo { get; }
        EventRepo EventRepo { get; }
        EventTranslationRepo EventTranslationRepo { get; }
        LanguageRepo LanguageRepo { get; }
        NotificationRepo NotificationRepo { get; }
        PartnerCompanyRepo PartnerCompanyRepo { get; }
        PartnerCompaniesTranslationRepo PartnerCompaniesTranslationRepo { get; }
        DecisionRepo DecisionRepo { get; }
        DecisionTranslationRepo DecisionTranslationRepo { get; }
        DisciplinaryRepo DisciplinaryRepo { get; }
        DisciplinaryTranslationRepo DisciplinaryTranslationRepo { get; }
        StaffPersonRepo StaffPersonRepo { get; }
        StaffPersonTranslationRepo StaffPersonTranslationRepo { get; }
        AboutUsRepo AboutUsRepo { get; }
        AboutUsTranslationRepo AboutUsTranslationRepo { get; }
        InnerComplaintRepo InnerComplaintRepo { get; }
        InnerComplaintTranslationRepo InnerComplaintTranslationRepo { get; }
        GalleryFileRepo GalleryFileRepo { get; }
        PhotoVideoGalleryRepo PhotoVideoGalleryRepo { get; }
        PhotoVideoGalleryTranslationRepo PhotoVideoGalleryTranslationRepo { get; }
        SearchTagRepo SearchTagRepo { get; }

        void Save();
    }
}
