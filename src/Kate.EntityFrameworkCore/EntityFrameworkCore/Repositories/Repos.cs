﻿using Kate.EntityFrameworkCore.Entities;

namespace Kate.EntityFrameworkCore.Repositories
{
    public class ArticleRepo : GenericRepository<Article>
    {
        public ArticleRepo(KateDbContext context) : base(context)
        {
        }
    }
    
    public class ArticleTranslationRepo : GenericRepository<ArticleTranslation>
    {
        public ArticleTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class CategoryRepo : GenericRepository<Category>
    {
        public CategoryRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class CategoryTranslationRepo : GenericRepository<CategoryTranslation>
    {
        public CategoryTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class EventRepo : GenericRepository<Event>
    {
        public EventRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class EventTranslationRepo : GenericRepository<EventTranslation>
    {
        public EventTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class LanguageRepo : GenericRepository<Language>
    {
        public LanguageRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class AboutUsRepo : GenericRepository<AboutUs>
    {
        public AboutUsRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class AboutUsTranslationRepo : GenericRepository<AboutUsTranslation>
    {
        public AboutUsTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }
    
    public class InnerComplaintRepo : GenericRepository<InnerComplaint>
    {
        public InnerComplaintRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class InnerComplaintTranslationRepo : GenericRepository<InnerComplaintTranslation>
    {
        public InnerComplaintTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class DecisionRepo : GenericRepository<Decision>
    {
        public DecisionRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class DecisionTranslationRepo : GenericRepository<DecisionTranslation>
    {
        public DecisionTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }
    public class DisciplinaryRepo : GenericRepository<Disciplinary>
    {
        public DisciplinaryRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class DisciplinaryTranslationRepo : GenericRepository<DisciplinaryTranslation>
    {
        public DisciplinaryTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class StaffPersonRepo : GenericRepository<StaffPerson>
    {
        public StaffPersonRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class StaffPersonTranslationRepo : GenericRepository<StaffPersonTranslation>
    {
        public StaffPersonTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class GalleryFileRepo : GenericRepository<GalleryFile>
    {
        public GalleryFileRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class PartnerCompanyRepo : GenericRepository<PartnerCompany>
    {
        public PartnerCompanyRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class PartnerCompaniesTranslationRepo : GenericRepository<PartnerCompaniesTranslation>
    {
        public PartnerCompaniesTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class PhotoVideoGalleryRepo : GenericRepository<PhotoVideoGallery>
    {
        public PhotoVideoGalleryRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class PhotoVideoGalleryTranslationRepo : GenericRepository<PhotoVideoGalleryTranslation>
    {
        public PhotoVideoGalleryTranslationRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class SearchTagRepo : GenericRepository<SearchTag>
    {
        public SearchTagRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class NotificationRepo : GenericRepository<Notification>
    {
        public NotificationRepo(KateDbContext context) : base(context)
        {
        }
    }

    public class DocumentSignatureRepository : GenericRepository<DocumentSignature>
    {
        public DocumentSignatureRepository(KateDbContext context) : base(context)
        {
        }
    }
}
