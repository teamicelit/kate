﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Kate.EntityFrameworkCore.Repositories
{
    public class CustomUnitOfWork : ICustomUnitOfWork
    {
        private readonly KateDbContext _context = new KateDbContext(new DbContextOptions<KateDbContext>());
    
        private ArticleRepo _articleRepo;
        private ArticleTranslationRepo _articleTranslationRepo;
        private CategoryRepo _categoryRepo;
        private CategoryTranslationRepo _categoryTranslationRepo;
        private EventRepo _eventRepo;
        private EventTranslationRepo _eventTranslationRepo;
        private LanguageRepo _languageRepo;
        private NotificationRepo _notificationRepo;
        private PartnerCompanyRepo _partnerRepo;
        private PartnerCompaniesTranslationRepo _partnerCompaniesTranslationRepo;
        private DecisionRepo _decisionRepo;
        private DecisionTranslationRepo _decisionTranslationRepo;
        private DisciplinaryRepo _disciplinaryRepo;
        private DisciplinaryTranslationRepo _disciplinaryTranslationRepo;
        private StaffPersonRepo _staffPersonRepo;
        private StaffPersonTranslationRepo _staffPersonTranslationRepo;
        private AboutUsRepo _aboutUsRepo;
        private AboutUsTranslationRepo _aboutUsTranslationRepo;
        private InnerComplaintRepo _innerComplaintRepo;
        private InnerComplaintTranslationRepo _innerComplaintTranslationRepo;
        private GalleryFileRepo _galleryFileRepo;
        private PhotoVideoGalleryRepo _photoVideoGalleryRepo;
        private PhotoVideoGalleryTranslationRepo _photoVideoGalleryTranslationRepo;
        private SearchTagRepo _searchTagRepo;
        private DocumentSignatureRepository _documentSignatureRepository;
        
        public PartnerCompaniesTranslationRepo PartnerCompaniesTranslationRepo
        {
            get
            {
                if (this._partnerCompaniesTranslationRepo == null)
                    this._partnerCompaniesTranslationRepo = new PartnerCompaniesTranslationRepo(_context);

                return _partnerCompaniesTranslationRepo;
            }
        }

        public DecisionRepo DecisionRepo
        {
            get
            {
                if (this._decisionRepo == null)
                    this._decisionRepo = new DecisionRepo(_context);

                return _decisionRepo;
            }
        }
        
        public DecisionTranslationRepo DecisionTranslationRepo
        {
            get
            {
                if (this._decisionTranslationRepo == null)
                    this._decisionTranslationRepo = new DecisionTranslationRepo(_context);

                return _decisionTranslationRepo;
            }
        }

        public DisciplinaryRepo DisciplinaryRepo
        {
            get
            {
                if (this._disciplinaryRepo == null)
                    this._disciplinaryRepo = new DisciplinaryRepo(_context);

                return _disciplinaryRepo;
            }
        }

        public DisciplinaryTranslationRepo DisciplinaryTranslationRepo
        {
            get
            {
                if (this._disciplinaryTranslationRepo == null)
                    this._disciplinaryTranslationRepo = new DisciplinaryTranslationRepo(_context);

                return _disciplinaryTranslationRepo;
            }
        }

        public StaffPersonRepo StaffPersonRepo
        {
            get
            {
                if (this._staffPersonRepo == null)
                    this._staffPersonRepo = new StaffPersonRepo(_context);

                return _staffPersonRepo;
            }
        }

        public StaffPersonTranslationRepo StaffPersonTranslationRepo
        {
            get
            {
                if (this._staffPersonTranslationRepo == null)
                    this._staffPersonTranslationRepo = new StaffPersonTranslationRepo(_context);

                return _staffPersonTranslationRepo;
            }
        }

        public AboutUsRepo AboutUsRepo
        {
            get
            {
                if (this._aboutUsRepo == null)
                    this._aboutUsRepo = new AboutUsRepo(_context);

                return _aboutUsRepo;
            }
        }

        public AboutUsTranslationRepo AboutUsTranslationRepo
        {
            get
            {
                if (this._aboutUsTranslationRepo == null)
                    this._aboutUsTranslationRepo = new AboutUsTranslationRepo(_context);

                return _aboutUsTranslationRepo;
            }
        }
        public InnerComplaintRepo InnerComplaintRepo
        {
            get
            {
                if (this._innerComplaintRepo == null)
                    this._innerComplaintRepo = new InnerComplaintRepo(_context);

                return _innerComplaintRepo;
            }
        }

        public InnerComplaintTranslationRepo InnerComplaintTranslationRepo
        {
            get
            {
                if (this._innerComplaintTranslationRepo == null)
                    this._innerComplaintTranslationRepo = new InnerComplaintTranslationRepo(_context);

                return _innerComplaintTranslationRepo;
            }
        }

        public GalleryFileRepo GalleryFileRepo
        {
            get
            {
                if (this._galleryFileRepo == null)
                    this._galleryFileRepo = new GalleryFileRepo(_context);

                return _galleryFileRepo;
            }
        }

        public PhotoVideoGalleryRepo PhotoVideoGalleryRepo
        {
            get
            {
                if (this._photoVideoGalleryRepo == null)
                    this._photoVideoGalleryRepo = new PhotoVideoGalleryRepo(_context);

                return _photoVideoGalleryRepo;
            }
        }

        public PhotoVideoGalleryTranslationRepo PhotoVideoGalleryTranslationRepo
        {
            get
            {
                if (this._photoVideoGalleryTranslationRepo == null)
                    this._photoVideoGalleryTranslationRepo = new PhotoVideoGalleryTranslationRepo(_context);

                return _photoVideoGalleryTranslationRepo;
            }
        }

        public SearchTagRepo SearchTagRepo
        {
            get
            {
                if (this._searchTagRepo == null)
                    this._searchTagRepo = new SearchTagRepo(_context);

                return _searchTagRepo;
            }
        }

        public PartnerCompanyRepo PartnerCompanyRepo
        {
            get
            {
                if (this._partnerRepo == null)
                    this._partnerRepo = new PartnerCompanyRepo(_context);

                return _partnerRepo;
            }
        }

        public DocumentSignatureRepository DocumentSignatureRepository
        {
            get
            {
                if (this._documentSignatureRepository == null)
                    this._documentSignatureRepository = new DocumentSignatureRepository(_context);

                return _documentSignatureRepository;
            }
        }

        public ArticleRepo ArticleRepo
        {
            get
            {
                if (this._articleRepo == null)
                    this._articleRepo = new ArticleRepo(_context);

                return _articleRepo;
            }
        }

        public ArticleTranslationRepo ArticleTranslationRepo
        {
            get
            {
                if (this._articleTranslationRepo == null)
                    this._articleTranslationRepo = new ArticleTranslationRepo(_context);

                return _articleTranslationRepo;
            }
        }

        public CategoryRepo CategoryRepo
        {
            get
            {
                if (this._categoryRepo == null)
                    this._categoryRepo = new CategoryRepo(_context);

                return _categoryRepo;
            }
        }

        public CategoryTranslationRepo CategoryTranslationRepo
        {
            get
            {
                if (this._categoryTranslationRepo == null)
                    this._categoryTranslationRepo = new CategoryTranslationRepo(_context);

                return _categoryTranslationRepo;
            }
        }

        public EventRepo EventRepo
        {
            get
            {
                if (this._eventRepo == null)
                    this._eventRepo = new EventRepo(_context);

                return _eventRepo;
            }
        }

        public EventTranslationRepo EventTranslationRepo
        {
            get
            {
                if (this._eventTranslationRepo == null)
                    this._eventTranslationRepo = new EventTranslationRepo(_context);

                return _eventTranslationRepo;
            }
        }

        public LanguageRepo LanguageRepo
        {
            get
            {
                if (this._languageRepo == null)
                    this._languageRepo = new LanguageRepo(_context);

                return _languageRepo;
            }
        }

        public NotificationRepo NotificationRepo
        {
            get
            {
                if (this._notificationRepo == null)
                    this._notificationRepo = new NotificationRepo(_context);

                return _notificationRepo;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
