﻿using System;
using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class Decision : BaseEntity<DecisionTranslation>
    {
        public Decision()
        {
            SearchTags = new HashSet<SearchTag>();
            Translations = new HashSet<DecisionTranslation>();
        }
        
        public DateTime DocumentCameInAction { get; set; }
        
        public int SortIndex { get; set; }

        public long PdfFileSize { get; set; }
        public long WordFileSize { get; set; }

        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }        
        public string WordFilePath { get; set; }
        public string ComplaintPicturePhotoPath { get; set; }
        public string LinkToFile { get; set; }
        public string VideoEmbed { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPage { get; set; }
        public bool ShowOnMainPageBox { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public ICollection<SearchTag> SearchTags { get; set; }
    }
}
