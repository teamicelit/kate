﻿using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class InnerComplaint : BaseEntity<InnerComplaintTranslation>
    {
        public InnerComplaint()
        {
            Translations = new HashSet<InnerComplaintTranslation>();
        }
    }
}
