﻿using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class AboutUs : BaseEntity<AboutUsTranslation>
    {
        public AboutUs()
        {
            Translations = new HashSet<AboutUsTranslation>();
        }
    }
}
