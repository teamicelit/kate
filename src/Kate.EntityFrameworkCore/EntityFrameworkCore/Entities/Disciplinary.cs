﻿using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class Disciplinary : BaseEntity<DisciplinaryTranslation>
    {
        public Disciplinary()
        {
            SearchTags = new HashSet<SearchTag>();
            Translations = new HashSet<DisciplinaryTranslation>();
        }
                
        public string FileUserReadableName { get; set; }

        public long PdfFileSize { get; set; }
        public string PdfFilePath { get; set; }

        public long WordFileSize { get; set; }
        public string WordFilePath { get; set; }
        
        public string MainPhotoPath { get; set; }

        public int SortIndex { get; set; }
        
        public string VideoEmbed { get; set; }

        public bool ShowOnSite { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public ICollection<SearchTag> SearchTags { get; set; }
    }
}
