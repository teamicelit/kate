﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Kate.EntityFrameworkCore.Entities
{
    public class DecisionTranslation : FullAuditedEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public int DecisionId { get; set; }
        public Decision Decision { get; set; }
    }
}
