﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Kate.EntityFrameworkCore.Entities
{
    public class AboutUsTranslation : FullAuditedEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public int AboutUsId { get; set; }
        public AboutUs AboutUs { get; set; }
    }
}