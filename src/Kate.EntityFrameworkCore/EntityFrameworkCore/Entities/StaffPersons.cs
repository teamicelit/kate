﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class StaffPerson : BaseEntity<StaffPersonTranslation>
    {
        public StaffPerson()
        {
            Translations = new HashSet<StaffPersonTranslation>();
        }
        
        public bool ShowOnSite { get; set; }

        public int SortIndex { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }
        public string Mail { get; set; }
        public string PhoneNumber { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}