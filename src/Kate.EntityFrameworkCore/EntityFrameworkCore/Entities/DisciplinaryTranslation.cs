﻿using Abp.Domain.Entities.Auditing;

namespace Kate.EntityFrameworkCore.Entities
{
    public class DisciplinaryTranslation : FullAuditedEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public int DisciplinaryId { get; set; }
        public Disciplinary Disciplinary { get; set; }
    }
}
