﻿using Abp.Domain.Entities.Auditing;

namespace Kate.EntityFrameworkCore.Entities
{
    public class PartnerCompaniesTranslation : FullAuditedEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public int PartnerCompanyId { get; set; }
        public PartnerCompany PartnerCompany { get; set; }
    }
}
