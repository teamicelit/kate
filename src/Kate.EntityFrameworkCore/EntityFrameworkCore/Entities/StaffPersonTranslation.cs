﻿using Abp.Domain.Entities.Auditing;

namespace Kate.EntityFrameworkCore.Entities
{
    public class StaffPersonTranslation : FullAuditedEntity
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string HeaderText { get; set; }

        public string Possition { get; set; }

        public string ResumeDescription { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public int StaffPersonId { get; set; }
        public StaffPerson StaffPerson { get; set; }
    }
}
