﻿using Kate.EntityFrameworkCore.Entities;
using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class Category : BaseEntity<CategoryTranslation>
    {
        public Category()
        {
            Translations = new HashSet<CategoryTranslation>();
            ChildCategories = new HashSet<Category>();
            Articles = new HashSet<Article>();
            Decisions = new HashSet<Decision>();
            StaffPersons = new HashSet<StaffPerson>();
            SearchTags = new HashSet<SearchTag>();
            Galleries = new HashSet<PhotoVideoGallery>();
            SchemeItems = new List<SchemeItem>();
        }

        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }
        public bool DontShowOnResponsive { get; set; }
        public bool ShowInSideNavigation { get; set; }
        public bool Test { get; set; }

        public long PdfFileSize { get; set; }
        public long WordFileSize { get; set; }

        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }
        public string SchemeSvg { get; set; }

        public int SortIndex { get; set; }

        public CategoryType CategoryType { get; set; }

        public int? ParentCategoryId { get; set; }
        public Category ParentCategory { get; set; }

        public ICollection<Article> Articles { get; set; }
        public ICollection<Decision> Decisions { get; set; }
        public ICollection<Disciplinary> Disciplinaries { get; set; }
        public ICollection<Category> ChildCategories { get; set; }
        public ICollection<SearchTag> SearchTags { get; set; }
        public ICollection<StaffPerson> StaffPersons { get; set; }
        public ICollection<PhotoVideoGallery> Galleries { get; set; }
        public ICollection<SchemeItem> SchemeItems { get; set; }
    }

    public enum CategoryType
    {
        Basic = 1,
        Home = 2,
        Aparat = 4,
        Decisions = 5,
        PhotoGallery = 6,
        News = 7,
        Disciplinary
    }

    public enum LanguageCode
    {
        Ge = 1,
        Ru = 2,
        En = 3
    }
}
