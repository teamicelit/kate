﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Kate.EntityFrameworkCore.Entities
{
    public class ArticleTranslation : FullAuditedEntity
    {        
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public int ArticleId { get; set; }
        public Article Article { get; set; }
    }
}