﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Kate.EntityFrameworkCore.Entities
{
    public class GalleryFile : FullAuditedEntity
    {
        public string FilePath { get; set; }

        public string Embed { get; set; }

        public FileType Type { get; set; }

        public int GalleryId { get; set; }
        public PhotoVideoGallery Gallery { get; set; }
    }

    public enum FileType
    {
        Photo = 1,
        Video = 2
    }
}
