﻿using System;
using Abp.Domain.Entities.Auditing;

namespace Kate.EntityFrameworkCore.Entities
{
    public class DocumentSignature
    {
        public Guid Id { get; set; }

        public string FilePath { get; set; }

        public bool IsProcessed { get; set; }

        public DocumentSignature()
        {
            Id = Guid.NewGuid();
        }
    }
}