﻿using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class PartnerCompany : BaseEntity<PartnerCompaniesTranslation>
    {
        public PartnerCompany()
        {
            Translations = new HashSet<PartnerCompaniesTranslation>();
        }
        
        public bool ShowOnSite { get; set; }

        public bool ShowOnMainPageBox { get; set; }
        
        public string PartnerLogo { get; set; }

        public string Link { get; set; }
    }
}