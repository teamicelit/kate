﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;

namespace Kate.EntityFrameworkCore.Entities
{
    public class Notification : FullAuditedEntity
    {
        public string Subject { get; set; }

        public string Text { get; set; }

        public string Mail { get; set; }

        public DateTime SendDate { get; set; }
    }
}
