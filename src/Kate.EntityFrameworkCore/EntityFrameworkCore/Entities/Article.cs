﻿using Kate.EntityFrameworkCore.Entities;
using System;
using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class Article : BaseEntity<ArticleTranslation>
    {
        public Article()
        {
            Translations = new HashSet<ArticleTranslation>();
            SearchTags = new HashSet<SearchTag>();
        }
                
        public DateTime CreateDate { get; set; }
        
        public int SortIndex { get; set; }

        public long WordFileSize { get; set; }
        public long PdfFileSize { get; set; }

        public string FileUserReadableName { get; set; }
        public string PdfFilePath { get; set; }
        public string WordFilePath { get; set; }
        public string MainPhotoPath { get; set; }
        public string VideoEmbed { get; set; }

        public bool ShowOnSite { get; set; }
        public bool ShowInMainSlider { get; set; }
        public bool IsNews { get; set; }
        public bool ShowOnMainPageBox { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
        
        public ICollection<SearchTag> SearchTags { get; set; }
    }
}