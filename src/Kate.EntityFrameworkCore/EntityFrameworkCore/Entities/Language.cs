﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class Language : FullAuditedEntity
    {
        public Language()
        {
            CategoryTranslations=new HashSet<CategoryTranslation>();
            ArticleTranslations =new HashSet<ArticleTranslation>();
            EventTranslations =new HashSet<EventTranslation>();
            PartnerCompanyTranslations = new HashSet<PartnerCompaniesTranslation>();
            DecisionTranslations = new HashSet<DecisionTranslation>();
            AboutUsTranslations = new HashSet<AboutUsTranslation>();
            PhotoVideoGalleryTranslations = new HashSet<PhotoVideoGalleryTranslation>();
            StaffPersonTranslations = new HashSet<StaffPersonTranslation>();
            DisciplinaryTranslations = new HashSet<DisciplinaryTranslation>();
        }

        public Language(string name, string shortName)
        {
            Name = name;
            ShortName = shortName;
        }
        
        public string Name { get; set; }

        public string ShortName { get; set; }

        public ICollection<CategoryTranslation> CategoryTranslations { get; set; }
        public ICollection<ArticleTranslation> ArticleTranslations { get; set; }
        public ICollection<EventTranslation> EventTranslations { get; set; }
        public ICollection<PartnerCompaniesTranslation> PartnerCompanyTranslations { get; set; }
        public ICollection<DecisionTranslation> DecisionTranslations { get; set; }
        public ICollection<AboutUsTranslation> AboutUsTranslations { get; set; }
        public ICollection<PhotoVideoGalleryTranslation> PhotoVideoGalleryTranslations { get; set; }
        public ICollection<StaffPersonTranslation> StaffPersonTranslations { get; set; }
        public ICollection<DisciplinaryTranslation> DisciplinaryTranslations { get; set; }
    }
}
