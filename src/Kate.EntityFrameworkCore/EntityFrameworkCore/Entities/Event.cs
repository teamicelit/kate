﻿using System;
using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class Event : BaseEntity<EventTranslation>
    {
        public Event()
        {
            Translations = new HashSet<EventTranslation>();
            SearchTags = new HashSet<SearchTag>();
        }

        public DateTime EventDate { get; set; }

        public bool ShowOnSite { get; set; }

        public string FileName { get; set; }

        public ICollection<SearchTag> SearchTags { get; set; }
    }
}
 