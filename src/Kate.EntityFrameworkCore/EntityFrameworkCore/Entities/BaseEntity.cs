﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kate.EntityFrameworkCore.Entities
{
    public class BaseEntity<T> : FullAuditedEntity where T : class
    {         
        public ICollection<T> Translations { get; set; }  
    }
}
