﻿using System;
using System.Collections.Generic;

namespace Kate.EntityFrameworkCore.Entities
{
    public class PhotoVideoGallery : BaseEntity<PhotoVideoGalleryTranslation>
    {
        public PhotoVideoGallery()
        {
            Translations = new HashSet<PhotoVideoGalleryTranslation>();
            GalleryFiles = new HashSet<GalleryFile>();
            SearchTags = new HashSet<SearchTag>();
        }
        
        public bool ShowOnSite { get; set; }
        public bool ShowOnMainPageBox { get; set; }

        public DateTime GalleryDate { get; set; }
        
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public ICollection<GalleryFile> GalleryFiles { get; set; }
        public ICollection<SearchTag> SearchTags { get; set; }
    }
}