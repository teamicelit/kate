﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Kate.EntityFrameworkCore.Entities
{
    public class SearchTag : FullAuditedEntity
    {
        public string Tag { get; set; }     

        public int? ArticleId { get; set; }
        public Article Article { get; set; }

        public int? CategoryId { get; set; }
        public Category Category { get; set; }

        public int? EventId { get; set; }
        public Event Event { get; set; }

        public int? DecisionId { get; set; }
        public Decision Decision { get; set; }
        
        public int? DisciplinaryId { get; set; }
        public Disciplinary Disciplinary { get; set; }
        
        public int? PhotoVideoGalleryId { get; set; }
        public PhotoVideoGallery PhotoVideoGallery { get; set; }
    }
}