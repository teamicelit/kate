﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Kate.Web.Sign.Database
{
    [Table("DocumentSignatures", Schema = "public")]
    public class DocumentSignature
    {
        public Guid Id { get; set; }

        public string FilePath { get; set; }

        public bool IsProcessed { get; set; }

        public DocumentSignature()
        {
            Id = Guid.NewGuid();
        }
    }

    public class KateDbContext : DbContext
    {
        public DbSet<DocumentSignature> DocumentSignatures { get; set; }

        public KateDbContext() : base("Default")
        {
        }
    }
}