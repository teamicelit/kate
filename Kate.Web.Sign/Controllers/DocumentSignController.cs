﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ge.eid.esignature.adessa.helper;
using ge.eid.esignature.adessa.pades.sign;
using java.io;
using java.net;
using java.security;
using java.util;
using Kate.Web.Sign.Database;
using org.bouncycastle.jce.provider;
using org.bouncycastle.util.encoders;

namespace Kate.Web.Sign.Controllers
{
    public class JsonMessage
    {
        public String docType { get; set; }

        public String dataType { get; set; }


        public String dataHex { get; set; }


        public String dataUrl { get; set; }


        public String hash { get; set; }


        public String signAlg { get; set; }


        public String submitUrl { get; set; }

        public String description { get; set; }

        public String language { get; set; }

        public String keyId { get; set; }

        public String signatureProfile { get; set; }
    }

    public class DocumentSignController : Controller
    {
        private readonly KateDbContext _kateDbContext = new KateDbContext();

        public ActionResult Test()
        {
            var x = _kateDbContext.DocumentSignatures.ToList();

            return Content(x.Count.ToString());
        }

        [HttpPost]
        public JsonResult UploadFile(HttpPostedFileBase file)
        {
            if (!Directory.Exists(Server.MapPath("~/TempDocs")))
            {
                Directory.CreateDirectory(Server.MapPath("~/TempDocs"));
            }

            var fileName = $"/TempDocs/" + Guid.NewGuid() + ".pdf";

            file.SaveAs(Server.MapPath(fileName));

            return Json(new
            {
                url = fileName
            });
        }

        [HttpPost]
        public ActionResult PutHash(string signedData, string fileName = "")
        {
            var path = Server.MapPath($"~{fileName}");
            var docBytes = System.IO.File.ReadAllBytes(path);

            ByteArrayInputStream byteArrayInputStream = null;

            var signer = new PDFSigner();
            var bouncyCastleProvider = new BouncyCastleProvider();
            Security.addProvider(bouncyCastleProvider);
            signer.setProvider(bouncyCastleProvider);

            byteArrayInputStream = new ByteArrayInputStream(docBytes);
            var document = new AdessaDocument(byteArrayInputStream);

            var signingTime = Calendar.getInstance();
            signingTime.setTimeInMillis(1467882695703);
            var signatureProperties = new SignatureProperties();
            signatureProperties.setClaimedTime(signingTime);
            signatureProperties.setPreferredSize(16 * 1024);
            signatureProperties
                    .setTsaUrl(new URL("http://tsa.cra.ge/signserver/tsa?workerName=TimeStampSigner"));
            var id = new java.lang.Long(13);
            document.setDocumentId(id);
            var cmsBytes = Hex.decode(signedData.ToUpper());
            var outputFilePath = path;
            var outputStream = new FileOutputStream(outputFilePath);
            signer.addCMSToDocument(document, outputStream, cmsBytes, signatureProperties, PAdESProfileConformity.B_LEVEL);
            outputStream.close();

            var file = _kateDbContext.DocumentSignatures.SingleOrDefault(x => x.FilePath == fileName);
            if (file == null)
            {
                throw new System.IO.FileNotFoundException();
            }

            file.IsProcessed = true;
            _kateDbContext.SaveChanges();

            return Json(new { success = true });
        }

        public ActionResult Download(string fileName)
        {
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "document.pdf",
                Inline = false,
            };

            Response.AddHeader("Content-Disposition", cd.ToString());

            var filePath = Server.MapPath($"~{fileName}");
            return File(System.IO.File.ReadAllBytes(filePath), cd.ToString());
        }

        public string GetDocumentHashString(string fileName)
        {
            var filePath = Server.MapPath($"~{fileName}");

            if (!System.IO.File.Exists(filePath))
            {
                throw new System.IO.FileNotFoundException();
            }

            var docBytes = System.IO.File.ReadAllBytes(filePath);
            ByteArrayInputStream byteArrayInputStream = null;
            PDFSigner signer = new PDFSigner();
            BouncyCastleProvider bouncyCastleProvider = new BouncyCastleProvider();
            Security.addProvider(bouncyCastleProvider);
            signer.setProvider(bouncyCastleProvider);

            byteArrayInputStream = new ByteArrayInputStream(docBytes);
            AdessaDocument document = new AdessaDocument(byteArrayInputStream);

            Calendar signingTime = Calendar.getInstance();
            signingTime.setTimeInMillis(1467882695703);
            SignatureProperties signatureProperties = new SignatureProperties();
            signatureProperties.setClaimedTime(signingTime);
            signatureProperties.setPreferredSize(16 * 1024);
            signatureProperties
                    .setTsaUrl(new URL("http://tsa.cra.ge/signserver/tsa?workerName=TimeStampSigner"));
            java.lang.Long id = new java.lang.Long(13);
            document.setDocumentId(id);
            byte[] documentHash = signer.getDocumentRangeHash(document, "SHA-256", signatureProperties);

            // Convert the byte to hex format
            String hexStr = Hex.toHexString(documentHash);
            return hexStr;
        }

        protected override void Dispose(bool disposing)
        {
            _kateDbContext.Dispose();
            base.Dispose(disposing);
        }
    }
}